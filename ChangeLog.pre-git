2009-04-15  Paolo Bacchilega  <paobac@svn.gnome.org>

	[ version 2.14.0 released ]

	* NEWS: 
	* configure.ac: updated.

2009-02-21  Paolo Bacchilega  <paobac@svn.gnome.org>

	* COPYING: new file
	
	Fixed bug #572564 – Missing COPYING information.

2009-02-17  Paolo Bacchilega  <paobac@svn.gnome.org>

	[ version 2.13.2 released ]

	* NEWS: 
	* configure.ac: updated.

2009-02-12  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.c (open_file_list_with_launcher): don't free the
	file list.

2009-02-11  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/file-utils.c (_g_desktop_entry_new_for_uri): marked string
	for translation.

2009-02-11  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* src/quick-lounge.c: 
	* src/quick-button.c: 
	* src/quick-box.h: 
	* src/quick-box.c: 

	New feature: Allow to add url launchers by dropping a file between
	launchers.  On the other hand if the file is dropped on a launcher 
	than the file will be opened with that launcher.

2009-02-11  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.h: 
	* src/quick-lounge.c: 
	* src/quick-desktop-entry-dialog.c: 
	* src/quick-box.c: 	
	* src/file-utils.h: 
	* src/file-utils.c: 
	* src/dlg-properties.c: 
	
	* data/ui/Makefile.am: 
	* data/ui/link-entry-editor.ui: new file
	* data/ui/desktop-entry-editor.ui: 

	New feature: Allow to add url launchers by dragging a file on 
	the preferences dialog.
	
	New feature: Added support for URL launchers.

2009-02-05  Paolo Bacchilega  <paobac@svn.gnome.org>

	[ version 2.13.1 released ]

	* NEWS: updated
	
	* src/gtk-utils.h: 
	* src/gtk-utils.c (create_missing_pixbuf): scale the image if the size
	doesn't match the requested size.
	
	* src/dlg-add-from-menu.c: do not use the missing image icon in the
	application list.

2009-02-04  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/xstuff.c:
	* src/xstuff.h: 
	
	Feature: Removed the animation when launching the application.

	* src/quick-lounge.c: 
	* src/quick-box.h: 
	* src/quick-box.c: 
	* src/Makefile.am: 
	* src/gtk-utils.h: 
	* src/gtk-utils.c: 
	* src/file-utils.h: 
	* src/file-utils.c: 
	* src/dlg-properties.c: 

	Feature: Support launching apps by dropping files on them (#171739).

	* src/quick-desktop-entry-dialog.c: 
	* data/ui/desktop-entry-editor.ui: fixed label mnemonics.

2009-02-04  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* src/Makefile.am: 
	* src/quick-desktop-entry-dialog.c: 
	* src/quick-desktop-entry-dialog.h: 
	
	renamed qck-desktop-entry-dialog.c as quick-desktop-entry-dialog.c
	
	* src/dlg-add-from-menu.c: 
	* src/dlg-add-from-menu.h: 
	
	renamed dlg-pick-applications.c as dlg-add-from-menu.c
	
	* src/quick-lounge.c: 
	* src/quick-button.c: 
	* src/gtk-utils.h: 
	* src/gtk-utils.c: 
	
	Feature: Use the default panel launcher icon, if the icon is missing.
	
	* data/ui/properties.ui: 

2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* data/ui/properties.ui: 
	* data/ui/desktop-entry-editor.ui: 
	* data/ui/choose-from-menu.ui: added comments for translators and 
	mnemotics to some label.

2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.c (about_verb_cb): updated copyright year.

2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/dlg-properties.c: set the column label acording to the 
	orientation.
	* data/ui/properties.ui: added a column label.

2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* configure.ac: changed version to 2.13.1
	
	* src/quick-lounge.c: 
	* src/quick-box.h: 
	* src/quick-box.c: 
	
	register the applet to the theme_changed signal instead of 
	the quick_box.
	
2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.h: 
	* src/quick-lounge.c: 
	* src/quick-box.h: 
	* src/quick-box.c: 
	* src/dlg-properties.c: 
	* data/ui/properties.ui: 
	* data/quick-lounge.schemas.in: 
	
	Feature: Allow to specify the number of rows (columns) explicitly.
	Patch by Jared Warren (#161444).
	
2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* configure.ac: doesn't depend on libgnome and libgnomeui anymore, 
	though libpanelapplet does and it brings the dependency as a side 
	effect.
	
2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>	
	
	* src/qck-desktop-entry-dialog.c (icon_button_clicked_cb): 
	(browse_button_clicked_cb): use the home directory if the command
	or icon do not specify a path.
	
2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* src/quick-lounge.c (get_child_position): do not increment pos 
	twice per cycle.

2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.c: 
	* src/dlg-properties.c: 
	* data/ui/properties.ui: 
	* data/ui/choose-from-menu.ui: ui changes.
	
2009-02-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/qck-desktop-entry-dialog.c: 
	
	allow to select the command from the file chooser.
	
	* src/dlg-pick-applications.c: 
	* data/ui/choose-from-menu.ui: 

	Feature: Re-designed the "choose application from menu" dialog.

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/dlg-properties.c: update the button sensitivity.
	
	Fixes bug #532174 – Odd behaviour when changing the order of 
	the icons.
	
2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* src/file-utils.c (_g_key_file_cleanup): 

	remove the X-Ubuntu-Gettext-Domain from the key_file.

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-box.c: 
	* src/qck-desktop-entry-dialog.c: 
	* src/file-utils.h: 
	* src/file-utils.c: 
	
	no need to use a personalized g_key_file_get_locale_string function.

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/file-utils.c (_g_key_file_get_locale_string): do not 
	dereference error.
	
2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/file-utils.h: 	
	* src/file-utils.c: added missing declaration.	

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-box.c: 
	* src/qck-desktop-entry-dialog.c: 
	* src/file-utils.h: 	
	* src/file-utils.c: 
	
	find a suitable locale when getting a locale string.
		
2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>	
	
	* src/quick-box.c: 
	
	do not include the arrow to the size request.
	
	* src/quick-lounge.h: 
	* src/quick-lounge.c: 
	* src/dlg-properties.c: 
	
	Update the min value if the max value is smaller and vice versa.
	
	* data/ui/properties.ui: assign an adjustement to the spinbuttons 

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-box.c: use $(HOME) as path if not specified in the
	desktop entry.

2009-02-02  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-box.h: 
	* src/quick-box.c: use the new tooltip api.	

2009-02-01  Paolo Bacchilega  <paobac@svn.gnome.org>

	* configure.ac: removed the gnome-desktop dependency.
	
	* src/quick-separator.c: 
	* src/quick-lounge.h: 
	* src/quick-lounge.c: 
	* src/quick-button.h: 
	* src/quick-button.c: 
	* src/quick-box.h: 
	* src/quick-box.c: 
	* src/qck-desktop-entry-dialog.h: added
	* src/qck-desktop-entry-dialog.c: added
	* src/Makefile.am: 
	* src/gtk-utils.h: 
	* src/gtk-utils.c: 
	* src/gnome-ditem-edit.c: removed
	* src/gnome-ditem-edit.h: removed
	* src/file-utils.h: 
	* src/file-utils.c: 
	* src/dlg-properties.c: 
	* src/dlg-pick-applications.c: 
	
	Feature: Replaced the old launcher dialog with a dialog  
	similar to the one used by gnome-panel.
	
	* data/ui/Makefile.am: 
	* data/ui/desktop-entry-editor.ui: new file.

2009-01-31  Paolo Bacchilega  <paobac@svn.gnome.org>

	* help: 
	
	Feature: Migrated the documentation to gnome-doc-utils.  
	
	I couldn't migrate the documentation as described in 
	http://live.gnome.org/GnomeDocUtilsTranslationMigration, because
	it gives some errors that I don't know how to fix.

	* configure.ac: requires gtk 2.14 for gtk_show_uri
	
	* src/quick-lounge.c: 
	* src/gtk-utils.h: 
	* src/gtk-utils.c: 
	* src/dlg-properties.c: 
	* src/dlg-pick-applications.c: use gtk_show_uri to show the help.

2009-01-30  Paolo Bacchilega  <paobac@svn.gnome.org>

	* autogen.sh: check for configure.ac now.

2009-01-30  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* configure.ac: 
	* configure.in: renamed .in as .ac
	
	* data/ui/properties.ui: 
	* data/ui/choose-from-menu.ui: 
	* src/Makefile.am: fixed loading of the ui.

2009-01-30  Paolo Bacchilega  <paobac@svn.gnome.org>

	* configure.in: 
	* src/typedefs.h: 
	* src/quick-lounge.h: 
	* src/quick-lounge.c: 
	* src/quick-box.c: 
	* src/file-utils.h: 
	* src/file-utils.c: 
	* src/dlg-properties.c: 
	* src/dlg-pick-applications.c: 

	Feature: Ported to gio.

2009-01-30  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	Moved data to the data subdirectory.  
	
	Feature: Removed the libglade dependency.

2009-01-30  Paolo Bacchilega  <paobac@svn.gnome.org>

	* configure.in: removed dependency on libgnome-desktop.
	
	* src/gnome-ditem-edit.h: 
	* src/gnome-ditem-edit.c: new files

	* src/quick-lounge.c: 
	* src/quick-box.c: 
	* src/Makefile.am: 	
	* src/dlg-properties.c: 

	Use a local copy of gnome-ditem-edit, because libgnome-desktop 
	doesn't	have it anymore.
	Fixes bug #559584 – does not build on GNOME 2.25
	Patch by Migi.

2008-09-11  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/GNOME_QuickLoungeApplet.xml: 
	
	Fixed bug #551786 – applet still uses deprecated gnome-stock-about 
	icon instead of gtk-about.
	Patch by Pacho Ramos.

2008-08-05  Paolo Bacchilega  <paobac@svn.gnome.org>

	[ version 2.12.6 released ]

	* NEWS: updated
	
2008-08-05  Paolo Bacchilega  <paobac@svn.gnome.org>	
	
	* src/GNOME_QuickLoungeApplet_Factory.server.in.in: 
	
    Fixes bug: interaction with b-a-s left over from previous sessions.
    Patch by Philipp Kern.

2008-07-27  Paolo Bacchilega  <paobac@svn.gnome.org>

	* configure.in: removed pangox check, use AM_PROG_LIBTOOL instead of
	LT_PROG_LIBTOOL.
	
	Fixed bug #544888 – LT_PROG_LIBTOOL should be AM_PROG_LIBTOOL?
	Fixed bug #544850 – configure needs to handle the non-existence of pangox

2008-05-09  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/quick-lounge.c (about_verb_cb): fixed logo loading in the
	about dialog.  Updated the copyright year.

2008-05-09  Paolo Bacchilega  <paobac@svn.gnome.org>
	
	* src/quick-box.c (quick_box_size_allocate): 
	(get_child_size): 
	(quick_box_size_request): 

	Fixed bug #531358 – Applet height in vertical mode.
	Patch by Alexander Kojevnikov.

2008-05-09  Paolo Bacchilega  <paobac@svn.gnome.org>

	* configure.in: 
	
	post release version bump.

2008-04-15  Paolo Bacchilega  <paobac@svn.gnome.org>

	[ version 2.12.5 released ]

	* NEWS: updated for 2.12.5

2007-11-03  Paolo Bacchilega  <paobac@svn.gnome.org>

	* src/dlg-pick-applications.c (add_applications_from_dir): 
	
	In src/dlg-pick-applications.c, the code calls functions like
	gmenu_tree_entry_get_icon, gmenu_tree_entry_get_name,
	gmenu_tree_entry_get_comment when processing directories.  This
	causes core dumping issues that are fixed when the code instead
	calls gmenu_tree_directory_get_icon, gmenu_tree_directory_get_name,
	gmenu_tree_directory_get_comment.
	
	Fixes bug #489133 – quick-lounge-applet crashes

	Patch by Brian Cameron.  

2007-09-01  Paolo Bacchilega  <paobac@svn.gnome.org>

	* MAINTAINERS: added because it's required now.

2007-01-16  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.12.4 released ]

	* configure.in: changed version to 2.12.4
	* NEWS: updated for 2.12.4

2007-01-07  Christian Persch  <chpe@svn.gnome.org>

	* autogen.sh:
	* configure.in:
	* po/LINGUAS:
	* src/Makefile.am: Use po/LINGUAS as per GNOME Goal, and cleanup build
	system. Bug #393729.

	* src/file-utils.c:
	* src/quick-box.h:
	* src/quick-button.c: (quick_button_expose), (quick_button_new):
	* src/quick-button.h:
	* src/quick-lounge.c: (get_button_from_uri),
	(quick_lounge_load_uri_async), (quick_lounge_applet_fill):
	* src/quick-lounge.h:
	* src/quick-separator.c: Fix all compile warnings and deprecated
	functions use.

	* src/gtk-utils.c: (_gtk_error_dialog_from_gerror_run),
	(_gtk_error_dialog_run), (_gtk_info_dialog_run):
	* src/gtk-utils.h: Remove cruft and just use GtkMessageDialog API. Bug
	#393731.

2006-12-14  Pema Geyleg  <pgeyleg@gmail.com>

	* configure.in: Added 'dz' in ALL_LINGUAS

2006-11-11  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.c (ditem_properties_clicked):

	Fixed bug #369103 – Add help response to open up suitable user guide link
	Patch by Glynn Foster

	* configure.in:
	* src/Makefile.am:

	Fixed bug #369265 – Building fails on Solaris lacking X_LIBS
	Patch by Glynn Foster

2006-10-17  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/gtk-utils.h:
	* src/dlg-pick-applications.c:
	* src/dlg-properties.c:
	* src/gtk-utils.c:
	* src/quick-box.c:
	* src/quick-button.c:
	* src/quick-lounge.c:
	* src/GNOME_QuickLoungeApplet_Factory.server.in.in:
	* src/Makefile.am:
	* Makefile.am:

    Use the 'screen' id.  Ported over to GtkIconTheme.
    Patch by Glynn Foster

2006-10-13  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.12.3 released ]

	* configure.in: changed version to 2.12.3

	* src/quick-lounge.c (properties_verb_cb, about_verb_cb):
	* src/quick-box.c (display_popup_menu):
	* src/dlg-pick-applications.c (dlg_pick_applications):

	Fixed bug #319050 - Dropdown shows on wrong head
	Patch by Glynn Foster

2006-07-12  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.12.2 released ]

	* NEWS:
	* configure.in: update for 2.12.2

2006-09-01  Abel Cheung  <abel@oaka.org>

	* configure.in: Added 'tr' to ALL_LINGUAS.

2006-04-18  Kjartan Maraas <kmaraas@gnome.org>

	* configure.in: Rename.
	* po/nb.po: Add this
	* po/no.po: Remove this

2006-04-09  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-box.c (arrow_button_do_not_eat_button_press):

	Fixed bug #335628 - Don't eat the button press on the arrow button.
	Patch by Yang Hong.

2006-01-24  Clytie Siddall <clytie@riverland.net.au>

	* configure.in	Added vi in ALL_LINGUAS line.

2005-11-10  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.12.1 released ]

	* NEWS: updated

2005-11-07  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/dlg-properties.c (update_list):
	* src/quick-lounge.c:
	* src/quick-box.c (arrow_button_pressed, icon_theme_changed):

	Update the icons without reloading the launchers when the
	icon_theme_changed event is emitted.  Fixes bug #317709.

2005-11-06  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-box.c (arrow_button_pressed): use gtk_container_get_children
	to get the launchers.

	* src/quick-lounge.c (populate_context_menu): added execute command
	in the context menu to conform to the gnome panel.
	(populate_context_menu): use stock items.

	* src/quick-button.c (drag_data_get):
	* src/dlg-properties.c (drag_data_get): cast to guchar to avoid
	compilation warning.

	* src/gtk-utils.c (_gtk_container_get_n_children)
	(_gtk_container_get_all_children): new functions.

	* src/quick-box.c (quick_box_size_request):

	Fixes bug #151612: Blank space between last icon and button should be
	removed.

2005-11-05  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/GNOME_QuickLoungeApplet_Factory.server.in.in: the category is
	Utilities, not Utility.

	* src/quick-box.c (ICON_SIZE_PANEL): set panel icon size to 22 (as the
	menu icon size)
	(arrow_button_pressed): use gtk_container_get_children to get the
	children.

2005-10-04  Pawan Chitrakar  <pchitrakar@gmail.com>

	* configure.in: Added ne in ALL_LINGUAS

2005-09-18  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.12.0 released ]

	* src/dlg-pick-applications.c (add_applications_from_dir): cast to
	GMENU_TREE_ENTRY

	* NEWS: updated.

2005-09-16  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* configure.in: depends on libgnome-menu >= 2.12.0

	* src/Makefile.am:
	* src/dlg-pick-applications.c:

	Fixes bug #314818: Port to new libgnomemenu API
	Patch by Glynn Foster.

2005-08-16  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* NEWS: fixed a typo.

2005-08-16  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.10.1 released ]

2005-05-30  Srirama Sharma  <srirama.sharma@wipro.com>

	* src/quick-lounge.c (new_launcher_dialog_response_cb):
 	launch help when user clicks on the new launcher help button.

2005-04-20  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-separator.c (quick_separator_expose): draw a separator.

2005-06-05  Ignacio Casal Quinteiro <icq@cvs.gnome.org>

	* configure.in: Added 'gl' to ALL_LINGUAS

2005-05-19  Alexander Shopov  <ash@contact.bg>

	* configure.in: Added "bg" (Bulgarian) to ALL_LINGUAS

2005-04-04  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.10.0 released ]

2005-04-03  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.glade: use the stock 'New' icon for the
	'Add new launcher' button.

2005-04-01  Steve Murphy  <murf@e-tools.com>

	* configure.in: Added "rw" to ALL_LINGUAS.

2005-03-26  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-box.c (quick_box_add_separator)
	(quick_box_add_button): remove gtk_widget_realize

	* src/dlg-properties.c (drag_data_received):
	Fixed drag&drop position computing when reordering launchers.
	(drag_motion): autoscroll when dragging an item.

2005-03-25  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/dlg-properties.c (drag_data_received, update_sensitivity):
	Fix drag&drop position computing.  Make moving of launcher with
	the arrow buttons more usable.

	* src/quick-box.c (quick_box_add_button)
	(quick_box_add_separator): realize buttons here.

2005-03-17  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/quick-lounge.c (about_verb_cb): use GtkAboutDialog instead of
	deprecated GnomeAbout. Fix a little memory leak.

2005-03-11  Ryan Lortie  <desrt@desrt.ca>

	* src/quick-lounge.c: Fix leaking of a gtk style.

2005-02-08  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.9.1 released ]

2005-02-03  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.glade: center dialogs.

	* src/GNOME_QuickLoungeApplet.xml: use Properties instead of
	Preferences.

	* quick-lounge-applet/src/Makefile.am:
	* quick-lounge-applet/src/dlg-pick-applications.c:
	* quick-lounge-applet/src/gtk-utils.c:
	* quick-lounge-applet/src/gtk-utils.h:
	* quick-lounge-applet/src/quick-box.c:
	* quick-lounge-applet/src/quick-button.c:
	* quick-lounge-applet/src/quick-lounge.c:
	* quick-lounge-applet/src/quick-lounge.glade:

	Use libgnome_menu instead of applications:// to retrieve
	the list of available applications.

	* quick-lounge-applet/configure.in:

	Updated version number and dependencies.

2005-01-18  Dinoop Thomas <dinoop.thomas@wipro.com>

	* src/quick-lounge.c (quick_lounge_set_min_visible_cols):
	  Added a check to ensure min value does not go beyond max.
	  (quick_lounge_set_max_size): Added a check to ensure max
	  value does not fall below min.
	* src/dlg-properties.c (min_size_value_changed_cb):
	  (change_max_size):
	  Set the value of the spin button.

        Fixes bug #164417: Minimum size can be set more than Maximum in
        quicklounge

2005-01-17  Amanpreet Singh Alam <amanpreetalam@yahoo.com>

	*configure.in: Punjabi (pa) is added to ALL_LINGUAS

2005-01-17  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.c (applet_change_background):

	Fixes bug #164319: patch to support panel transparency and pixmap
	backgrounds.
	Patch by Ryan Lortie.

2004-12-24  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-box.c (quick_box_get_type): use GType instead of guint.

2004-10-13  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.2.0 released ]

2004-08-23  Ilkka Tuohela  <hile@iki.fi>

	* configure.in: Added "fi" to ALL_LINGUAS

2004-08-16  Christian Rose  <menthos@menthos.com>

	* configure.in: Added "bs" to ALL_LINGUAS.

2004-08-11  Ankit Patel <ankit@redhat.com>

	* configure.in : Added "gu" to ALL_LINGUAS.

2004-08-10  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.1.2 released ]

2004-06-23  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/dlg-pick-applications.c (add_applications):
	* src/dlg-properties.c (drag_data_received):
	* src/file-utils.c (get_unique_desktop_file):
	* src/quick-lounge.c (remove_directory): use g_build_filename
	instead of g_strconcat where appropriate.  Fixes bug #144855.
	(about_verb_cb): updated copyright note and e-mail address.

2004-06-18  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/dlg-properties.c (change_max_size): move change_max_size to
	the top.

2004-06-18  Vinay M R  <vinay.mandyakoppal@wipro.com>

	* src/dlg-properties.c (destroy_cb): Fix the applet crash
	when max value is changed and dialog is closed quickly.
	Fixes bug #144594

2004-05-18  Paolo Bacchilega  <paolo@localhost.localdomain>

	* src/quick-box.c (create_arrow_button): added a tooltip to the arrow
	button.

2004-05-03  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.glade: fix label alignment.

	* src/quick-box.c (quick_box_size_request): fix size request on
	vertical panels.

2004-04-29  Adam Weinberger  <adamw@gnome.org>

	* configure.in: Added en_CA to ALL_LINGUAS.

2004-04-09  Arvind Samptur  <arvind.samptur@wipro.com>

	* src/quick-button.c: (quick_button_new) : Remove the
	un-neccesary check for icon_path. We any fall back
	to gnome-unknown.png when we don't have one.

	Fixes #139587

2004-04-17  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.glade:

	Fixes bug #139939: "Quick Lounge Properties" dialog -- various
	suggestions.

	* src/GNOME_QuickLoungeApplet.xml: use Preferences instead of
	Properties.

	Bug #139938: Menu item: "Properties" should be "Preferences".

2004-04-04  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/quick-lounge.schemas.in: s/follow/follows/

2004-04-08  Yuriy Syrota  <rasta@cvs.gnome.org>

	* configure.in: Added "uk" (Ukrainian) to ALL_LINGUAS.

2004-04-08  Gareth Owen  <gowen72@yahoo.com>

	* configure.in: Added en_GB to ALL_LINGUAS

2004-04-02  Dmitry G. Mastrukov  <dmitry@taurussoft.org>

	* configure.in: Added Russian to ALL_LINGUAS.

2004-03-29  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.1.1 released ]

2004-03-26  Paolo Bacchilega  <paobac@cvs.gnome.org>

	* src/dlg-properties.c (drag_data_received, drag_data_get): make DnD
        work with spaces too.

2004-03-24  Paolo Bacchilega  <paobac@cvs.gnome.org>

	[ version 2.1.0 released ]

	* src/quick-lounge.c (about_verb_cb): added credits to the documenters.

	* src/dlg-properties.c (help_cb, add_space_cb): do not convert to utf8
	the separator name, it's already in utf8 format.

	* src/quick-lounge.c (panel_show_gnome_help): open help windows on the
	current screen.
	(populate_context_menu): remove the help items to be consistent with
	the panel launcher.

	* src/xstuff.c:
	* src/xstuff.h: copied from gnome-panel for the lauch animation code.

	* src/quick-box.c (quick_box_add_button): use the same tooltip of the
	standard launcher.
	(item_activated): added launch animation.

	* src/Makefile.am (glade_DATA):
	* src/quick-lounge.c: use a exe factory instead of a shared library.

	* src/file-utils.c (is_desktop_file): add application/x-desktop as
        possible mime type for .desktop files.

2004-03-23  Glynn Foster  <glynn.foster@sun.com>

	* autogen.sh, Makefile.am, configure.in: Enable documentation.

2004-03-23  Glynn Foster  <glynn.foster@sun.com>

	* help/*: Online user documentation for de, es, fr, it, ja, ko,
	sv, zh_CN, zh_HK and zh_TW.

2004-03-22  Gustavo Maciel Dias Vieira  <gdvieira@zaz.com.br>

	* configure.in: Added pt_BR to ALL_LINGUAS.

2004-02-09 Breda McColgan <breda.mccolgan@sun.com>

	* help: Created for GNOME 2.6.
	* help/C: Created for GNOME 2.6.
	* help/C/quick-lounge.xml: Created for GNOME 2.6. Issued for technical review.
	* help/C/quick-lounge-C.omf: Created for GNOME 2.6.
	* help/C/legal.xml: Created for GNOME 2.6.
	* help/C/l10n.txt: Created for GNOME 2.6.
	* help/C/figures: Created for GNOME 2.6.
	* help/C/figures/quick-lounge_down.png: Created for GNOME 2.6.
	* help/C/figures/quick-lounge_end.png: Created for GNOME 2.6.
	* help/C/figures/quick-lounge_start.png: Created for GNOME 2.6.
	* help/C/figures/quick-lounge_up.png: Created for GNOME 2.6.
	* help/C/figures/quick-lounge_window.png: Created for GNOME 2.6.

2004-02-07  Robert Sedak  <robert.sedak@sk.htnet.hr>

         * configure.in: Added "hr" (Croatian) to ALL_LINGUAS.

2003-10-06  Shakti Sen  <shakti.sen@wipro.com>

	* src/quick-lounge.c (applet_destroy): fix bug #123935

2003-12-16  Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>

	* sk.po: Added Slovak translation by Zdenko Podobny.

2003-11-27  Jordi Mallach  <jordi@sindominio.net>

	* configure.in (ALL_LINGUAS): Added "ca" (Catalan).

2003-11-23  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 2.0.3 released ]

	* src/Makefile.am (DISABLE_DEPRECATED): do not compile with
	DISABLE_DEPRECATED macros, unless specified with a configure option.

	* src/quick-lounge.c (applet_size_request, applet_size_allocate):
	update the icon size when the panel size changes.

2003-11-18  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 2.0.2 released ]

2003-09-20  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 2.0.1 released ]

2003-09-01  Metin Amiroff  <metin@karegen.com>

	configure.in: Added "az" in ALL_LINGUAS.

2003-08-30  Wang Jian  <lark@linux.net.cn>

	* configure.in: Added "zh_CN" to ALL_LINGUAS.

2003-08-25  Danilo Šegan  <dsegan@gmx.net>

	* configure.in: Added "sr" and "sr@Latn" to ALL_LINGUAS.

2003-08-23  Kjartan Maraas  <kmaraas@gnome.org>

	* configure.in: Added Norwegian (no) to ALL_LINGUAS.

2003-08-20  Christophe Merlet  <redfox@redfoxcenter.org>

        * configure.in: Added French (fr) to ALL_LINGUAS.

2003-08-18  Artur Flinta <aflinta@cvs.gnome.org>

        * configure.in: Added Portuguese (pl) to ALL_LINGUAS.

2003-07-31  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (about_verb_cb): fix bugs #118681 and #118682

2003-07-29  Pablo Gonzalo del Campo  <pablodc@bigfoot.com>

        * configure.in: Added Spanish (es) to ALL_LINGUAS.

2003-07-28  Duarte Loreto <happyguy_pt@hotmail.com>

        * configure.in: Added Portuguese (pt) to ALL_LINGUAS.

2003-07-28  Christian Rose  <menthos@menthos.com>

	* sv.po: Added "sv" to ALL_LINGUAS.

2003-07-10  Takeshi AIHANA <aihana@gnome.gr.jp>

	* configure.in: Added Japanese 'ja' into ALL_LINGUAS.

2003-07-07  Hasbullah Bin Pit  <sebol@ikhlas.com>

        * configure.in: Added ms to ALL_LINGUAS.

2003-07-06  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 2.0.0 released ]

2003-07-05  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (quick_lounge_applet_fill): allow svg icons.

2003-05-04  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 1.1.4 released ]

2003-03-26  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (quick_lounge_applet_fill): do not overwrite
	the min_cols value when it is setted to 0.

2003-02-26  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 1.1.3 released ]

2003-02-01  James Willcox  <jwillcox@gnome.org>

	* src/quick-box.c (item_activated): Launch the app on the correct
	screen (how did I miss this before?)

2003-01-29  James Willcox  <jwillcox@gnome.org>

	* src/quick-box.c (button_button_press_cb): Popup the context menu
	on the correct screen.

2003-01-27  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ version 1.1.2 released ]

2003-01-23  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-box.c (quick_box_size_request):
	* src/quick-lounge.glade: Allow to set the minimum dimension to 0.

	* src/quick-box.c (quick_box_size_request): fixed size request.

2003-01-21  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-box.c (quick_box_size_allocate): center icons.
	(get_child_size): use a bigger size for icons.

2002-10-21  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/file-utils.c (file_copy): add FOLLOW_LINKS option.

2002-10-15  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ Quick Lounge 0.96 ]

2002-10-14  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/Makefile.am : distribute quick-lounge.schemas not
	quick-lounge.schemas.in.

	* src/quick-lounge.c (applet_destroy): do not remove the directory
	when the applet is destroyed.

	* src/quick-box.c (menu_item__drag_begin): disable tooltips before
	starting the drag action.
	(menu_item__drag_end): re-enable them again when the drag action
	is terminated.

2002-09-27  Paolo Bacchilega  <paolo.bacch@tin.it>

	[ Quick Lounge 0.93 ]

2002-09-27  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (quick_lounge_new_launcher): when creating a
	new launcher use Cancel,OK buttons.

2002-09-25  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/dlg-pick-applications.c:
	* src/dlg-pick-applications.h: implement the "Add from menu" function.

2002-09-20  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (ditem_properties_close, item_properties_cb)
	(ditem_properties_changed, ditem_properties_apply_timeout)
	(ditem_properties_clicked): automatically update changes when
	editing ditem properties.

2002-09-12  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c:
	* src/quick-box.c: Added max size options.

2002-08-28  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (about_verb_cb): Add e-mail address.

2002-08-25  Paolo Bacchilega  <paolo.bacch@tin.it>

	* Version 0.87 released.

2002-08-23  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-box.c (quick_box_size_allocate): avoid infinite loop when
	height is inconsistent.

2002-08-22  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-lounge.c (populate_context_menu): Add "Help on Applicaction"
	menu item if appropiate.

2002-08-19  Paolo Bacchilega  <paolo.bacch@tin.it>

	* Version 0.75 released.

2002-08-18  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/dlg-properties.c (update_list): do not translate the name to
	utf8, it's already in utf8 format.

	* src/quick-box.c: size request and allocation should work now.

2002-08-17  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/dlg-properties.c: update size when adding or removing items.

	* src/GNOME_QuickLoungeApplet.xml: remove useless options.

	* src/quick-lounge.c (get_unique_dirname): new function
	(quick_lounge_applet_fill): create a unique directory for each applet.

2002-08-16  Paolo Bacchilega  <paolo.bacch@tin.it>

	* Version 0.5 released.

2002-08-16  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-box.c (quick_box_size_request): add a box->spacing to the
	size request.
	(quick_box_size_request): handle the vertical orientation case.

	* src/quick-lounge.c (applet_size_request): use child_req.width
	instead	of child_req.width - 1.

2002-08-15  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/quick-box.h:
	* src/quick-box.c (quick_box_get_size_hint_list): New function
	(quick_box_size_request): Calculate min and max size.

	* src/quick-lounge.c (applet_size_request): Use the size hints.

	* src/*.[ch]: a lot of changes.

2002-08-13  Paolo Bacchilega  <paolo.bacch@tin.it>

	* src/Makefile.am (schemas_in_files): rename
	quick-lounge-applet.schemas.in to quick-lounge.schemas.in

	* Imported all files.

