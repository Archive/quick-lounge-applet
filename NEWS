version 2.14.1
--------------

        Bugs fixed:

	* Always show the icons in the launchers menu.
	* Show dialogs on the correct screen. All the dialogs are now
          displayed on the screen where the action that activated the dialog
          was executed.
	* Set the working directory to the some directory of the terminal 
	  command.
	* Fixed build with gmake-3.82
		
        New or updated application translations:

        * Polish (Piotr Drąg)
        * Brazilian Portuguese (Djavan Fagundes)
        * Chinese (simplified) (YunQiang Su)
        * Czech (Marek Černocký)
        * Dansk-gruppen (Joe Hansen)
        * Español (Jorge González)
        * French (Claude Paroz)
        * Galician (Fran Diéguez)
        * INDONESIA (Imam Musthaqim)
        * Slovenian (Matej Urbančič)

        New or updated manual translations:

        * Español (Jorge González)
        * German (Mario Blättermann)
        * Slovenian Translation (Matej Urbančič)
        * Swedish (Daniel Nylander)

version 2.14.0
--------------

	Bugs fixed:

	* #572564: Missing COPYING information.

	New or updated application translations:

	* Basque (Iñaki Larrañaga Murgoitio)
	* Estonian (Mattias Põldaru)
	* German (Mario Blättermann)
	* Hungarian (Gabor Kelemen)
	* Spanish (Francisco Javier F. Serrador)

	New or updated manual translations:

	* German (Mario Blättermann)

version 2.13.2
--------------

	New features and user visible changes:

	* Allow to add url launchers by dropping a file between launchers.
	  On the other hand if the file is dropped on a launcher than the
	  file will be opened with that launcher.
	* Allow to add url launchers by dragging a file on the preferences
	  dialog.
	* Added support for URL launchers.

	New or updated application translations:

	* Italian (Paolo Bacchilega)
	* Swedish (Daniel Nylander)

version 2.13.1
--------------

	New features and user visible changes:

	* Removed the animation when launching the application.
	* Support launching apps by dropping files on them (#171739).
	* Use the default panel launcher icon, if the icon is missing.
	* Allow to specify the number of rows (columns) explicitly. Patch by
	  Jared Warren (#161444).
	* Re-designed the "choose application from menu" dialog.
	* Replaced the old launcher dialog with a dialog similar to the one
	  used by gnome-panel.
	* Migrated the documentation to gnome-doc-utils.
	* Ported to gio.
	* Removed the libglade dependency.

	Bugs fixed:

	* #532174: Odd behaviour when changing the order of the icons.
	* #559584: does not build on GNOME 2.25 Patch by Migi.
	* #551786: applet still uses deprecated gnome-stock-about icon
	  instead of gtk-about. Patch by Pacho Ramos.

	New or updated application translations:

	* Aviary.pl (Piotr Drąg)
	* Dansk Gruppen (Kris Hansen)
	* German (Mario Blättermann)
	* Italian (Paolo Bacchilega)
	* Swedish (Daniel Nylander)

	New or updated manual translations:

	* French (Claude Paroz)

version 2.12.6
--------------

	Bugfixes:

	* #544888: LT_PROG_LIBTOOL should be AM_PROG_LIBTOOL?
	* #544850: configure needs to handle the non-existence of pangox
	* #531358: Applet height in vertical mode. Patch by Alexander
	  Kojevnikov.
	* interaction with b-a-s left over from previous sessions. Patch
	  by Philipp Kern.

	Updated application translations:

	* Arabeyes (Djihed Afifi)
	* Brazilian Portuguese (Fábio Nogueira)
	* Occitan (Yannig Marchegay (Kokoyaya))

version 2.12.5
--------------
	* Fixed bug #489133 – quick-lounge-applet crashes.
	* Translations: updated Korean, Occitan, Nepali; added Basque.

version 2.12.4
--------------
	* Ported over to GtkIconTheme. Use the 'screen' id.
	* Use GtkMessageDialog API. (#393731)
	* Building fails on Solaris lacking X_LIBS (#369265)
	* Add help response to open up suitable user guide link (#369103)
	* Fix all compile warnings and deprecated functions use.
	* Use po/LINGUAS as per GNOME Goal, and cleanup build system. (#393729)

version 2.12.3
--------------
	* Fixed bug #319050 - Dropdown shows on wrong head

version 2.12.2
--------------
	* Fixed bug #335628 - Don't eat the button press on the arrow button.
	* New translations: nb, vi.
	* Updated translations: ca, zh_CN, sv, ru.

version 2.12.1
--------------
	* Fixed bug #151612: Blank space between last icon and button should be
	  removed.
	* Fixed bug #317709: quick-lounge-applet crashes when clicking the
	  "more.." button
	* Fixed applet category.

version 2.12.0
--------------
	* Ported to libgnomemenu 2.12

version 2.10.1
--------------
	* Launch help when user clicks on the new launcher help button.
	* Draw the separators as vertical lines.
	* Updated translations: bg ca cs en_CA es fi gl hu nl sk

version 2.10.0
--------------
	* Fixed leaking of a gtk style.
	* Use GtkAboutDialog instead of the deprecated GnomeAbout.
	* Fixed a little memory leak.
	* Fixed DnD position computing when reordering launchers in the
	  preferences dialog.  Make moving of a launcher with the arrow
	  buttons more usable.
	* Use the stock 'New' icon for the 'Add new launcher' button.

version 2.9.1
-------------
	* It's now compatible with the upcoming GNOME 2.10
	* Use libgnome_menu to retrieve the list of available applications.
	* It's now called "Launchers List" instead of "Quick Lounge" in the
	  user interface.
	* A better icon.
	* Fixed bug #164319: patch to support panel transparency and pixmap
	  backgrounds.
	* Fixed bug #164417: Minimum size can be set more than Maximum in
	  quicklounge.

version 2.2.0
-------------
	* Updated translations: gu, bs, fi

version 2.1.2
-------------
	* Bug #144855: use g_build_filename instead of g_strconcat where
	  appropriate.
	* Bug #144594: Fix the applet crash when max value is changed and
	  dialog is closed quickly.
	* Added a tooltip to the arrow button.
	* Fix size request on vertical panels.
	* Bug #139587: Remove the un-neccesary check for icon_path. We any
	  fall back to gnome-unknown.png when we don't have one.
	* Bug #139939: "Quick Lounge Properties" dialog -- various
	  suggestions.
	* Bug #139938: Menu item: "Properties" should be "Preferences".

version 2.1.1
-------------
	* Allow to drag and drop the spaces in the properties dialog.

version 2.1.0
-------------
	* Make the applet work with the GNOME 2.6 environment.
	* Updated the manual and added some localized versions.
	* Changed applet type from shared library to executable to avoid
	  localization weirdness.
	* Added the same launch animation of the panel launcher.
	* Removed the help items in the context menu to be consistent with
	  the panel launcher context menu.

version 2.0.3
-------------
	* Update the icon size when the panel size changes.
	* Do not compile with the DISABLE_DEPRECATED macros defined.

version 2.0.2
-------------
	* Fixed little bug.
	* Translations.

version 2.0.1
-------------
	* Fixed little bug.
	* Many translations.

version 2.0.0
-------------
	* Allow svg icons.

version 1.1.4
-------------
	* Restore correctly the minimum dimension when it's setted to 0.

version 1.1.3
-------------
	* Multiscreen support.

version 1.1.2
-------------
	* Improve icon positioning and sizing.
	* Added keyword support.
	* Allow to set the minimum dimension to 0.

version 1.1.1
-------------
	* Update the icons when the icon theme changes.
	* Bugfixes.

version 1.1.0
-------------
	* Ported to the Gnome 2.2 platform.
	* HIG compliant dialogs.
	* Launched applications have session management support.

version 0.96
------------
	* Bugfixes.

version 0.93
------------
	* Added drag & drop support to the applet properties dialog.
	* Added "Add from menu" function, to add many launchers at once.
	* Launcher properties dialog uses revert/close buttons instead of
	  cancel/ok.
	* Added max size option, usefull for sliding panels.

version 0.87
------------
	* Added option to make icons size follow the panel size.
	* Remove the applet directory when the applet gets removed from the
	  panel.
	* Minor bugfixes.

version 0.75
------------
	* Added ability to add launchers, add spaces and remove spaces from
	  the context menu.
	* Size request and allocation now works properly.
	* Automatically create the applet folder.
	* Many other bugfixes.

version 0.5
-----------

	* First public version.
