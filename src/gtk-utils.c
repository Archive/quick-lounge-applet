/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <math.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>


GtkWidget *
_gtk_image_new_from_xpm_data (char * xpm_data[])
{
	GtkWidget *image;
	GdkPixbuf *pixbuf;

	pixbuf = gdk_pixbuf_new_from_xpm_data ((const char**) xpm_data);
	image = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (G_OBJECT (pixbuf));

	return image;
}


GtkWidget *
_gtk_image_new_from_inline (const guint8 *data)
{
	GtkWidget *image;
	GdkPixbuf *pixbuf;

	pixbuf = gdk_pixbuf_new_from_inline (-1, data, FALSE, NULL);
	image = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (G_OBJECT (pixbuf));

	return image;
}


GtkWidget*
_gtk_message_dialog_new (GtkWindow        *parent,
			 GtkDialogFlags    flags,
			 const char       *stock_id,
			 const char       *message,
			 const char       *secondary_message,
			 const gchar      *first_button_text,
			 ...)
{
	GtkWidget    *dialog;
	GtkWidget    *label;
	GtkWidget    *image;
	GtkWidget    *hbox;
	va_list       args;
	const gchar  *text;
	int           response_id;
	char         *markup_text;

	g_return_val_if_fail ((message != NULL) || (secondary_message != NULL), NULL);

	if (stock_id == NULL)
		stock_id = GTK_STOCK_DIALOG_INFO;

	dialog = gtk_dialog_new_with_buttons ("", parent, flags, NULL);

	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);
	gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), 6);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 8);

	/* Add label and image */

	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);

	label = gtk_label_new ("");

	if (message != NULL) {
		char *escaped_message;

		escaped_message = g_markup_escape_text (message, -1);
		if (secondary_message != NULL) {
			char *escaped_secondary_message = g_markup_escape_text (secondary_message, -1);
			markup_text = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
						       escaped_message,
						       escaped_secondary_message);
			g_free (escaped_secondary_message);
		}
		else
			markup_text = g_strdup (escaped_message);
		g_free (escaped_message);
	}
	else
		markup_text = g_markup_escape_text (secondary_message, -1);

	gtk_label_set_markup (GTK_LABEL (label), markup_text);
	g_free (markup_text);

	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);

	hbox = gtk_hbox_new (FALSE, 24);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);

	gtk_box_pack_start (GTK_BOX (hbox), image,
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (hbox), label,
			    TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
			    hbox,
			    FALSE, FALSE, 0);

	gtk_widget_show_all (hbox);

	/* Add buttons */

	if (first_button_text == NULL)
		return dialog;

	va_start (args, first_button_text);

	text = first_button_text;
	response_id = va_arg (args, gint);

	while (text != NULL) {
		gtk_dialog_add_button (GTK_DIALOG (dialog), text, response_id);

		text = va_arg (args, char*);
		if (text == NULL)
			break;
		response_id = va_arg (args, int);
	}

	va_end (args);

	return dialog;
}


static GtkWidget *
create_button (const char *stock_id,
	       const char *text)
{
	GtkWidget    *button;
	GtkWidget    *hbox;
	GtkWidget    *image;
	GtkWidget    *label;
	GtkWidget    *align;
	char         *label_text;
	gboolean      text_is_stock;
	GtkStockItem  stock_item;

	button = gtk_button_new ();

	if (gtk_stock_lookup (text, &stock_item)) {
		label_text = stock_item.label;
		text_is_stock = TRUE;
	} else {
		label_text = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
		text_is_stock = FALSE;
	}

	if (text_is_stock)
		image = gtk_image_new_from_stock (text, GTK_ICON_SIZE_BUTTON);
	else
		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
	label = gtk_label_new_with_mnemonic (label_text);
	hbox = gtk_hbox_new (FALSE, 2);
	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);

	if (! text_is_stock)
		g_free (label_text);

	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (button), align);
	gtk_container_add (GTK_CONTAINER (align), hbox);

	gtk_widget_show_all (button);

	return button;
}

char *
_gtk_request_dialog_run (GtkWindow        *parent,
			 GtkDialogFlags    flags,
			 const char       *message,
			 const char       *default_value,
			 int               max_length,
			 const char       *no_button_text,
			 const char       *yes_button_text)
{
	GtkWidget    *d;
	GtkWidget    *label;
	GtkWidget    *image;
	GtkWidget    *hbox;
	GtkWidget    *vbox;
	GtkWidget    *entry;
	GtkWidget    *button;
	GtkStockItem  item;
	char         *title;
	char         *stock_id;
	char         *result = NULL;

	stock_id = GTK_STOCK_DIALOG_QUESTION;
	if (gtk_stock_lookup (stock_id, &item))
		title = item.label;
	else
		title = _("Quick Lounge");

	d = gtk_dialog_new_with_buttons (title, parent, flags, NULL);
	gtk_window_set_resizable (GTK_WINDOW (d), FALSE);

	/* Add label and image */

	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);

	label = gtk_label_new (message);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

	entry = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (entry), max_length);
	gtk_entry_set_text (GTK_ENTRY (entry), default_value);
	gtk_entry_set_activates_default (GTK_ENTRY (entry), TRUE);

	hbox = gtk_hbox_new (FALSE, 6);
	vbox = gtk_vbox_new (FALSE, 6);

	gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);

	gtk_box_pack_start (GTK_BOX (hbox), image,
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (hbox), label,
			    TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), hbox,
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), entry,
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (d)->vbox),
			    vbox,
			    FALSE, FALSE, 0);

	gtk_widget_show_all (vbox);

	/* Add buttons */

	button = create_button (GTK_STOCK_CANCEL, no_button_text);
	gtk_dialog_add_action_widget (GTK_DIALOG (d),
				      button,
				      GTK_RESPONSE_CANCEL);

	/**/

	button = create_button (GTK_STOCK_OK, yes_button_text);
	gtk_dialog_add_action_widget (GTK_DIALOG (d),
				      button,
				      GTK_RESPONSE_YES);

	/**/

	gtk_dialog_set_default_response (GTK_DIALOG (d),
					 GTK_RESPONSE_YES);
	gtk_widget_grab_focus (entry);

	/* Run dialog */

	if (gtk_dialog_run (GTK_DIALOG (d)) == GTK_RESPONSE_YES) {
		const char *text;
		text = gtk_entry_get_text (GTK_ENTRY (entry));
		result = g_locale_from_utf8 (text, -1, NULL, NULL, NULL);
	} else
		result = NULL;

	gtk_widget_destroy (d);

	return result;
}


void
_gtk_error_dialog_from_gerror_run (GtkWindow   *parent,
				   const char  *message,
				   GError     **gerror)
{
	GtkWidget *d;

	g_return_if_fail (*gerror != NULL);


	d = _gtk_message_dialog_new (parent,
				     GTK_DIALOG_DESTROY_WITH_PARENT,
				     GTK_STOCK_DIALOG_ERROR,
				     message,
				     (*gerror)->message,
				     GTK_STOCK_OK, GTK_RESPONSE_OK,
				     NULL);
	gtk_dialog_run (GTK_DIALOG (d));

	gtk_widget_destroy (d);
	g_clear_error (gerror);
}


void
_gtk_error_dialog_run (GtkWindow        *parent,
		       const gchar      *format,
		       ...)
{
	GtkWidget *d;
	char      *message;
	va_list    args;

	va_start (args, format);
	message = g_strdup_vprintf (format, args);
	va_end (args);

	d =  gtk_message_dialog_new (parent,
				     GTK_DIALOG_MODAL,
				     GTK_MESSAGE_ERROR,
				     GTK_BUTTONS_OK,
				     "%s", message);
	g_free (message);

	gtk_dialog_run (GTK_DIALOG (d));
	gtk_widget_destroy (d);
}


void
_gtk_info_dialog_run (GtkWindow        *parent,
		      const gchar      *format,
		      ...)
{
	GtkWidget *d;
	char      *message;
	va_list    args;

	va_start (args, format);
	message = g_strdup_vprintf (format, args);
	va_end (args);

	d =  gtk_message_dialog_new (parent,
				     GTK_DIALOG_MODAL,
				     GTK_MESSAGE_INFO,
				     GTK_BUTTONS_CLOSE,
				     "%s", message);
	g_free (message);

	gtk_dialog_run (GTK_DIALOG (d));
	gtk_widget_destroy (d);
}


void
_gtk_entry_set_locale_text (GtkEntry   *entry,
			    const char *text)
{
	char *utf8_text;

	utf8_text = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	gtk_entry_set_text (entry, utf8_text);
	g_free (utf8_text);
}


char *
_gtk_entry_get_locale_text (GtkEntry *entry)
{
	const char *utf8_text;
	char       *text;

	utf8_text = gtk_entry_get_text (entry);
	if (utf8_text == NULL)
		return NULL;

	text = g_locale_from_utf8 (utf8_text, -1, NULL, NULL, NULL);

	return text;
}


void
_gtk_label_set_locale_text (GtkLabel   *label,
			    const char *text)
{
	char *utf8_text;

	utf8_text = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	gtk_label_set_text (label, utf8_text);
	g_free (utf8_text);
}


char *
_gtk_label_get_locale_text (GtkLabel *label)
{
	const char *utf8_text;
	char       *text;

	utf8_text = gtk_label_get_text (label);
	if (utf8_text == NULL)
		return NULL;

	text = g_locale_from_utf8 (utf8_text, -1, NULL, NULL, NULL);

	return text;
}



int
_gtk_icon_get_pixel_size (GtkWidget   *widget,
                          GtkIconSize  size)
{
	int icon_width, icon_height;

	gtk_icon_size_lookup_for_settings (gtk_widget_get_settings (widget),
                                           size,
                                           &icon_width, &icon_height);
	return MAX (icon_width, icon_height);
}


static GdkPixbuf *
create_internal_missing_pixbuf (int size)
{
        int        i, j;
        int        rowstride;
        guchar    *pixels;
        GdkPixbuf *pb;

        pb = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                             FALSE /* has alpha */,
                             8 /* bits per sample */,
                             size, size);
        pixels = gdk_pixbuf_get_pixels (pb);
        rowstride = gdk_pixbuf_get_rowstride (pb);

        for (i = 0; i < size; i++) {
                for (j = 0; j < size; j++) {
                        guchar *p = pixels + i * rowstride + 3 * j;
                        if (i == j || size - i == j) {
                                p[0] = 255;
                                p[1] = 0;
                                p[2] = 0;
                        } 
                        else {
                                p[0] = 255;
                                p[1] = 255;
                                p[2] = 255;
                        }
                }
        }

	return pb;
}


static gboolean
scale_keeping_ratio (int      *width,
		     int      *height,
		     int       max_width,
		     int       max_height,
		     gboolean  allow_upscaling)
{
	double   w = *width;
	double   h = *height;
	double   max_w = max_width;
	double   max_h = max_height;
	double   factor;
	int      new_width, new_height;
	gboolean modified;

	if ((*width < max_width) && (*height < max_height) && !allow_upscaling)
		return FALSE;

	factor = MIN (max_w / w, max_h / h);
	new_width  = MAX ((int) floor (w * factor + 0.50), 1);
	new_height = MAX ((int) floor (h * factor + 0.50), 1);

	modified = (new_width != *width) || (new_height != *height);

	*width = new_width;
	*height = new_height;

	return modified;
}


static GdkPixbuf *
create_missing_pixbuf (GtkIconTheme *theme, 
	               int           preffered_size)
{
	GtkIconInfo *icon_info;
	const char  *filename = NULL;
	GdkPixbuf   *pixbuf;
	int          w, h;
	
	icon_info = gtk_icon_theme_lookup_icon (theme,	
						"gnome-panel-launcher", /*"gtk-missing-image",*/
						preffered_size,
						0);
	if (icon_info != NULL)  
		filename = gtk_icon_info_get_filename (icon_info);
		
	if (filename != NULL)
		pixbuf = gdk_pixbuf_new_from_file (filename, NULL);
	else
		pixbuf = create_internal_missing_pixbuf (preffered_size);

	w = gdk_pixbuf_get_width (pixbuf);
	h = gdk_pixbuf_get_height (pixbuf);	
	if (scale_keeping_ratio (&w, &h, preffered_size, preffered_size, FALSE)) {
		GdkPixbuf *scaled;
		
		scaled = gdk_pixbuf_scale_simple (pixbuf, w, h, GDK_INTERP_BILINEAR);
		g_object_unref (pixbuf);
		pixbuf = scaled;
	}

	if (icon_info != NULL)
		gtk_icon_info_free (icon_info);
		
	return pixbuf;
}


/* This function from gnome-panel/panel-util.c
 * (C) 1997, 1998, 1999, 2000 The Free Software Foundation
 * Copyright 2000 Helix Code, Inc.
 * Copyright 2000,2001 Eazel, Inc.
 * Copyright 2001 George Lebl
 * Copyright 2002 Sun Microsystems Inc.
 *
 * Authors: George Lebl
 *          Jacob Berkman
 *          Mark McLoughlin
 *
 * Modified by Paolo Bacchilega for the Quick Lounge applet
 */
char *
panel_find_icon (GtkIconTheme *icon_theme,
                 const char   *icon_name,
                 gint          size)
{
	GtkIconInfo *icon_info;
        char *retval;
        char *icon_no_extension;
        char *p;

        if (icon_name == NULL || strcmp (icon_name, "") == 0)
                return NULL;

        if (g_path_is_absolute (icon_name)) {
                if (g_file_test (icon_name, G_FILE_TEST_EXISTS)) {
                        return g_strdup (icon_name);
                } 
                else {
                        char *basename;

			basename = g_path_get_basename (icon_name);
                        retval = panel_find_icon (icon_theme, basename, size);
                        g_free (basename);

                        return retval;
                }
        }

        /* This is needed because some .desktop files have an icon name *and*
         * an extension as icon */
        icon_no_extension = g_strdup (icon_name);
        p = strrchr (icon_no_extension, '.');
        if (p &&
            (strcmp (p, ".png") == 0 ||
             strcmp (p, ".xpm") == 0 ||
             strcmp (p, ".svg") == 0)) 
        {
            *p = 0;
        }

	icon_info = gtk_icon_theme_lookup_icon (icon_theme,
						icon_no_extension,
					        size,
					        0);

	if (icon_info != NULL) {
		retval = g_strdup (gtk_icon_info_get_filename (icon_info));
		gtk_icon_info_free (icon_info);
	} 
	else
		retval = NULL;

	g_free (icon_no_extension);

        return retval;
}


GdkPixbuf *
create_pixbuf (GtkIconTheme *icon_theme,
	       const char   *icon_name,
	       int           icon_size)
{
	char      *icon_path;
	GdkPixbuf *pixbuf;
	int        iw;
	int        ih;

	icon_path = panel_find_icon (icon_theme, icon_name, icon_size);
	if (icon_path == NULL)
		return NULL;

	pixbuf = gdk_pixbuf_new_from_file (icon_path, NULL);

	g_free (icon_path);

	if (pixbuf == NULL)
		return NULL;

	iw = gdk_pixbuf_get_width (pixbuf);
	ih = gdk_pixbuf_get_height (pixbuf);

	if ((iw > icon_size) || (ih > icon_size)) {
		GdkPixbuf *scaled;
		gdouble    factor;
		gdouble    max = icon_size;
		int        new_w, new_h;

		factor = MIN (max / iw, max / ih);
		new_w  = MAX ((int) (iw * factor), 1);
		new_h = MAX ((int) (ih * factor), 1);

		scaled = gdk_pixbuf_scale_simple (pixbuf,
						  new_w,
						  new_h,
						  GDK_INTERP_BILINEAR);
		g_object_unref (pixbuf);
		pixbuf = scaled;
	}

	return pixbuf;
}


GdkPixbuf *
create_pixbuf_or_missing (GtkIconTheme *icon_theme,
			  const char   *icon_name,
			  int           icon_size)
{
	GdkPixbuf *pixbuf;
	
	pixbuf = create_pixbuf (icon_theme, icon_name, icon_size);
	if (pixbuf == NULL)
		pixbuf = create_missing_pixbuf (icon_theme, icon_size);
		
	return pixbuf;
}


GtkWidget *
create_image (GtkIconTheme    *icon_theme,
	      const char      *icon_name,
	      int              icon_size)
{
	GtkWidget *icon = NULL;
	GdkPixbuf *pixbuf;

	pixbuf = create_pixbuf_or_missing (icon_theme, icon_name, icon_size);

	icon = gtk_image_new ();
	gtk_image_set_from_pixbuf (GTK_IMAGE (icon), pixbuf);
	g_object_unref (pixbuf);

	gtk_widget_show (icon);

	return icon;
}


static void
collect_children (GtkWidget *widget,
		  gpointer   data)
{
	GList **children = data;
	*children = g_list_prepend (*children, widget);
}


GList *
_gtk_container_get_all_children (GtkContainer *container)
{
	GList *children = NULL;

	gtk_container_forall (container,
			      collect_children,
			      &children);

	return g_list_reverse (children);
}


static void
count_children (GtkWidget *widget,
		gpointer   data)
{
	int *n = data;
	*n = *n + 1;
}


int
_gtk_container_get_n_children (GtkContainer *container)
{
	int n = 0;

	gtk_container_forall (container,
			      count_children,
			      &n);

	return n;
}


GtkBuilder *
_gtk_builder_new_from_file (const char *ui_file)
{
	char       *filename;
	GtkBuilder *builder;
	GError     *error = NULL;

	filename = g_build_filename (UI_DIR, ui_file, NULL);
	builder = gtk_builder_new ();
        if (! gtk_builder_add_from_file (builder, filename, &error)) {
                g_warning ("%s\n", error->message);
                g_clear_error (&error);
        }
	g_free (filename);

        return builder;
}


GtkWidget *
_gtk_builder_get_widget (GtkBuilder *builder,
			 const char *name)
{
	return (GtkWidget *) gtk_builder_get_object (builder, name);
}


void
_gtk_show_help (GtkWindow  *parent,
		const char *manual,
		const char *section)
{
	char   *uri;
	GError *error = NULL;

	uri = g_strconcat ("ghelp:", manual, section ? "?" : NULL, section, NULL);
	if (! gtk_show_uri (gtk_window_get_screen (parent), uri, GDK_CURRENT_TIME, &error)) {
  		GtkWidget *dialog;

		dialog = _gtk_message_dialog_new (parent,
						  GTK_DIALOG_DESTROY_WITH_PARENT,
						  GTK_STOCK_DIALOG_ERROR,
						  _("Could not display help"),
						  error->message,
						  GTK_STOCK_OK, GTK_RESPONSE_OK,
						  NULL);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy),
				  NULL);

		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

		gtk_widget_show (dialog);

		g_clear_error (&error);
	}
	g_free (uri);
}


GtkWidget * 
_gtk_combo_box_new_with_texts (const char *first_text,
			       ...)
{
	GtkWidget  *combo_box;
	va_list     args;
	const char *text;
	
	combo_box = gtk_combo_box_new_text ();
	
	va_start (args, first_text);
	
	text = first_text;
	while (text != NULL) {
		gtk_combo_box_append_text (GTK_COMBO_BOX (combo_box), text);
		text = va_arg (args, const char *);
	}
	
	va_end (args);
	
	return combo_box;
}
