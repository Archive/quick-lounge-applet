/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "quick-separator.h"

static void     quick_separator_class_init      (QuickSeparatorClass  *klass);
static void     quick_separator_instance_init   (QuickSeparator    *quick_sep);
static void     quick_separator_size_request    (GtkWidget         *widget,
						 GtkRequisition    *requisition);
static gboolean quick_separator_expose          (GtkWidget         *widget,
						 GdkEventExpose    *event);
static gboolean quick_separator_button_press    (GtkWidget         *widget,
						 GdkEventButton	   *event);
static gboolean quick_separator_enter_notify    (GtkWidget         *widget,
						 GdkEventCrossing  *event);
static gboolean quick_separator_leave_notify    (GtkWidget         *widget,
						 GdkEventCrossing  *event);
static void     quick_separator_button_pressed  (GtkButton         *button);
static void     quick_separator_button_released (GtkButton         *button);
static void     quick_separator_set_pixmap      (QuickButton       *button,
						 const char        *icon_path,
						 int                size);

static QuickButtonClass *parent_class;

GType
quick_separator_get_type (void)
{
	static GType object_type = 0;

	if (object_type == 0) {
		static const GTypeInfo object_info = {
			sizeof (QuickSeparatorClass),
                    	(GBaseInitFunc)         NULL,
                    	(GBaseFinalizeFunc)     NULL,
                    	(GClassInitFunc)        quick_separator_class_init,
                    	NULL,                   /* class_finalize */
                    	NULL,                   /* class_data */
                   	sizeof (QuickSeparator),
                    	0,                      /* n_preallocs */
                    	(GInstanceInitFunc)     quick_separator_instance_init
		};

		object_type = g_type_register_static (QUICK_TYPE_BUTTON, 
						      "QuickSeparator", 
						      &object_info, 
						      0);
		parent_class = g_type_class_ref (QUICK_TYPE_BUTTON);
	}

	return object_type;
}


static void
quick_separator_class_init (QuickSeparatorClass *class)
{
	GtkWidgetClass   *widget_class = GTK_WIDGET_CLASS (class);
	GtkButtonClass   *button_class = GTK_BUTTON_CLASS (class);
	QuickButtonClass *quick_class = QUICK_BUTTON_CLASS (class);

	widget_class->size_request       = quick_separator_size_request;
	widget_class->button_press_event = quick_separator_button_press;
	widget_class->enter_notify_event = quick_separator_enter_notify;
	widget_class->leave_notify_event = quick_separator_leave_notify;
	widget_class->expose_event       = quick_separator_expose;

	button_class->pressed  = quick_separator_button_pressed;
	button_class->released = quick_separator_button_released;

	quick_class->set_pixmap = quick_separator_set_pixmap;
}


static void
quick_separator_instance_init (QuickSeparator *quick_sep)
{
	GTK_WIDGET_UNSET_FLAGS (quick_sep, GTK_CAN_FOCUS);
	quick_sep->orient = GTK_ORIENTATION_HORIZONTAL;
	quick_sep->panel_size = 0;
}


static void
quick_separator_size_request (GtkWidget         *widget,
			      GtkRequisition    *requisition)
{
	QuickButton    *quick_button = QUICK_BUTTON (widget);
	QuickSeparator *quick_sep = QUICK_SEPARATOR (widget);

	if (quick_sep->orient == GTK_ORIENTATION_HORIZONTAL) {
		requisition->width  = quick_button->size / 2;
		requisition->height = quick_sep->panel_size;
	} else {
		requisition->width  = quick_sep->panel_size;
		requisition->height = quick_button->size / 2;
	}
}


static gboolean
quick_separator_expose (GtkWidget         *widget,
			GdkEventExpose    *event)
{	
	QuickSeparator *quick_sep = QUICK_SEPARATOR (widget);

	if (!GTK_WIDGET_DRAWABLE (widget))
		return TRUE;

	if (quick_sep->orient == GTK_ORIENTATION_HORIZONTAL)
		gtk_paint_vline (widget->style, widget->window, GTK_WIDGET_STATE (widget),
				 &event->area, widget, "vseparator",
				 widget->allocation.y,
				 widget->allocation.y + widget->allocation.height - 1,
				 widget->allocation.x + (widget->allocation.width -
							 widget->style->xthickness) / 2);
	else
		gtk_paint_hline (widget->style, widget->window, GTK_WIDGET_STATE (widget),
				 &event->area, widget, "hseparator",
				 widget->allocation.x,
				 widget->allocation.x + widget->allocation.width - 1,
				 widget->allocation.y + (widget->allocation.height -
							 widget->style->ythickness) / 2);

	return TRUE;
}


static gboolean
quick_separator_button_press (GtkWidget      *widget, 
			      GdkEventButton *event)
{
	return FALSE;
}


static gboolean
quick_separator_enter_notify (GtkWidget        *widget, 
			      GdkEventCrossing *event)
{
	return TRUE;
}


static gboolean
quick_separator_leave_notify (GtkWidget        *widget, 
			      GdkEventCrossing *event)
{
	return TRUE;
}


static void
quick_separator_button_pressed (GtkButton *button)
{
}


static void
quick_separator_button_released (GtkButton *button)
{
}


GtkWidget *
quick_separator_new (int             size,
		     int             panel_size,
		     GtkOrientation  orient)
{
	GObject        *object;
	QuickButton    *button;
	QuickSeparator *sep;

	object = g_object_new (quick_separator_get_type (), NULL);

	button = QUICK_BUTTON (object);
	button->name = NULL;
	button->icon = NULL;
	button->size = size;
	sep = QUICK_SEPARATOR (object);
	sep->panel_size = panel_size;
	sep->orient = orient;
	
	return GTK_WIDGET (object);
}


static void
quick_separator_set_pixmap (QuickButton *button,
			    const char  *icon,
			    int          size)
{
	g_return_if_fail (QUICK_IS_BUTTON (button));
	g_return_if_fail (size > 0);

	button->size = size;
	gtk_widget_queue_resize (GTK_WIDGET (button));
}


void
quick_separator_set_size (QuickSeparator *quick_sep,
			  int             size,
			  int             panel_size)
{
	g_return_if_fail (QUICK_IS_SEPARATOR (quick_sep));
	g_return_if_fail (size > 0);

	QUICK_BUTTON (quick_sep)->size = size;
	quick_sep->panel_size          = panel_size;

	gtk_widget_queue_resize (GTK_WIDGET (quick_sep));
}


void
quick_separator_set_orient (QuickSeparator *quick_sep,
			    GtkOrientation  orient)
{
	g_return_if_fail (QUICK_IS_SEPARATOR (quick_sep));

	quick_sep->orient = orient;
	gtk_widget_queue_resize (GTK_WIDGET (quick_sep));
}
