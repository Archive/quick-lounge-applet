/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001, 2004 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <panel-applet.h>
#include <panel-applet-gconf.h>
#include "dlg-add-from-menu.h"
#include "dlg-properties.h"
#include "file-utils.h"
#include "gtk-utils.h"
#include "quick-box.h"
#include "quick-desktop-entry-dialog.h"
#include "quick-separator.h"
#include "quick-lounge.h"


#define APPLET_IID         "OAFIID:GNOME_QuickLoungeApplet"
#define APPLET_FACTORY_IID "OAFIID:GNOME_QuickLoungeApplet_Factory"
#define APPLET_UI          "GNOME_QuickLoungeApplet.xml"

#define PREFS_LOCATION         "location"
#define PREFS_ORDER            "order"
#define PREFS_MIN_VISIBLE_COLS "min_size"
#define PREFS_MAX_VISIBLE_COLS "max_size"
#define PREFS_ROWS             "rows"

#define BASE_DIR ".gnome2/quick-lounge"

#define DEFAULT_MIN_VISIBLE_COLS 4
#define DEFAULT_MAX_SIZE         20
#define DEFAULT_ROWS             1

#define ERROR_FORMAT "<span size=\"large\" weight=\"bold\">%s</span>\n\n%s"


enum {
	REVERT_BUTTON,
	QUICK_RESPONSE_REVERT
};


static GtkTargetEntry target_table[] = {
	{ "text/uri-list", 0, 0 }
};

static void
destroy_prop_dlg_cb (GtkWidget   *widget,
		     QuickLounge *quick_lounge)
{
	quick_lounge->prop_dialog = NULL;
}


static void
properties_verb_cb (BonoboUIComponent *uic,
		    QuickLounge       *quick_lounge,
		    const gchar       *verbname)
{
	if (quick_lounge->prop_dialog == NULL) {
		quick_lounge->prop_dialog = dlg_properties (quick_lounge,
							    NULL);
		g_signal_connect (G_OBJECT (quick_lounge->prop_dialog),
				  "destroy",
				  G_CALLBACK (destroy_prop_dlg_cb),
				  quick_lounge);
	}

	if (quick_lounge->prop_dialog != NULL) {
		gtk_window_set_screen (GTK_WINDOW (quick_lounge->prop_dialog),
				       gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));

		gtk_window_present (GTK_WINDOW (quick_lounge->prop_dialog));
	}
}


static void
help_verb_cb (BonoboUIComponent *uic,
	      QuickLounge       *quick_lounge,
	      const gchar       *verbname)
{
	_gtk_show_help (NULL, "quick-lounge", NULL);
}


static void
about_verb_cb (BonoboUIComponent *uic,
	       QuickLounge       *quick_lounge,
	       const gchar       *verbname)
{
	static GtkWidget *about_dialog = NULL;
	const char *authors[] = {
		"Paolo Bacchilega <paolo.bacchilega@libero.it>",
		NULL
	};
	const char *documenters [] = {
		"Sun GNOME Documentation Team",
		NULL
	};
	const char *translator_credits = _("translator_credits");

	if (about_dialog) {
		gtk_window_set_screen (GTK_WINDOW (about_dialog),
				       gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));
		gtk_window_present (GTK_WINDOW (about_dialog));
		return;
	}

	about_dialog = gtk_about_dialog_new ();

	g_object_set (about_dialog,
		      "name", _("Launchers List"),
		      "version", VERSION,
		      "copyright", "Copyright \xc2\xa9 2002-2009 Free Software Foundation, Inc.",
		      "comments", _("Organize your favorite applications on the Panel"),
		      "authors", authors,
		      "documenters", documenters,
			  "translator-credits", strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
		      "logo-icon-name", "quick-lounge-applet",
		      NULL);

	gtk_window_set_wmclass (GTK_WINDOW (about_dialog), "quick-lounge-applet", "Quick-lounge-applet");
	gtk_window_set_screen (GTK_WINDOW (about_dialog), gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));
	g_signal_connect (about_dialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &about_dialog);
	g_signal_connect (about_dialog, "response", G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (about_dialog);

}
static const BonoboUIVerb menu_verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("QLProperties",   properties_verb_cb),
	BONOBO_UI_UNSAFE_VERB ("QLHelp",         help_verb_cb),
	BONOBO_UI_UNSAFE_VERB ("QLAbout",        about_verb_cb),
	BONOBO_UI_VERB_END
};


static void
applet_destroy (GtkWidget    *applet,
		QuickLounge  *quick_lounge)
{
	/* free memory */

	if (quick_lounge->prop_dialog != NULL)
		gtk_widget_destroy (quick_lounge->prop_dialog);

	g_free (quick_lounge->location);
	g_object_unref (quick_lounge->icon_theme);
	g_free (quick_lounge);
}


static void
applet_change_orient (PanelApplet       *applet,
		      PanelAppletOrient  orientation,
		      QuickLounge       *quick_lounge)
{
	if (quick_lounge->orientation == orientation)
		return;
	quick_lounge->orientation = orientation;
	quick_box_set_orient (quick_lounge->quick_box, orientation);
}


static void
applet_change_size (PanelApplet   *applet,
		    int            size,
		    QuickLounge   *quick_lounge)
{
	if (quick_lounge->size != size) {
		quick_lounge->size = size;
		quick_box_set_size (quick_lounge->quick_box, size);
	}
}


static void
applet_change_background (PanelApplet               *applet,
			  PanelAppletBackgroundType  type,
			  GdkColor                  *color,
			  GdkPixmap                 *pixmap,
			  QuickLounge               *quick_lounge)
{
	/* taken from the Trash Applet */
	GtkRcStyle *rc_style;
	GtkStyle *style;

	/* reset style */
	gtk_widget_set_style (GTK_WIDGET (applet), NULL);
	rc_style = gtk_rc_style_new ();
	gtk_widget_modify_style (GTK_WIDGET (applet), rc_style);
	gtk_rc_style_unref (rc_style);

	switch (type) {
	case PANEL_COLOR_BACKGROUND:
		gtk_widget_modify_bg (GTK_WIDGET (applet),
				      GTK_STATE_NORMAL,
				      color);
		break;

	case PANEL_PIXMAP_BACKGROUND:
		style = gtk_style_copy (GTK_WIDGET (applet)->style);
		if (style->bg_pixmap[GTK_STATE_NORMAL] != NULL)
			g_object_unref (style->bg_pixmap[GTK_STATE_NORMAL]);
		style->bg_pixmap[GTK_STATE_NORMAL] = g_object_ref (pixmap);
		gtk_widget_set_style (GTK_WIDGET (applet), style);
		g_object_unref (style);
		break;

	case PANEL_NO_BACKGROUND:
	default:
		break;
	}
}


static void
applet_size_request (GtkWidget      *widget,
		     GtkRequisition *requisition,
		     QuickLounge    *quick_lounge)
{
	GtkRequisition  child_req;
	const int      *size_hints;
	int             len;

	gtk_widget_get_child_requisition (quick_lounge->applet, &child_req);
	size_hints = quick_box_get_size_hint_list (quick_lounge->quick_box, &len);
	panel_applet_set_size_hints (PANEL_APPLET (quick_lounge->applet),
				     size_hints,
				     len,
				     child_req.width);
}


static void
applet_size_allocate (GtkWidget      *widget,
		      GtkAllocation  *allocation,
		      QuickLounge    *quick_lounge)
{
	int size;

	switch (quick_lounge->orientation) {
	case PANEL_APPLET_ORIENT_LEFT:
	case PANEL_APPLET_ORIENT_RIGHT:
		size = allocation->width;
		break;
	case PANEL_APPLET_ORIENT_UP:
	case PANEL_APPLET_ORIENT_DOWN:
	default:
		size = allocation->height;
		break;
	}

	if (quick_lounge->size != size) {
		quick_lounge->size = size;
		quick_box_set_size (quick_lounge->quick_box, size);
	}
}


/* -- Button Context Menu -- */


typedef struct {
	QuickLounge *quick_lounge;
	GtkWidget   *button;
	int          position;
} EditItemData;


static void
edit_desktop_entry_dialog_destroy_cb (GtkWidget *widget,
				      gpointer   user_data)
{
	g_free (user_data);
}


static void
edit_desktop_entry_dialog_response_cb (GtkDialog *dialog,
				       int        response,
				       gpointer   user_data)
{
	switch (response) {
	case GTK_RESPONSE_HELP:
		_gtk_show_help (NULL, "user-guide", "gospanel-52");
		break;

	case QUICK_RESPONSE_REVERT:
		quick_desktop_entry_dialog_revert (QUICK_DESKTOP_ENTRY_DIALOG (dialog));
		break;

	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;

	default:
		break;
	}
}


static void
edit_desktop_entry_dialog_changed_cb (GtkDialog *dialog,
				      gboolean   reverted,
				      gpointer   user_data)
{
	EditItemData  *edit_data = user_data;
	GtkWidget     *button = edit_data->button;
	GKeyFile      *desktop_entry;
	GError        *error = NULL;
	char          *data;
	gsize          length;
	const char    *uri;
	GFile         *file;

	gtk_dialog_set_response_sensitive (dialog, QUICK_RESPONSE_REVERT, ! reverted);

	desktop_entry = quick_desktop_entry_dialog_get_entry (QUICK_DESKTOP_ENTRY_DIALOG (dialog), &error);
	if (error != NULL) {
		_gtk_error_dialog_from_gerror_run (GTK_WINDOW (dialog), _("Could not save the launcher"), &error);
		return;
	}

	if (desktop_entry == NULL)
		return;

	data = g_key_file_to_data (desktop_entry, &length, &error);
	if (data == NULL) {
		g_key_file_free (desktop_entry);
		_gtk_error_dialog_from_gerror_run (GTK_WINDOW (dialog), _("Could not save the launcher"), &error);
		return;
	}

	uri = g_object_get_data (G_OBJECT (edit_data->button), "uri");
	file = g_file_new_for_uri (uri);
	if (! g_write_file (file, FALSE, G_FILE_CREATE_NONE, data, length, NULL, &error)) {
		_gtk_error_dialog_from_gerror_run (GTK_WINDOW (dialog), _("Could not save the launcher"), &error);
	}
	else {
		quick_box_update_child (edit_data->quick_lounge->quick_box, (QuickButton*) button, desktop_entry);
		dlg_properties_update (edit_data->quick_lounge->prop_dialog);
	}

	g_object_unref (file);
	g_free (data);
	g_key_file_free (desktop_entry);
}


void
quick_lounge_util__item_properties (QuickLounge *quick_lounge,
				    GtkWidget   *button)
{
	EditItemData *data;
	GtkWidget    *dialog;
	GKeyFile     *desktop_entry;

	data = g_new0 (EditItemData, 1);
	data->quick_lounge = quick_lounge;
	data->button = button;

	desktop_entry = g_object_get_data (G_OBJECT (button), "desktop_entry");

	dialog = quick_desktop_entry_dialog_new (_("Launcher Properties"), NULL, desktop_entry);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_REVERT_TO_SAVED, QUICK_RESPONSE_REVERT);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), QUICK_RESPONSE_REVERT, FALSE);

	g_signal_connect (dialog,
			  "destroy",
			  G_CALLBACK (edit_desktop_entry_dialog_destroy_cb),
			  data);
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (edit_desktop_entry_dialog_response_cb),
			  data);
	g_signal_connect (dialog,
			  "changed",
			  G_CALLBACK (edit_desktop_entry_dialog_changed_cb),
			  data);

	gtk_window_set_screen (GTK_WINDOW (dialog), gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_present (GTK_WINDOW (dialog));
}


static void
item_remove_cb (GtkWidget *button)
{
	GtkWidget   *quick_box = button->parent;
	const char  *uri;
	QuickLounge *quick_lounge;

	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");

	uri = g_object_get_data (G_OBJECT (button), "uri");
	if (uri != NULL) {
		GFile *file;

		file = g_file_new_for_uri (uri);
		g_file_delete (file, NULL, NULL);
		g_object_unref (file);
	}

	gtk_container_remove (GTK_CONTAINER (quick_box), button);
	quick_lounge_save_order (quick_lounge);
	dlg_properties_update (quick_lounge->prop_dialog);
}


static void
item_move_cb (GtkWidget *button)
{
	QuickBox    *quick_box = QUICK_BOX (button->parent);
	QuickLounge *quick_lounge;

	quick_lounge = g_object_get_data (G_OBJECT (quick_box),
					  "quick_lounge");
	if (quick_lounge == NULL)
		return;

	if (quick_lounge->prop_dialog != NULL)
		dlg_properties_select_button (quick_lounge->prop_dialog,
					      button);
	else {
		quick_lounge->prop_dialog = dlg_properties (quick_lounge,
							    button);
		g_signal_connect (G_OBJECT (quick_lounge->prop_dialog),
				  "destroy",
				  G_CALLBACK (destroy_prop_dlg_cb),
				  quick_lounge);
	}
}


char *
quick_lounge_util__get_unique_uri (QuickLounge *quick_lounge)
{
	char *base_dir, *desktop_file, *uri;

	base_dir = quick_lounge_get_path (quick_lounge);
	desktop_file = get_unique_desktop_file (base_dir);
	uri = g_strconcat ("file://", desktop_file, NULL);
	g_free (desktop_file);
	g_free (base_dir);

	return uri;
}


/* -- */


gboolean
quick_lounge_add_launcher (QuickLounge  *quick_lounge,
			   GKeyFile     *desktop_entry,
			   int           position)
{
	gboolean  launcher_added = FALSE;
	GError   *error = NULL;
	char     *data;
	gsize     length;
	char     *uri;
	GFile    *file;

	if (desktop_entry == NULL)
		return FALSE;

	data = g_key_file_to_data (desktop_entry, &length, &error);
	if (data == NULL) {
		g_key_file_free (desktop_entry);
		_gtk_error_dialog_run (NULL,
				       ERROR_FORMAT,
				       _("Could not create the new launcher"),
				       error->message);
		g_clear_error (&error);
		return FALSE;
	}

	uri = quick_lounge_util__get_unique_uri (quick_lounge);
	file = g_file_new_for_uri (uri);
	if (! g_write_file (file, FALSE, G_FILE_CREATE_NONE, data, length, NULL, &error)) {
		_gtk_error_dialog_run (NULL,
				       ERROR_FORMAT,
				       _("Could not create the new launcher"),
				       error->message);
		g_clear_error (&error);
	}
	else {
		quick_box_add_button (quick_lounge->quick_box, uri, position);
		launcher_added = TRUE;
	}

	g_object_unref (file);
	g_free (uri);
	g_free (data);

	return launcher_added;
}


static void
new_desktop_entry_dialog_response_cb (GtkDialog *dialog,
				      int        response,
				      gpointer   user_data)
{
	EditItemData *edit_data = user_data;
	GKeyFile     *desktop_entry;
	GError       *error = NULL;

	switch (response) {
	case GTK_RESPONSE_HELP:
		_gtk_show_help (NULL, "user-guide", "gospanel-52");
		break;

	case GTK_RESPONSE_CANCEL:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;

	default:
		break;
	}

	if (response != GTK_RESPONSE_OK)
		return;

	desktop_entry = quick_desktop_entry_dialog_get_entry (QUICK_DESKTOP_ENTRY_DIALOG (dialog), &error);
	if (error != NULL) {
		_gtk_error_dialog_run (NULL,
				       ERROR_FORMAT,
				       _("Could not create the new launcher"),
				       error->message);
		g_clear_error (&error);
		return;
	}

	if (quick_lounge_add_launcher (edit_data->quick_lounge, desktop_entry, edit_data->position + 1)) {
		quick_lounge_save_order (edit_data->quick_lounge);
		dlg_properties_update (edit_data->quick_lounge->prop_dialog);
		gtk_widget_destroy (GTK_WIDGET (dialog));
	}

	g_key_file_free (desktop_entry);
}


void
quick_lounge_new_launcher (QuickLounge *quick_lounge,
			   int          position)
{
	EditItemData *data;
	GtkWidget    *dialog;

	data = g_new0 (EditItemData, 1);
	data->quick_lounge = quick_lounge;
	data->position = position;

	dialog = quick_desktop_entry_dialog_new (_("New Launcher"), NULL, NULL);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_OK, GTK_RESPONSE_OK);

	g_signal_connect (dialog,
			  "destroy",
			  G_CALLBACK (edit_desktop_entry_dialog_destroy_cb),
			  data);
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (new_desktop_entry_dialog_response_cb),
			  data);

	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_screen (GTK_WINDOW (dialog), gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));
	gtk_window_present (GTK_WINDOW (dialog));
}


static void
add_launcher_cb (GtkWidget *button)
{
	QuickBox         *quick_box = QUICK_BOX (button->parent);
	QuickLounge      *quick_lounge;
	int               pos;

	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");
	if (quick_lounge == NULL)
		return;

	pos = quick_box_get_child_position (quick_box, button);

	quick_lounge_new_launcher (quick_lounge, pos);
}


static void
add_from_menu_cb (GtkWidget *button)
{
	QuickBox    *quick_box = QUICK_BOX (button->parent);
	QuickLounge *quick_lounge;
	int          pos;

	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");
	if (quick_lounge == NULL)
		return;

	pos = quick_box_get_child_position (quick_box, button);
	dlg_add_from_menu (quick_lounge, pos);
}


static void
add_space_cb (GtkWidget *button)
{
	QuickBox    *quick_box = QUICK_BOX (button->parent);
	QuickLounge *quick_lounge;
	int          pos;

	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");
	if (quick_lounge == NULL)
		return;

	pos = quick_box_get_child_position (quick_box, button);
	quick_box_add_separator (quick_box, pos + 1);
	quick_lounge_save_order (quick_lounge);
	dlg_properties_update (quick_lounge->prop_dialog);
}


static void
execute_item_activate_cb (GtkWidget *item,
			  GtkWidget *button)
{
	QuickBox    *quick_box;
	QuickLounge *quick_lounge;

	quick_box = (QuickBox*) button->parent;
	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");
	if (quick_lounge == NULL)
		return;

	gtk_button_clicked (GTK_BUTTON (button));
}


static void
properties_activate_cb (GtkWidget *item,
			GtkWidget *button)
{
	QuickBox    *quick_box;
	QuickLounge *quick_lounge;

	quick_box = (QuickBox*) button->parent;
	quick_lounge = g_object_get_data (G_OBJECT (quick_box), "quick_lounge");
	if (quick_lounge == NULL)
		return;

	quick_lounge_util__item_properties (quick_lounge, button);
}


static void
populate_context_menu (GtkWidget *popup_menu,
		       GtkWidget *button,
		       gpointer   data)
{
	GtkWidget *item;

	if (! QUICK_IS_SEPARATOR (button)) {
		item = gtk_image_menu_item_new_with_mnemonic (_("_Launch"));
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU));
		gtk_widget_show_all (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
		g_signal_connect (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (execute_item_activate_cb),
				  button);

		item = gtk_image_menu_item_new_from_stock (GTK_STOCK_PROPERTIES, NULL);
		gtk_widget_show_all (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
		g_signal_connect (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (properties_activate_cb),
				  button);

		/**/

		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
		gtk_widget_show_all (item);
	}

	/**/

	item = gtk_image_menu_item_new_with_mnemonic (_("Add From M_enu..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), gtk_image_new_from_stock (GTK_STOCK_INDEX, GTK_ICON_SIZE_MENU));
	gtk_widget_show_all (item);

	g_signal_connect_swapped (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (add_from_menu_cb),
				  button);

	/**/

	item = gtk_image_menu_item_new_with_mnemonic (_("_New Launcher"));
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), gtk_image_new_from_stock (GTK_STOCK_NEW, GTK_ICON_SIZE_MENU));
	gtk_widget_show_all (item);

	g_signal_connect_swapped (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (add_launcher_cb),
				  button);

	/**/

	item = gtk_image_menu_item_new_with_mnemonic (_("Add _Separator"));
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), gtk_image_new_from_stock (GTK_STOCK_ADD, GTK_ICON_SIZE_MENU));
	gtk_widget_show_all (item);

	g_signal_connect_swapped (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (add_space_cb),
				  button);

	/**/

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	gtk_widget_show_all (item);

	/**/

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REMOVE, NULL);
	gtk_widget_show_all (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	g_signal_connect_swapped (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (item_remove_cb),
				  button);

	/**/

	item = gtk_image_menu_item_new_with_mnemonic (_("_Move"));
	gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
	gtk_widget_show_all (item);

	g_signal_connect_swapped (G_OBJECT (item),
				  "activate",
				  G_CALLBACK (item_move_cb),
				  button);
}


/* -- Drag & Drop -- */


static void
open_file_list_with_launcher (QuickLounge *quick_lounge,
			      int          pos,
			      GList       *list)
{
	GtkWidget *button;
	GKeyFile  *desktop_entry;

	button = quick_box_get_child_at_pos (quick_lounge->quick_box, pos);
	if (button == NULL)
		return;

	desktop_entry = g_object_get_data (G_OBJECT (button), "desktop_entry");
	if (desktop_entry != NULL)
		_g_desktop_entry_launch (desktop_entry, button, list);
}


static void
drag_data_received  (GtkWidget          *widget,
		     GdkDragContext     *context,
		     int                 x,
		     int                 y,
		     GtkSelectionData   *data,
		     guint               info,
		     guint               time,
		     gpointer            extra_data)
{
	QuickLounge  *quick_lounge = extra_data;
	QuickBox     *quick_box = QUICK_BOX (widget);
	GList        *list, *scan;
	int           pos;
	QuickDropPos  drop_position;
	gboolean      launcher_list_changed = FALSE;

	if (! ((data->length >= 0) && (data->format == 8))) {
		gtk_drag_finish (context, FALSE, FALSE, time);
		return;
	}

	gtk_drag_finish (context, TRUE, FALSE, time);

	pos = quick_box_get_drop_position (quick_box, x, y, &drop_position);
	list = get_file_list_from_url_list ((char *) data->data);

	if ((drop_position == QUICK_DROP_POSITION_INTO_OR_BEFORE)
	    || (drop_position == QUICK_DROP_POSITION_INTO_OR_AFTER))
	{
		if (! is_desktop_file (list->data)) {
			open_file_list_with_launcher (quick_lounge, pos, list);
			path_list_free (list);
			return;
		}
	}

	if ((drop_position == QUICK_DROP_POSITION_AFTER)
	    || (drop_position == QUICK_DROP_POSITION_INTO_OR_AFTER))
	{
		pos++;
	}

	for (scan = list; scan; scan = scan->next) {
		char *src_uri = scan->data;

		if (is_desktop_file (src_uri)) {
			char      *uri;
			GtkWidget *button;

			uri = g_build_filename (quick_lounge->location,
						file_name_from_path (src_uri),
						NULL);

			if (strcmp (src_uri, uri) == 0) {
				button = quick_box_get_child_from_uri (quick_box, uri);

				if (button != NULL) {
					int button_pos;
					button_pos = quick_box_get_child_position (quick_box, button);
					if (button_pos != -1) {
						quick_box_reorder_child (quick_box, button, pos);
						launcher_list_changed = TRUE;
					}
				}

				g_free (uri);
				continue;
			}

			g_free (uri);
			uri = quick_lounge_util__get_unique_uri (quick_lounge);

			if (! file_copy (src_uri, uri)) {
				g_free (uri);
				continue;
			}

			quick_box_add_button (quick_box, uri, pos++);
			launcher_list_changed = TRUE;

			g_free (uri);
		}
		else {
			GKeyFile *desktop_entry;

			desktop_entry = _g_desktop_entry_new_for_uri (src_uri);
			if (quick_lounge_add_launcher (quick_lounge, desktop_entry, pos))
				launcher_list_changed = TRUE;

			g_key_file_free (desktop_entry);
		}
	}

	if (launcher_list_changed) {
		quick_lounge_save_order (quick_lounge);
		dlg_properties_update (quick_lounge->prop_dialog);
	}

	path_list_free (list);
}


void
quick_lounge_save_order (QuickLounge *quick_lounge)
{
	PanelApplet *applet = PANEL_APPLET (quick_lounge->applet);
	GConfClient *client;
	char        *full_key;
	GList       *children, *scan;
	GSList      *uri_list = NULL;

	children = gtk_container_get_children (GTK_CONTAINER (quick_lounge->quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		char      *uri;

		if (QUICK_IS_SEPARATOR (child_widget)) {
			uri_list = g_slist_prepend (uri_list, g_strdup (""));
			continue;
		}

		uri = g_object_get_data (G_OBJECT (child_widget), "uri");
		if (uri != NULL)
			uri_list = g_slist_prepend (uri_list, g_strdup (file_name_from_path (uri)));
	}
	g_list_free (children);

	uri_list = g_slist_reverse (uri_list);

	full_key = panel_applet_gconf_get_full_key (applet, PREFS_ORDER);
	client = gconf_client_get_default ();
	gconf_client_set_list (client, full_key, GCONF_VALUE_STRING, uri_list,
			       NULL);
	g_free (full_key);

	g_slist_foreach (uri_list, (GFunc) g_free, NULL);
	g_slist_free (uri_list);
}


char *
quick_lounge_get_path (QuickLounge *quick_lounge)
{
	GFile *file;
	char  *path;

	file = g_file_new_for_uri (quick_lounge->location);
	path = g_file_get_path (file);

	g_object_unref (file);

	return path;
}


/* -- Load location -- */


static gboolean
path_in_list (GList *list, const char *path1)
{
	GList *scan;

	for (scan = list; scan; scan = scan->next) {
		char *path2 = scan->data;
		if (strcmp (path1, path2) == 0)
			return TRUE;
	}

	return FALSE;
}


static GList*
get_order_list (QuickLounge *quick_lounge)
{
	PanelApplet  *applet = PANEL_APPLET (quick_lounge->applet);
	GSList       *uri_list, *scan;
	GList        *result = NULL;
	GConfClient  *client;
	char         *full_key;

	full_key = panel_applet_gconf_get_full_key (applet, PREFS_ORDER);
	client = gconf_client_get_default ();
	uri_list = gconf_client_get_list (client, full_key, GCONF_VALUE_STRING,
					  NULL);
	g_free (full_key);

	/* GSList --> GList */

	for (scan = uri_list; scan; scan = scan->next) {
		const char *filename = scan->data;
		char       *uri;

		if (*filename == 0)
			uri = g_strdup ("");
		else
			uri = g_build_filename (quick_lounge->location,
						filename,
						NULL);

		result = g_list_prepend (result, uri);
	}
	result = g_list_reverse (result);

	g_slist_foreach (uri_list, (GFunc) g_free, NULL);
	g_slist_free (uri_list);

	return result;
}


static void
load_uri__step2 (GList    *files,
		 GList    *dirs,
		 GError   *error,
		 gpointer  data)
{
	QuickLounge *quick_lounge = data;
	QuickBox    *quick_box = quick_lounge->quick_box;
	GList       *folder_list = NULL;
	GList       *order_list = NULL;
	GList       *scan;

	quick_lounge->loading = FALSE;

	if (error != NULL) {
		g_error_free (error);
		return;
	}

	/**/

	order_list = get_order_list (quick_lounge);

	folder_list = path_list_dup (files);
	for (scan = folder_list; order_list && scan;) {
		char *path = scan->data;

		if (path_in_list (order_list, path)) {
			folder_list = g_list_remove_link (folder_list, scan);
			g_list_free (scan);
			g_free (path);

			scan = folder_list;
		}
		else
			scan = scan->next;
	}

	folder_list = g_list_concat (order_list, folder_list);

	gtk_widget_hide (GTK_WIDGET (quick_lounge->quick_box));

	for (scan = folder_list; scan; scan = scan->next) {
		char *uri = scan->data;

		if (*uri == 0)
			quick_box_add_separator (quick_box, -1);
		else if (is_desktop_file (uri))
			quick_box_add_button (quick_box, uri, -1);
	}

	path_list_free (folder_list);

	gtk_widget_show (GTK_WIDGET (quick_lounge->quick_box));

	quick_lounge_save_order (quick_lounge);
	dlg_properties_update (quick_lounge->prop_dialog);
}


void
quick_lounge_load_uri_async (QuickLounge  *quick_lounge,
			     const char   *uri)
{
	QuickBox     *quick_box = quick_lounge->quick_box;
	GList        *children, *scan;

	if (quick_lounge->loading)
		return;

	if (quick_lounge->location != uri) {
		g_free (quick_lounge->location);
		quick_lounge->location = g_strdup (uri);
	}

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		gtk_widget_destroy (child_widget);
	}
	g_list_free (children);

	quick_lounge->loading = TRUE;
	path_list_async_new (quick_lounge->location,
			     NULL,
			     load_uri__step2,
			     quick_lounge);
}


void
quick_lounge_load_uri_sync (QuickLounge  *quick_lounge,
			    const char   *uri)
{
	GList        *children = NULL, *scan;
	QuickBox     *quick_box = quick_lounge->quick_box;
	GList        *file_list = NULL, *dir_list = NULL;
	GList        *order_list = NULL;
	GList        *files;

	if (quick_lounge->location != uri) {
		g_free (quick_lounge->location);
		quick_lounge->location = g_strdup (uri);
	}

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		gtk_widget_destroy (child_widget);
	}
	g_list_free (children);

	if (! path_list_new (quick_lounge->location, &file_list, &dir_list))
		return;

	/**/

	order_list = get_order_list (quick_lounge);

	for (scan = file_list; order_list && scan;) {
		char *path = scan->data;

		if (path_in_list (order_list, path)) {
			file_list = g_list_remove_link (file_list, scan);
			g_list_free (scan);
			g_free (path);

			scan = file_list;
		} else
			scan = scan->next;
	}

	files = g_list_concat (order_list, file_list);

	gtk_widget_hide (GTK_WIDGET (quick_lounge->quick_box));

	for (scan = files; scan; scan = scan->next) {
		char *uri = scan->data;

		if (*uri == 0)
			quick_box_add_separator (quick_box, -1);
		else if (is_desktop_file (uri))
			quick_box_add_button (quick_box, uri, -1);
	}

	path_list_free (files);
	path_list_free (dir_list);

	gtk_widget_show (GTK_WIDGET (quick_lounge->quick_box));

	quick_lounge_save_order (quick_lounge);
	dlg_properties_update (quick_lounge->prop_dialog);
}


void
quick_lounge_load_uri (QuickLounge  *quick_lounge,
		       const char   *uri)
{
	quick_lounge_load_uri_sync (quick_lounge, uri);
}


gboolean
quick_lounge_set_min_visible_cols (QuickLounge *quick_lounge,
				   int          value)
{
	gboolean needs_update = FALSE;

	if (value == quick_box_get_min_visible_cols (quick_lounge->quick_box))
		return FALSE;
	if (value < 0)
		return FALSE;

	if (value > quick_box_get_max_visible_cols (quick_lounge->quick_box)) {
		quick_box_set_max_visible_cols (quick_lounge->quick_box, value);
		panel_applet_gconf_set_int (PANEL_APPLET (quick_lounge->applet),
					    PREFS_MAX_VISIBLE_COLS,
					    value,
					    NULL);
		needs_update = TRUE;
	}

	quick_box_set_min_visible_cols (quick_lounge->quick_box, value);
	panel_applet_gconf_set_int (PANEL_APPLET (quick_lounge->applet),
				    PREFS_MIN_VISIBLE_COLS,
				    value,
				    NULL);

	return needs_update;
}


gboolean
quick_lounge_set_max_size (QuickLounge *quick_lounge,
			   int          value)
{
	gboolean needs_update = FALSE;

	if (value == quick_box_get_max_visible_cols (quick_lounge->quick_box))
		return FALSE;
	if (value < 0)
		return FALSE;

	if (value < quick_box_get_min_visible_cols (quick_lounge->quick_box)) {
		quick_box_set_min_visible_cols (quick_lounge->quick_box, value);
		panel_applet_gconf_set_int (PANEL_APPLET (quick_lounge->applet),
					    PREFS_MIN_VISIBLE_COLS,
					    value,
					    NULL);
		needs_update = TRUE;
	}

	quick_box_set_max_visible_cols (quick_lounge->quick_box, value);
	panel_applet_gconf_set_int (PANEL_APPLET (quick_lounge->applet),
				    PREFS_MAX_VISIBLE_COLS,
				    value,
				    NULL);

	return needs_update;
}


void
quick_lounge_set_rows (QuickLounge *quick_lounge,
		       int          value)
{
	GError *error = NULL;

	quick_box_set_rows (quick_lounge->quick_box, value);
	panel_applet_gconf_set_int (PANEL_APPLET (quick_lounge->applet),
				    PREFS_ROWS,
				    value,
				    &error);

	if (error != NULL)
		_gtk_error_dialog_from_gerror_run (NULL, NULL, &error);
}


static char *
get_unique_dirname (void)
{
	static int  n = 0;
	char       *base_dir;
	char       *unique_dir = NULL;

	base_dir = g_build_filename (g_get_home_dir (), BASE_DIR, NULL);
	make_directory_tree_from_path (base_dir, 0700, NULL);

	while (unique_dir == NULL) {
		char     *path;
		char     *name;
		gboolean  exists;

		name = g_strdup_printf ("%d.%d", getpid (), n++);
		path = g_build_filename (base_dir, name, NULL);
		g_free (name);

		exists = path_is_dir (path);

		if (! exists)
			unique_dir = path;
		else
			g_free (path);
	}

	g_free (base_dir);
	make_directory_tree_from_path (unique_dir, 0700, NULL);

	return unique_dir;
}


static void
copy_default_items_to_uri (const char *to_uri)
{
	static char * default_items [] = {
		/* FIXME : put default items here. */
		"nautilus.desktop",
		"gnome-terminal.desktop",
		"yelp.desktop",
		NULL
	};
	int i;

	for (i = 0; default_items[i] != NULL; i++) {
		char *source_uri;
		char *dest_uri;

		source_uri = g_build_filename ("file://",
					       DATADIR,
					       "applications",
					       default_items[i],
					       NULL);
		dest_uri = g_build_filename (to_uri,
					     default_items[i],
					     NULL);

		file_copy (source_uri, dest_uri);

		g_free (source_uri);
		g_free (dest_uri);
	}
}


static gboolean
applet_button_press_cb (GtkWidget         *widget,
			GdkEventButton    *event)
{
	return FALSE;
}


static void
icon_theme_changed (GtkIconTheme *icon_theme,
		    QuickLounge  *quick_lounge)
{
	quick_box_icon_theme_changed (quick_lounge->quick_box);
}


static gboolean
quick_lounge_applet_fill (PanelApplet *applet)
{
	QuickLounge *quick_lounge;
	GError      *error = NULL;
	char        *uri;
	int          min_cols, max_size, rows;

	gtk_window_set_default_icon_name ("quick-lounge-applet");

	quick_lounge = g_new0 (QuickLounge, 1);

	quick_lounge->applet = GTK_WIDGET (applet);
	quick_lounge->size   = panel_applet_get_size (applet);
	quick_lounge->orientation = panel_applet_get_orient (applet);
	quick_lounge->icon_theme = gtk_icon_theme_get_default ();

	/**/

	panel_applet_add_preferences (applet, "/schemas/apps/quick-lounge-applet/prefs", &error);
	if (error != NULL) {
		g_warning ("%s", error->message);
		g_clear_error (&error);
	}

	min_cols = panel_applet_gconf_get_int (applet, PREFS_MIN_VISIBLE_COLS, &error);
	if ((error != NULL) || (min_cols < 0)) {
		g_clear_error (&error);
		min_cols = DEFAULT_MIN_VISIBLE_COLS;
	}

	max_size = panel_applet_gconf_get_int (applet, PREFS_MAX_VISIBLE_COLS, &error);
	if ((error != NULL) || (max_size <= 0)) {
		g_clear_error (&error);
		max_size = DEFAULT_MAX_SIZE;
	}

	rows = panel_applet_gconf_get_int (applet, PREFS_ROWS, &error);
	if ((error != NULL) || (rows <= 0)) {
		g_clear_error (&error);
		rows = DEFAULT_ROWS;
	}

	/**/

	quick_lounge->quick_box = QUICK_BOX (quick_box_new (quick_lounge->orientation, quick_lounge->size, quick_lounge->icon_theme));
	quick_box_set_min_visible_cols (quick_lounge->quick_box, min_cols);
	quick_box_set_max_visible_cols (quick_lounge->quick_box, max_size);
	quick_box_set_rows (quick_lounge->quick_box, rows);

	g_object_set_data (G_OBJECT (quick_lounge->quick_box),
			   "quick_lounge",
			   quick_lounge);

	quick_box_populate_menu_func (quick_lounge->quick_box,
				      populate_context_menu,
				      quick_lounge);

	gtk_drag_dest_set (GTK_WIDGET (quick_lounge->quick_box),
			   GTK_DEST_DEFAULT_ALL,
			   target_table, G_N_ELEMENTS (target_table),
			   GDK_ACTION_COPY | GDK_ACTION_MOVE);

	g_signal_connect (G_OBJECT (quick_lounge->quick_box),
			  "drag_data_received",
			  G_CALLBACK (drag_data_received),
			  quick_lounge);
	g_signal_connect (G_OBJECT (quick_lounge->icon_theme),
			  "changed",
			  G_CALLBACK (icon_theme_changed),
			  quick_lounge);

	/**/

	uri = panel_applet_gconf_get_string (applet,
					     PREFS_LOCATION,
					     &error);

	if ((error != NULL) || (uri == NULL) || (*uri == 0)) {
		char *path;

		g_clear_error (&error);

		path = get_unique_dirname ();
		uri = g_strconcat ("file://", path, NULL);
		g_free (path);

		error = NULL;
		panel_applet_gconf_set_string (applet, PREFS_LOCATION, uri,
					       &error);
		if (error != NULL)
			_gtk_error_dialog_from_gerror_run (NULL, NULL, &error);

		copy_default_items_to_uri (uri);
	}

	g_signal_connect (G_OBJECT (applet),
			  "button_press_event",
			  G_CALLBACK (applet_button_press_cb),
			  NULL);

	/**/

	gtk_container_add (GTK_CONTAINER (applet), GTK_WIDGET (quick_lounge->quick_box));

	panel_applet_set_flags (applet,
				PANEL_APPLET_EXPAND_MAJOR |
				PANEL_APPLET_EXPAND_MINOR |
				PANEL_APPLET_HAS_HANDLE);

	g_signal_connect (G_OBJECT (applet),
			  "destroy",
			  G_CALLBACK (applet_destroy),
			  quick_lounge);

	g_signal_connect (G_OBJECT (applet),
			  "change_orient",
			  G_CALLBACK (applet_change_orient),
			  quick_lounge);

	g_signal_connect (G_OBJECT (applet),
			  "change_size",
			  G_CALLBACK (applet_change_size),
			  quick_lounge);

	g_signal_connect (G_OBJECT (applet),
			  "change_background",
			  G_CALLBACK (applet_change_background),
			  quick_lounge);
	g_signal_connect (G_OBJECT (applet),
			  "size_request",
			  G_CALLBACK (applet_size_request),
			  quick_lounge);

	g_signal_connect (G_OBJECT (applet),
			  "size_allocate",
			  G_CALLBACK (applet_size_allocate),
			  quick_lounge);

	panel_applet_setup_menu_from_file (applet,
					   NULL,
					   APPLET_UI,
					   NULL,
					   menu_verbs,
					   quick_lounge);

	quick_lounge_load_uri (quick_lounge, uri);
	g_free (uri);

	gtk_widget_show (GTK_WIDGET (quick_lounge->quick_box));
	gtk_widget_show (GTK_WIDGET (applet));

	return TRUE;
}


static gboolean
quick_lounge_factory (PanelApplet *applet,
		      const  char *iid,
		      gpointer     data)
{
	if (strcmp (iid, APPLET_IID) == 0)
		return quick_lounge_applet_fill (applet);

	return FALSE;
}


PANEL_APPLET_BONOBO_FACTORY (APPLET_FACTORY_IID,
			     PANEL_TYPE_APPLET,
			     "quick-lounge-applet",
			     "0",
			     quick_lounge_factory,
			     NULL)
