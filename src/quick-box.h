/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef QUICK_BOX_H
#define QUICK_BOX_H

#include <glib.h>
#include <gtk/gtk.h>
#include <panel-applet.h>
#include "quick-button.h"
#include "quick-separator.h"

typedef enum {
	QUICK_DROP_POSITION_BEFORE,
	QUICK_DROP_POSITION_AFTER,
	QUICK_DROP_POSITION_INTO_OR_BEFORE,
	QUICK_DROP_POSITION_INTO_OR_AFTER
} QuickDropPos;

typedef void (*PopulateMenuFunc) (GtkWidget *popup_menu, 
				  GtkWidget *button,
				  gpointer   data);

#define QUICK_TYPE_BOX            (quick_box_get_type ())
#define QUICK_BOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), QUICK_TYPE_BOX, QuickBox))
#define QUICK_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), QUICK_TYPE_BOX, QuickBoxClass))
#define QUICK_IS_BOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), QUICK_TYPE_BOX))
#define QUICK_IS_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), QUICK_TYPE_BOX))
#define QUICK_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), QUICK_TYPE_BOX, QuickBoxClass))

typedef struct _QuickBox         QuickBox;
typedef struct _QuickBoxPrivate  QuickBoxPrivate;
typedef struct _QuickBoxClass    QuickBoxClass;

struct _QuickBox {
	GtkBox __parent;
	QuickBoxPrivate *priv;
};

struct _QuickBoxClass {
	GtkBoxClass __parent_class;
};

GType               quick_box_get_type             (void);
GtkWidget *         quick_box_new                  (PanelAppletOrient  orient,
						    int                size,
						    GtkIconTheme      *icon_theme);
void                quick_box_set_orient           (QuickBox          *quick_box, 
						    PanelAppletOrient  orient);
PanelAppletOrient   quick_box_get_orient           (QuickBox          *quick_box);
void                quick_box_set_size             (QuickBox          *quick_box, 
						    int                size);
int                 quick_box_get_size             (QuickBox          *quick_box);
void                quick_box_set_min_visible_cols (QuickBox          *quick_box,
						    int                cols);
int                 quick_box_get_min_visible_cols (QuickBox          *quick_box);
void                quick_box_set_max_visible_cols (QuickBox          *quick_box,
						    int                cols);
int                 quick_box_get_max_visible_cols (QuickBox          *quick_box);
void		    quick_box_set_rows		   (QuickBox	      *quick_box,
						    int 	       rows);
int                 quick_box_get_rows             (QuickBox          *quick_box);
const int *         quick_box_get_size_hint_list   (QuickBox          *quick_box,
						    int               *n_elements);
void                quick_box_reorder_child        (QuickBox          *quick_box,
						    GtkWidget         *child,
						    int                pos);
void                quick_box_populate_menu_func   (QuickBox          *quick_box,
						    PopulateMenuFunc   pm_func,
						    gpointer           data);
int                 quick_box_get_pointer_position (QuickBox          *quick_box, 
						    int                x, 
						    int                y);
int                 quick_box_get_drop_position    (QuickBox          *quick_box, 
						    int                x, 
						    int                y,
						    QuickDropPos      *drop_position);
GtkWidget *         quick_box_add_button           (QuickBox          *quick_box,
						    const char        *uri,
						    int                pos);
GtkWidget *         quick_box_add_separator        (QuickBox          *quick_box,
						    int                pos);
void                quick_box_update_child         (QuickBox          *quick_box,
						    QuickButton       *child,
						    GKeyFile          *desktop_entry);
void                quick_box_icon_theme_changed   (QuickBox          *quick_box);
int                 quick_box_get_child_position   (QuickBox          *quick_box,
						    GtkWidget         *child);
GtkWidget *         quick_box_get_child_at_pos     (QuickBox          *quick_box,
						    int                 pos);
GtkWidget *         quick_box_get_child_from_uri   (QuickBox          *quick_box,
						    char              *uri);
						    
#endif /* QUICK_BOX_H */
