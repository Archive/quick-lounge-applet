/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2009 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib/gi18n.h>
#include "file-utils.h"
#include "gtk-utils.h"
#include "quick-desktop-entry-dialog.h"


#define GET_WIDGET(name) _gtk_builder_get_widget (self->priv->builder, (name))


enum {
	CHANGED,
	LAST_SIGNAL
};

static gpointer parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };


struct _QuickDesktopEntryDialogPrivate {
	GtkBuilder       *builder;
	GtkWidget        *type_combobox;
	DesktopEntryType  entry_type;
	GKeyFile         *original;
	GKeyFile         *current;
};


static void
quick_desktop_entry_dialog_finalize (GObject *object)
{
	QuickDesktopEntryDialog *dialog;

	dialog = QUICK_DESKTOP_ENTRY_DIALOG (object);

	if (dialog->priv != NULL) {
		g_object_unref (dialog->priv->builder);
		g_free (dialog->priv);
		dialog->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
quick_desktop_entry_dialog_class_init (QuickDesktopEntryDialogClass *class)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (class);

	object_class = (GObjectClass*) class;
	object_class->finalize = quick_desktop_entry_dialog_finalize;

	/* signals */

	signals[CHANGED] = g_signal_new ("changed",
					 G_TYPE_FROM_CLASS (class),
					 G_SIGNAL_RUN_LAST,
					 G_STRUCT_OFFSET (QuickDesktopEntryDialogClass, changed),
  					 NULL, NULL,
					 g_cclosure_marshal_VOID__BOOLEAN,
					 G_TYPE_NONE,
					 1,
					 G_TYPE_BOOLEAN);
}


static void
quick_desktop_entry_dialog_init (QuickDesktopEntryDialog *dialog)
{
	dialog->priv = g_new0 (QuickDesktopEntryDialogPrivate, 1);
}


GType
quick_desktop_entry_dialog_get_type (void)
{
	static GType type = 0;

	if (! type) {
		GTypeInfo type_info = {
			sizeof (QuickDesktopEntryDialogClass),
			NULL,
			NULL,
			(GClassInitFunc) quick_desktop_entry_dialog_class_init,
			NULL,
			NULL,
			sizeof (QuickDesktopEntryDialog),
			0,
			(GInstanceInitFunc) quick_desktop_entry_dialog_init
		};

		type = g_type_register_static (GTK_TYPE_DIALOG,
					       "QuickDesktopEntryDialog",
					       &type_info,
					       0);
	}

	return type;
}


static void
quick_desktop_entry_dialog_changed (QuickDesktopEntryDialog *self)
{
	g_signal_emit (self, signals[CHANGED], 0, _g_desktop_entry_equal (self->priv->current, self->priv->original));
}


static void
type_combobox_changed_cb (GtkEditable             *editable,
			  QuickDesktopEntryDialog *self)
{
	g_key_file_set_boolean (self->priv->current,
				G_KEY_FILE_DESKTOP_GROUP,
				G_KEY_FILE_DESKTOP_KEY_TERMINAL,
				gtk_combo_box_get_active (GTK_COMBO_BOX (self->priv->type_combobox)) == 1);
	quick_desktop_entry_dialog_changed (self);
}


static void
name_entry_changed_cb (GtkEditable             *editable,
		       QuickDesktopEntryDialog *self)
{
	char **languages;

	languages = (char **) g_get_language_names ();

	g_key_file_set_locale_string (self->priv->current,
				      G_KEY_FILE_DESKTOP_GROUP,
				      G_KEY_FILE_DESKTOP_KEY_NAME,
				      languages[0],
				      gtk_entry_get_text (GTK_ENTRY (GET_WIDGET ("name_entry"))));
	quick_desktop_entry_dialog_changed (self);
}


static void
exec_entry_changed_cb (GtkEditable             *editable,
		       QuickDesktopEntryDialog *self)
{
	g_key_file_set_string (self->priv->current,
			       G_KEY_FILE_DESKTOP_GROUP,
			       G_KEY_FILE_DESKTOP_KEY_EXEC,
			       gtk_entry_get_text (GTK_ENTRY (GET_WIDGET ("exec_entry"))));
	quick_desktop_entry_dialog_changed (self);
}


static void
url_entry_changed_cb (GtkEditable             *editable,
		      QuickDesktopEntryDialog *self)
{
	g_key_file_set_string (self->priv->current,
			       G_KEY_FILE_DESKTOP_GROUP,
			       G_KEY_FILE_DESKTOP_KEY_URL,
			       gtk_entry_get_text (GTK_ENTRY (GET_WIDGET ("url_entry"))));
	quick_desktop_entry_dialog_changed (self);
}


static void
comment_entry_changed_cb (GtkEditable             *editable,
			  QuickDesktopEntryDialog *self)
{
	char **languages;

	languages = (char **) g_get_language_names ();

	g_key_file_set_locale_string (self->priv->current,
				      G_KEY_FILE_DESKTOP_GROUP,
				      G_KEY_FILE_DESKTOP_KEY_COMMENT,
				      languages[0],
				      gtk_entry_get_text (GTK_ENTRY (GET_WIDGET ("comment_entry"))));
	quick_desktop_entry_dialog_changed (self);
}


static void
quick_desktop_entry_dialog_update_icon (QuickDesktopEntryDialog *self,
					GKeyFile                *entry)
{
	char      *icon;
	int        size;
	GdkPixbuf *pixbuf;

	icon = g_key_file_get_string (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	size = _gtk_icon_get_pixel_size (GTK_WIDGET (self), GTK_ICON_SIZE_DIALOG);
	pixbuf = create_pixbuf_or_missing (gtk_icon_theme_get_default (), icon, size);
	gtk_image_set_from_pixbuf (GTK_IMAGE (GET_WIDGET ("icon_image")), pixbuf);

	g_object_unref (pixbuf);
	g_free (icon);
}


static void
icon_chooser_response_cb (GtkWidget               *widget,
			  int                      response,
			  QuickDesktopEntryDialog *self)
{
	GtkFileChooser *file_chooser = GTK_FILE_CHOOSER (widget);
	char           *path;

	if ((response == GTK_RESPONSE_CANCEL) || (response == GTK_RESPONSE_DELETE_EVENT)) {
		gtk_widget_destroy (widget);
		return;
	}

	path = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_chooser));
	if (path == NULL)
		return;

	g_key_file_set_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, path);
	quick_desktop_entry_dialog_update_icon (self, self->priv->current);

	quick_desktop_entry_dialog_changed (self);

	g_free (path);

	if (response == GTK_RESPONSE_OK)
		gtk_widget_destroy (GTK_WIDGET (file_chooser));
}


static void
icon_button_clicked_cb (GtkButton               *button,
			QuickDesktopEntryDialog *self)
{
	GtkWidget *icon_chooser;
	char      *icon;
	char      *icon_path;
	GFile     *file;
	char      *uri;
	gboolean   set_current = FALSE;

	icon_chooser = gtk_file_chooser_dialog_new (_("Select an Image"),
						    GTK_WINDOW (self),
						    GTK_FILE_CHOOSER_ACTION_OPEN,
						    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						    GTK_STOCK_APPLY, GTK_RESPONSE_APPLY,
						    GTK_STOCK_OK, GTK_RESPONSE_OK,
						    NULL);

	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (icon_chooser), FALSE);
	gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (icon_chooser),TRUE);
	gtk_dialog_set_default_response (GTK_DIALOG (icon_chooser), GTK_RESPONSE_OK);

	icon = g_key_file_get_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	icon_path = panel_find_icon (gtk_icon_theme_get_default (), icon, _gtk_icon_get_pixel_size (GTK_WIDGET (self), GTK_ICON_SIZE_DIALOG));
	if ((icon_path == NULL) || (icon_path[0] != '/')) {
		g_free (icon_path);
		icon_path = g_strdup ("/usr/share/pixmaps/");
		set_current = TRUE;
	}
	file = g_file_new_for_path (icon_path);
	uri = g_file_get_uri (file);
	if (set_current)
		gtk_file_chooser_set_current_folder_uri (GTK_FILE_CHOOSER (icon_chooser), uri);
	else
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (icon_chooser), uri);

	g_signal_connect (G_OBJECT (icon_chooser),
			  "response",
			  G_CALLBACK (icon_chooser_response_cb),
			  self);

	gtk_window_set_screen (GTK_WINDOW (icon_chooser), gtk_widget_get_screen (GTK_WIDGET (button)));
	gtk_window_set_modal (GTK_WINDOW (icon_chooser), TRUE);
	gtk_widget_show (icon_chooser);

	g_free (uri);
	g_object_unref (file);
	g_free (icon_path);
	g_free (icon);
}


static void
command_chooser_response_cb (GtkWidget               *widget,
			     int                      response,
			     QuickDesktopEntryDialog *self)
{
	GtkFileChooser *file_chooser = GTK_FILE_CHOOSER (widget);
	gboolean        changed = FALSE;

	if ((response == GTK_RESPONSE_CANCEL) || (response == GTK_RESPONSE_DELETE_EVENT)) {
		gtk_widget_destroy (widget);
		return;
	}

	if (response != GTK_RESPONSE_OK)
		return;

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION) {
		char *command;

		command = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_chooser));
		if (command != NULL) {
			g_key_file_set_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, command);
			gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("exec_entry")), command);
			changed = TRUE;
		}
	}
	else if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_LINK) {
		char *uri;

		uri = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (file_chooser));
		if (uri != NULL) {
			g_key_file_set_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, uri);
			gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("url_entry")), uri);
			changed = TRUE;
		}
	}

	if (! changed)
		return;

	quick_desktop_entry_dialog_changed (self);
	gtk_widget_destroy (GTK_WIDGET (file_chooser));
}


static void
browse_button_clicked_cb (GtkButton               *button,
			  QuickDesktopEntryDialog *self)
{
	GtkWidget *command_chooser;
	char      *path;
	GFile     *file;
	char      *uri;
	gboolean   set_current = FALSE;

	command_chooser = gtk_file_chooser_dialog_new ((self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION) ? _("Select a Command") : _("Select a File"),
						       GTK_WINDOW (self),
						       GTK_FILE_CHOOSER_ACTION_OPEN,
						       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						       GTK_STOCK_OK, GTK_RESPONSE_OK,
						       NULL);

	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (command_chooser), FALSE);
	gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (command_chooser),TRUE);
	gtk_dialog_set_default_response (GTK_DIALOG (command_chooser), GTK_RESPONSE_OK);

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION)
		path = g_key_file_get_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
	else if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_LINK)
		path = g_key_file_get_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, NULL);
	else
		path = NULL;

	if ((path == NULL) || (path[0] != '/')) {
		g_free (path);
		path = g_strdup (g_get_home_dir ());
		set_current = TRUE;
	}
	file = g_file_new_for_path (path);
	uri = g_file_get_uri (file);
	if (set_current)
		gtk_file_chooser_set_current_folder_uri (GTK_FILE_CHOOSER (command_chooser), uri);
	else
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (command_chooser), uri);

	g_signal_connect (G_OBJECT (command_chooser),
			  "response",
			  G_CALLBACK (command_chooser_response_cb),
			  self);

	gtk_window_set_screen (GTK_WINDOW (command_chooser), gtk_widget_get_screen (GTK_WIDGET (button)));
	gtk_window_set_modal (GTK_WINDOW (command_chooser), TRUE);
	gtk_widget_show (command_chooser);

	g_free (uri);
	g_object_unref (file);
	g_free (path);
}


static void
quick_desktop_entry_dialog_construct (QuickDesktopEntryDialog *self,
				      const char              *title,
				      GtkWindow               *parent,
				      GKeyFile                *entry)
{
	if (title != NULL)
    		gtk_window_set_title (GTK_WINDOW (self), title);
  	if (parent != NULL)
    		gtk_window_set_transient_for (GTK_WINDOW (self), parent);
    	gtk_window_set_resizable (GTK_WINDOW (self), TRUE);
	gtk_dialog_set_has_separator (GTK_DIALOG (self), FALSE);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (self)->vbox), 5);
	gtk_container_set_border_width (GTK_CONTAINER (self), 5);

  	quick_desktop_entry_dialog_set_entry (self, entry);
}


GtkWidget *
quick_desktop_entry_dialog_new (const char *title,
				 GtkWindow  *parent,
				 GKeyFile   *entry)
{
	QuickDesktopEntryDialog *self;

	self = g_object_new (QUICK_TYPE_DESKTOP_ENTRY_DIALOG, NULL);
	quick_desktop_entry_dialog_construct (self, title, parent, entry);

	return (GtkWidget *) self;
}


static void
quick_desktop_entry_dialog_update (QuickDesktopEntryDialog *self,
				   GKeyFile                *entry)
{


	char *name;
	char *comment;

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION) {
		gboolean  terminal;
		char     *exec;

		terminal = g_key_file_get_boolean (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, NULL);
		gtk_combo_box_set_active (GTK_COMBO_BOX (self->priv->type_combobox), terminal ? 1 : 0);

		exec = g_key_file_get_string (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
		gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("exec_entry")), exec ? exec : "");

		g_free (exec);
	}
	else if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_LINK) {
		char *url;

		url = g_key_file_get_string (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, NULL);
		gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("url_entry")), url ? url : "");

		g_free (url);
	}

	name = g_key_file_get_locale_string (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL);
	gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("name_entry")), name ? name : "");

	comment = g_key_file_get_locale_string (entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
	gtk_entry_set_text (GTK_ENTRY (GET_WIDGET ("comment_entry")), comment ? comment : "");

	g_free (comment);
	g_free (name);

	quick_desktop_entry_dialog_update_icon (self, entry);
}


void
quick_desktop_entry_dialog_set_entry (QuickDesktopEntryDialog *self,
				      GKeyFile                *entry)
{
	char      *type;
	GtkWidget *content;

	if (self->priv->original != NULL)
		g_key_file_free (self->priv->original);
	self->priv->original = _g_key_file_dup (entry);

	if (self->priv->current != NULL)
		g_key_file_free (self->priv->current);
	self->priv->current = _g_key_file_dup (entry);

	/**/

	type = g_key_file_get_string (self->priv->current, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, NULL);
	if (g_strcmp0 (type, G_KEY_FILE_DESKTOP_TYPE_APPLICATION) == 0)
		self->priv->entry_type = DESKTOP_ENTRY_TYPE_APPLICATION;
	else if (g_strcmp0 (type, G_KEY_FILE_DESKTOP_TYPE_LINK) == 0)
		self->priv->entry_type = DESKTOP_ENTRY_TYPE_LINK;
	else
		self->priv->entry_type = DESKTOP_ENTRY_TYPE_UNKNOWN;

	if (self->priv->builder != NULL) {
		g_object_unref (self->priv->builder);
		self->priv->builder = NULL;
	}

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_UNKNOWN)
		return;

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION) {
		self->priv->builder = _gtk_builder_new_from_file ("desktop-entry-editor.ui");

		self->priv->type_combobox = _gtk_combo_box_new_with_texts (_("Application"), _("Application in Terminal"), NULL);
		gtk_widget_show (self->priv->type_combobox);
		gtk_box_pack_start (GTK_BOX (GET_WIDGET ("type_box")), self->priv->type_combobox, TRUE, TRUE, 0);
		gtk_label_set_mnemonic_widget (GTK_LABEL (GET_WIDGET ("type_label")), self->priv->type_combobox);
	}
	else if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_LINK)
		self->priv->builder = _gtk_builder_new_from_file ("link-entry-editor.ui");

    	content = _gtk_builder_get_widget (self->priv->builder, "desktop_entry_editor");
    	gtk_container_set_border_width (GTK_CONTAINER (content), 5);
    	gtk_widget_show (content);
  	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (self)->vbox), content, TRUE, TRUE, 0);

	quick_desktop_entry_dialog_update (self, self->priv->current);

	if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_APPLICATION) {
		g_signal_connect (self->priv->type_combobox,
	  			  "changed",
	  			  G_CALLBACK (type_combobox_changed_cb),
	  			  self);
	  	g_signal_connect (GET_WIDGET ("exec_entry"),
	  			  "changed",
	  			  G_CALLBACK (exec_entry_changed_cb),
	  			  self);
	  	g_signal_connect (GET_WIDGET ("exec_browse_button"),
	  			  "clicked",
	  			  G_CALLBACK (browse_button_clicked_cb),
	  			  self);
	}
	else if (self->priv->entry_type == DESKTOP_ENTRY_TYPE_LINK) {
	  	g_signal_connect (GET_WIDGET ("url_entry"),
	  			  "changed",
	  			  G_CALLBACK (url_entry_changed_cb),
	  			  self);
		g_signal_connect (GET_WIDGET ("url_browse_button"),
	  			  "clicked",
	  			  G_CALLBACK (browse_button_clicked_cb),
	  			  self);
	}

  	g_signal_connect (GET_WIDGET ("name_entry"),
  			  "changed",
  			  G_CALLBACK (name_entry_changed_cb),
  			  self);
  	g_signal_connect (GET_WIDGET ("comment_entry"),
  			  "changed",
  			  G_CALLBACK (comment_entry_changed_cb),
  			  self);
  	g_signal_connect (GET_WIDGET ("icon_button"),
  			  "clicked",
  			  G_CALLBACK (icon_button_clicked_cb),
  			  self);

	g_free (type);
}


GKeyFile *
quick_desktop_entry_dialog_get_entry (QuickDesktopEntryDialog  *self,
				      GError                  **error)
{
	return _g_key_file_dup (self->priv->current);
}


void
quick_desktop_entry_dialog_revert (QuickDesktopEntryDialog  *self)
{
	if (self->priv->current != NULL)
		g_key_file_free (self->priv->current);
	self->priv->current = _g_key_file_dup (self->priv->original);

	quick_desktop_entry_dialog_update (self, self->priv->current);
	quick_desktop_entry_dialog_changed (self);
}
