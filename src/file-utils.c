/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <sys/types.h>
#include <unistd.h>
#include <gconf/gconf-client.h>
#include <glib/gi18n.h>
#include <gio/gdesktopappinfo.h>
#include "file-utils.h"
#include "gtk-utils.h"


/* -- path_list_async_new implementation -- */


typedef struct {
	GFile            *directory;
	GCancellable     *cancellable;
	PathListDoneFunc  done_func;
	gpointer          done_data;
	GList            *dirs;
	GList            *files;
	GError           *error;
} PathListData;


void
path_list_data_free (PathListData *pli)
{
	g_object_unref (pli->directory);
	path_list_free (pli->dirs);
	path_list_free (pli->files);
	g_free (pli);
}


void
path_list_async_done (PathListData *pli)
{
	if (pli->done_func != NULL)
		pli->done_func (pli->dirs, pli->files, pli->error, pli->done_data);
	path_list_data_free (pli);
}


static void
path_list_close_enumerator_cb (GObject      *source_object,
				     GAsyncResult *result,
				     gpointer      user_data)
{
	GFileEnumerator *enumerator = (GFileEnumerator *) source_object;
	PathListData    *pli = user_data;
	GError          *error = NULL;

	if (! g_file_enumerator_close_finish (enumerator, result, &error)) {
		if (pli->error == NULL)
			pli->error = g_error_copy (error);
		else
			g_clear_error (&error);
	}

	path_list_async_done (pli);
}


static void
path_list_files_ready_cb (GObject      *source_object,
			  GAsyncResult *result,
			  gpointer      user_data)
{
	GFileEnumerator *enumerator = (GFileEnumerator *) source_object;
	PathListData    *pli = user_data;
	GList           *children, *scan;

	pli->error = NULL;
	children = g_file_enumerator_next_files_finish (enumerator,
							result,
							&(pli->error));

	if (children == NULL) {
		g_file_enumerator_close_async (enumerator,
					       G_PRIORITY_DEFAULT,
					       pli->cancellable,
					       path_list_close_enumerator_cb,
					       pli);
		return;
	}

	for (scan = children; scan; scan = scan->next) {
		GFileInfo *info = scan->data;
		GFile     *file;

		file = g_file_get_child (pli->directory, g_file_info_get_name (info));
		if (file == NULL)
			continue;

		switch (g_file_info_get_file_type (info)) {
		case G_FILE_TYPE_DIRECTORY:
			pli->dirs = g_list_prepend (pli->dirs, g_file_get_uri (file));
			break;
		case G_FILE_TYPE_REGULAR:
			pli->files = g_list_prepend (pli->files, g_file_get_uri (file));
			break;
		default:
			break;
		}

		g_object_unref (file);
	}

	g_file_enumerator_next_files_async (enumerator,
					    128,
					    G_PRIORITY_DEFAULT,
					    pli->cancellable,
					    path_list_files_ready_cb,
					    pli);
}


static void
path_list_ready_cb (GObject      *source_object,
		    GAsyncResult *result,
		    gpointer      user_data)
{
	PathListData    *pli = user_data;
	GFileEnumerator *enumerator;

	enumerator = g_file_enumerate_children_finish (G_FILE (source_object), result, &(pli->error));
	if (enumerator == NULL) {
		path_list_async_done (pli);
		return;
	}

	g_file_enumerator_next_files_async (enumerator,
					    128,
					    G_PRIORITY_DEFAULT,
					    pli->cancellable,
					    path_list_files_ready_cb,
					    pli);
}


void
path_list_async_new (const gchar      *uri,
		     GCancellable     *cancellable,
		     PathListDoneFunc  f,
		     gpointer         data)
{
	PathListData *pli;

	pli = g_new0 (PathListData, 1);
	pli->directory = g_file_new_for_uri (uri);
	pli->cancellable = cancellable;
	pli->done_func = f;
	pli->done_data = data;

	g_file_enumerate_children_async (pli->directory,
					 "standard::*",
					 G_FILE_QUERY_INFO_NONE,
					 G_PRIORITY_DEFAULT,
					 pli->cancellable,
					 path_list_ready_cb,
					 pli);
}


static gboolean
uri_is_filetype (const char *uri,
		 GFileType   file_type)
{
	gboolean   result = FALSE;
	GFile     *file;
	GFileInfo *info;
	GError    *error = NULL;

	file = g_file_new_for_uri (uri);

	if (! g_file_query_exists (file, NULL)) {
		g_object_unref (file);
		return FALSE;
	}

	info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_TYPE, 0, NULL, &error);
	if (error == NULL) {
		result = (g_file_info_get_file_type (info) == file_type);
	}
	else {
		g_warning ("Failed to get file type for uri %s: %s", uri, error->message);
		g_error_free (error);
	}

	g_object_unref (info);
	g_object_unref (file);

	return result;
}


gboolean
uri_is_file (const char *uri)
{
	return uri_is_filetype (uri, G_FILE_TYPE_REGULAR);
}


gboolean
uri_is_dir (const char *uri)
{
	return uri_is_filetype (uri, G_FILE_TYPE_DIRECTORY);
}


gboolean
path_is_dir (const char *path)
{
	char     *uri;
	gboolean  result;

	uri = g_filename_to_uri (path, NULL, NULL);
	result = uri_is_dir (uri);
	g_free (uri);

	return result;
}


gboolean
file_copy (const char *source_uri,
	   const char *destination_uri)
{
	GFile    *source;
	GFile    *destination;
	gboolean  result;

	source = g_file_new_for_uri (source_uri);
	destination = g_file_new_for_uri (destination_uri);

	result = g_file_copy (source, destination, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, NULL);

	g_object_unref (source);
	g_object_unref (destination);

	return result;
}


gboolean
path_list_new (const char  *uri,
	       GList      **files,
	       GList      **dirs)
{
	GFile           *directory;
	GFileEnumerator *enumerator;
	GFileInfo       *info;

	directory = g_file_new_for_uri (uri);
	enumerator = g_file_enumerate_children (directory,
						"standard::*",
						G_FILE_QUERY_INFO_NONE,
						NULL,
						NULL);

	if (enumerator == NULL) {
		g_object_unref (directory);
		return FALSE;
	}

	while ((info = g_file_enumerator_next_file (enumerator, NULL, NULL)) != NULL) {
		GFile *file;

		file = g_file_get_child (directory, g_file_info_get_name (info));
		if (file == NULL)
			continue;

		switch (g_file_info_get_file_type (info)) {
		case G_FILE_TYPE_DIRECTORY:
			if (dirs != NULL)
				*dirs = g_list_prepend (*dirs, g_file_get_uri (file));
			break;
		case G_FILE_TYPE_REGULAR:
			if (files != NULL)
				*files = g_list_prepend (*files, g_file_get_uri (file));
			break;
		default:
			break;
		}

		g_object_unref (file);
		g_object_unref (info);
	}

	if (dirs != NULL)
		*dirs = g_list_reverse (*dirs);

	if (files != NULL)
		*files = g_list_reverse (*files);

	g_object_unref (enumerator);
	g_object_unref (directory);

	return TRUE;
}


GList *
path_list_dup (GList *list)
{
	GList *new_list = NULL;
	GList *scan;

	for (scan = list; scan; scan = scan->next)
		new_list = g_list_prepend (new_list, g_strdup ((char *) scan->data));

	return g_list_reverse (new_list);
}


void
path_list_free (GList *list)
{
	if (list == NULL)
		return;
	g_list_foreach (list, (GFunc) g_free, NULL);
	g_list_free (list);
}


const char *
file_name_from_path (const char *file_name)
{
	register gssize base;
	register gssize last_char;

	if (file_name == NULL)
		return NULL;

	if (file_name[0] == '\0')
		return "";

	last_char = strlen (file_name) - 1;

	if (file_name [last_char] == G_DIR_SEPARATOR)
		return "";

	base = last_char;
	while ((base >= 0) && (file_name [base] != G_DIR_SEPARATOR))
		base--;

	return file_name + base + 1;
}


gboolean
make_directory_tree (GFile    *dir,
		     mode_t    mode,
		     GError  **error)
{
	gboolean  success = TRUE;
	GFile    *parent;

	if ((dir == NULL) || g_file_query_exists (dir, NULL))
		return TRUE;

	parent = g_file_get_parent (dir);
	if (parent != NULL) {
		success = make_directory_tree (parent, mode, error);
		g_object_unref (parent);
		if (! success)
			return FALSE;
	}

	success = g_file_make_directory (dir, NULL, error);
	if ((error != NULL) && (*error != NULL) && g_error_matches (*error, G_IO_ERROR, G_IO_ERROR_EXISTS)) {
		g_clear_error (error);
		success = TRUE;
	}

	if (success)
		g_file_set_attribute_uint32 (dir,
					     G_FILE_ATTRIBUTE_UNIX_MODE,
					     mode,
					     0,
					     NULL,
					     NULL);

	return success;
}


gboolean
make_directory_tree_from_uri (const char  *uri,
			      mode_t       mode,
			      GError     **error)
{
	GFile  *dir;
	GError *priv_error = NULL;

	if (uri == NULL)
		return FALSE;

	if (error == NULL)
		error = &priv_error;

	dir = g_file_new_for_uri (uri);
	if (! make_directory_tree (dir, mode, error)) {
		g_warning ("could create directory %s: %s", uri, (*error)->message);
		if (priv_error != NULL)
			g_clear_error (&priv_error);
		return FALSE;
	}

	return TRUE;
}


gboolean
make_directory_tree_from_path (const char  *path,
				  mode_t       mode,
				  GError     **error)
{
	char     *uri;
	gboolean  result;

	uri = g_filename_to_uri (path, NULL, NULL);
	result = make_directory_tree_from_uri (uri, mode, error);
	g_free (uri);

	return result;
}


char *
get_unique_desktop_file (const char *base_dir)
{
	static int n = 0;

	while (TRUE) {
		char  *path;
		char  *name;

		name = g_strdup_printf ("QL.%d.%d.desktop", getpid (), n++);
		path = g_build_filename (base_dir, name, NULL);
		g_free (name);

		if (! g_file_test (path, G_FILE_TEST_EXISTS))
			return path;
		g_free (path);
	}
}


GHashTable *static_strings = NULL;


const char *
get_static_string (const char *s)
{
	const char *result;

	if (s == NULL)
		return NULL;

	if (static_strings == NULL)
		static_strings = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

	if (! g_hash_table_lookup_extended (static_strings, s, (gpointer*) &result, NULL)) {
		result = g_strdup (s);
		g_hash_table_insert (static_strings,
				     (gpointer) result,
				     GINT_TO_POINTER (1));
	}

	return result;
}


const char*
get_file_mime_type (const char *uri,
		    gboolean    fast_file_type)
{
	GFile      *file;
	GFileInfo  *info;
	GError     *err = NULL;
 	const char *result = NULL;

 	file = g_file_new_for_uri (uri);
	info = g_file_query_info (file,
				  fast_file_type ?
				  G_FILE_ATTRIBUTE_STANDARD_FAST_CONTENT_TYPE :
				  G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
				  0, NULL, &err);
	if (info == NULL) {
		g_warning ("could not get content type for %s: %s", uri, err->message);
		g_clear_error (&err);
	}
	else {
		result = get_static_string (g_file_info_get_content_type (info));
		g_object_unref (info);
	}

	g_object_unref (file);

	return result;
}


gboolean
is_desktop_file (const char *uri)
{
	const char *mime_type;

	mime_type = get_file_mime_type (uri, FALSE);
	if (mime_type == NULL)
		return FALSE;

	return ((strcmp (mime_type, "application/x-gnome-app-info") == 0)
		|| (strcmp (mime_type, "application/x-desktop") == 0));
}


GList *
get_file_list_from_url_list (char *url_list)
{
	GList *list = NULL;
	int    i;
	char  *url_start, *url_end;

	i = 0;
	url_start = url_list;
	while (url_list[i] != '\0')     {
		while ((url_list[i] != '\0')
		       && (url_list[i] != '\r')
		       && (url_list[i] != '\n')) i++;

		url_end = url_list + i;
		list = g_list_prepend (list, g_strndup (url_start, url_end - url_start));

		while ((url_list[i] != '\0')
		       && ((url_list[i] == '\r')
			   || (url_list[i] == '\n'))) i++;
		url_start = url_list + i;
	}

	return g_list_reverse (list);
}


gboolean
g_write_file (GFile             *file,
	      gboolean           make_backup,
	      GFileCreateFlags   flags,
	      void              *buffer,
	      gsize              count,
	      GCancellable      *cancellable,
	      GError           **error)
{
	gboolean       success;
	GOutputStream *stream;

	stream = (GOutputStream *) g_file_replace (file, NULL, make_backup, flags, cancellable, error);
	if (stream != NULL)
		success = g_output_stream_write_all (stream, buffer, count, NULL, cancellable, error);
	else
		success = FALSE;

	g_object_unref (stream);

	return success;
}


void
_g_key_file_cleanup (GKeyFile *file)
{
	/* workaround for a bug in Ubuntu */
	g_key_file_remove_key (file, G_KEY_FILE_DESKTOP_GROUP, "X-Ubuntu-Gettext-Domain", NULL);
}


GKeyFile *
_g_key_file_dup (GKeyFile *file)
{

	GKeyFile *new_file;

	if (file != NULL) {
		char  *data;
		gsize  length;

		new_file = g_key_file_new ();
		data = g_key_file_to_data (file, &length, NULL);
		g_key_file_load_from_data (new_file, data, length, G_KEY_FILE_KEEP_TRANSLATIONS, NULL);

		g_free (data);
	}
	else
		new_file = _g_desktop_entry_new (DESKTOP_ENTRY_TYPE_APPLICATION);

	return new_file;
}


GKeyFile *
_g_desktop_entry_new (DesktopEntryType type)
{
	GKeyFile *new_file;

	new_file = g_key_file_new ();
	g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, G_KEY_FILE_DESKTOP_TYPE_APPLICATION);
	g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, "Encoding", "UTF-8");
	g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, "Version", "1.0");

	switch (type) {
	case DESKTOP_ENTRY_TYPE_APPLICATION:
		g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, G_KEY_FILE_DESKTOP_TYPE_APPLICATION);
		break;
	case DESKTOP_ENTRY_TYPE_LINK:
		g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, G_KEY_FILE_DESKTOP_TYPE_LINK);
		break;
	default:
		break;
	}

	return new_file;
}


GKeyFile *
_g_desktop_entry_new_for_uri (const char *uri)
{
	GKeyFile  *new_file;
	GFile     *file;
	GFileInfo *info;
	char      *comment;

	new_file = _g_desktop_entry_new (DESKTOP_ENTRY_TYPE_LINK);
	g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, uri);

	file = g_file_new_for_uri (uri);
	info = g_file_query_info (file, "standard::icon,standard::display-name", 0, NULL, NULL);
	if (info != NULL) {
		GIcon *icon;
		char  *icon_name;

		g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, g_file_info_get_display_name (info));

		icon = g_file_info_get_icon (info);
		if (G_IS_THEMED_ICON (icon)) {
			GtkIconTheme  *icon_theme;
			char         **names;
			int            i;

			icon_theme = gtk_icon_theme_get_default ();
			g_object_get (G_OBJECT (icon), "names", &names, NULL);
			for (i = 0; names[i] != NULL; i++) {
				if (gtk_icon_theme_has_icon (icon_theme, names[i])) {
					icon_name = g_strdup (names[i]);
					break;
				}
			}

			g_strfreev (names);
		}
		else if (G_IS_FILE_ICON (icon)) {
			GFile *file;

			file = g_file_icon_get_file (G_FILE_ICON (icon));
			icon_name = g_file_get_path (file);
			g_object_unref (file);
		}
		else
			icon_name = NULL;

		if (icon_name != NULL)
			g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, icon_name);

		g_free (icon_name);
		g_object_unref (info);
	}
	g_object_unref (file);

	/* Translators: %s is a URI */
	comment = g_strdup_printf (_("Open '%s'"), uri);
	g_key_file_set_string (new_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, comment);

	g_free (comment);

	return new_file;
}


gboolean
_g_desktop_entry_equal (GKeyFile *file1,
			GKeyFile *file2)
{
	gboolean   terminal1;
	char      *value1;
	gboolean   terminal2;
	char      *value2;

	value1 = g_key_file_get_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, NULL);
	value2 = g_key_file_get_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	terminal1 = g_key_file_get_boolean (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, NULL);
	terminal2 = g_key_file_get_boolean (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, NULL);
	if (terminal1 != terminal2)
		return FALSE;

	value1 = g_key_file_get_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	value2 = g_key_file_get_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	value1 = g_key_file_get_locale_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL);
	value2 = g_key_file_get_locale_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	value1 = g_key_file_get_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
	value2 = g_key_file_get_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	value1 = g_key_file_get_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, NULL);
	value2 = g_key_file_get_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	value1 = g_key_file_get_locale_string (file1, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
	value2 = g_key_file_get_locale_string (file2, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
	if (g_strcmp0 (value1, value2) != 0) {
		g_free (value1);
		g_free (value2);
		return FALSE;
	}

	return TRUE;
}


void
_g_desktop_entry_launch (GKeyFile  *desktop_entry,
			 GtkWidget *source,
			 GList     *files)
{
	GKeyFile *key_file;
	char     *type;

	key_file = _g_key_file_dup (desktop_entry);

	type = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, NULL);
	if (g_strcmp0 (type, G_KEY_FILE_DESKTOP_TYPE_APPLICATION) == 0) {
		char                *path;
		GDesktopAppInfo     *app_info;
		GdkAppLaunchContext *context;
		GError              *error = NULL;

		path = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_PATH, NULL);
		if (path == NULL) {
			if (g_key_file_get_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, NULL)) {
				char *exec;

				exec = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
				if (exec == NULL)
					exec = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TRY_EXEC, NULL);

				if (exec != NULL) {
					char *first_space;
					char *command;

					first_space = strchr (exec, ' ');
					if (first_space != NULL)
						command = g_strndup (exec, first_space - exec);
					else
						command = g_strdup (exec);
					path = g_path_get_dirname (command);

					g_free (command);
					g_free (exec);
				}
			}

			if (path == NULL)
				path = g_strdup (g_get_home_dir ());

			g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_PATH, path);
		}

		g_free (path);

		app_info = g_desktop_app_info_new_from_keyfile (key_file);
		context = gdk_app_launch_context_new ();
		gdk_app_launch_context_set_screen (context, gtk_widget_get_screen (source));
		gdk_app_launch_context_set_timestamp (context, GDK_CURRENT_TIME);
		if (! g_app_info_launch_uris (G_APP_INFO (app_info), files, G_APP_LAUNCH_CONTEXT (context), &error))
			_gtk_error_dialog_from_gerror_run (NULL, _("Could not launch the application"), &error);

		g_object_unref (context);
		g_object_unref (app_info);
	}
	else if (g_strcmp0 (type, G_KEY_FILE_DESKTOP_TYPE_LINK) == 0) {
		char   *url;
		GError *error = NULL;

		url = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, NULL);
		if (! gtk_show_uri ((source != NULL ? gtk_widget_get_screen (source) : NULL), url, 0, &error))
			_gtk_error_dialog_from_gerror_run (NULL, _("Could not show the location"), &error);

		g_free (url);
	}

	g_key_file_free (key_file);
}
