/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001-2009 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gmenu-tree.h>
#include "dlg-add-from-menu.h"
#include "dlg-properties.h"
#include "gtk-utils.h"
#include "file-utils.h"


#define GET_WIDGET(name) _gtk_builder_get_widget (data->builder, (name))


enum {
	APP_URI_COLUMN = 0,
	APP_ICON_COLUMN,
	APP_NAME_COLUMN,
	APP_COMMENT_COLUMN,
	USE_APP_COLUMN,
	APP_N_COLUMNS
};


enum {
	DIRECTORY_MENU_POINTER_COLUMN = 0,
	DIRECTORY_MENU_FILE_COLUMN,
	DIRECTORY_MENU_ID_COLUMN,
	DIRECTORY_ICON_COLUMN,
	DIRECTORY_NAME_COLUMN,
	DIRECTORY_N_COLUMNS
};


typedef struct {
	QuickLounge   *quick_lounge;
	QuickBox      *quick_box;
	int            pos;
	GtkBuilder    *builder;
	GtkWidget     *dialog;
	GtkWidget     *directory_treeview;
	GtkWidget     *application_treeview;
	GtkTreeModel  *directory_model;
	GtkTreeModel  *application_model;
	GHashTable    *uris;
	GList         *tree_items;
} DialogData;


/* called when the main dialog is closed. */
static void
destroy_cb (GtkWidget  *widget,
	    DialogData *data)
{
	g_object_unref (data->builder);
	g_list_foreach (data->tree_items, (GFunc) gmenu_tree_item_unref, NULL);
	g_hash_table_destroy (data->uris);
	g_free (data);
}


/* called when the "help" button is pressed. */
static void
help_cb (GtkWidget  *widget,
	 DialogData *data)
{
	_gtk_show_help (GTK_WINDOW (data->dialog), "quick-lounge", "quick-lounge-usage-add-existing");
}


static gboolean
add_selected_applications (DialogData *data)
{
	gboolean  changed = FALSE;
	GList    *keys;
	GList    *scan;

	keys = g_hash_table_get_keys (data->uris);
	for (scan = keys; scan; scan = scan->next) {
		const char *path = scan->data;
		char       *src_uri;
		char       *dest_uri;

		src_uri = g_filename_to_uri (path, NULL, NULL);
		dest_uri = quick_lounge_util__get_unique_uri (data->quick_lounge);
		if (! file_copy (src_uri, dest_uri)) {
			g_free (src_uri);
			g_free (dest_uri);
			break;
		}

		quick_box_add_button (data->quick_box, dest_uri, ++data->pos);
		changed = TRUE;

		g_free (dest_uri);
		g_free (src_uri);
	}
	g_list_free (keys);

	return changed;
}


/* called when the "help" button is pressed. */
static void
ok_cb (GtkWidget  *widget,
       DialogData *data)
{
	if (add_selected_applications (data)) {
		quick_lounge_save_order (data->quick_lounge);
		dlg_properties_update (data->quick_lounge->prop_dialog);
	}

	gtk_widget_destroy (data->dialog);
}


static void
set_application_list (DialogData         *data,
		      GMenuTreeDirectory *menu_dir)
{
	GSList *items, *scan;

	gtk_list_store_clear (GTK_LIST_STORE (data->application_model));

	if (menu_dir == NULL)
		return;

	items = gmenu_tree_directory_get_contents (menu_dir);
	for (scan = items; scan; scan = scan->next) {
		GMenuTreeItem *item = scan->data;
		GtkTreeIter    iter;
		const char    *icon = NULL;
		GdkPixbuf     *image = NULL;

		switch (gmenu_tree_item_get_type (item)) {
		case GMENU_TREE_ITEM_ENTRY:
			icon = gmenu_tree_entry_get_icon (GMENU_TREE_ENTRY (item));
			if (icon != NULL) {
				int size;

				size = _gtk_icon_get_pixel_size (GTK_WIDGET (data->dialog), GTK_ICON_SIZE_BUTTON);
				image = create_pixbuf (data->quick_lounge->icon_theme, icon, size);
			}

			gtk_list_store_append (GTK_LIST_STORE (data->application_model), &iter);
			gtk_list_store_set (GTK_LIST_STORE (data->application_model), &iter,
					    APP_URI_COLUMN, gmenu_tree_entry_get_desktop_file_path (GMENU_TREE_ENTRY (item)),
					    APP_ICON_COLUMN, image,
					    APP_NAME_COLUMN, gmenu_tree_entry_get_name (GMENU_TREE_ENTRY (item)),
					    APP_COMMENT_COLUMN, gmenu_tree_entry_get_comment (GMENU_TREE_ENTRY (item)),
					    USE_APP_COLUMN, FALSE,
					    -1);

			if (image != NULL)
				g_object_unref (image);

			break;
		default:
			break;
		}

		gmenu_tree_item_unref (item);
	}
	g_slist_free (items);
}


static void
add_directories_from_dir (DialogData         *data,
			  const char         *menu_file,
			  GtkTreeIter        *tree_iter,
			  GMenuTreeDirectory *menu_dir,
			  gboolean            expand)
{
	QuickLounge *quick_lounge = data->quick_lounge;
	const char  *icon = NULL;
	GdkPixbuf   *image = NULL;
	GtkTreeIter  iter;
	GSList      *items, *scan;

	data->tree_items = g_list_prepend (data->tree_items, gmenu_tree_item_ref (menu_dir));

	icon = gmenu_tree_directory_get_icon (menu_dir);
	if (icon != NULL) {
		int size;

		size = _gtk_icon_get_pixel_size (GTK_WIDGET (data->dialog), GTK_ICON_SIZE_BUTTON);
		image = create_pixbuf_or_missing (quick_lounge->icon_theme, icon, size);
	}

	gtk_tree_store_append (GTK_TREE_STORE (data->directory_model), &iter, tree_iter);
	gtk_tree_store_set (GTK_TREE_STORE (data->directory_model), &iter,
			    DIRECTORY_MENU_POINTER_COLUMN, menu_dir,
			    DIRECTORY_MENU_FILE_COLUMN, menu_file,
			    DIRECTORY_MENU_ID_COLUMN, gmenu_tree_directory_get_menu_id (menu_dir),
			    DIRECTORY_ICON_COLUMN, image,
			    DIRECTORY_NAME_COLUMN, gmenu_tree_directory_get_name (menu_dir),
			    -1);

	if (image != NULL)
		g_object_unref (image);

	items = gmenu_tree_directory_get_contents (menu_dir);
	for (scan = items; scan; scan = scan->next) {
		GMenuTreeItem *item = scan->data;

		switch (gmenu_tree_item_get_type (item)) {
		case GMENU_TREE_ITEM_DIRECTORY:
			add_directories_from_dir (data, menu_file, &iter, GMENU_TREE_DIRECTORY (item), FALSE);
			break;
		default:
			break;
		}

		gmenu_tree_item_ref (item);
	}
	g_slist_free (items);

	if (expand) {
		GtkTreePath *path;

		path = gtk_tree_model_get_path (data->directory_model, &iter);
		gtk_tree_view_expand_row (GTK_TREE_VIEW (data->directory_treeview), path, FALSE);
		gtk_tree_path_free (path);
	}
}


static void
add_directories_from_file (DialogData *data,
			   const char *menu_file)
{
	GMenuTree          *menu_tree;
	GMenuTreeDirectory *menu_dir;

	menu_tree = gmenu_tree_lookup (menu_file, GMENU_TREE_FLAGS_INCLUDE_EXCLUDED | GMENU_TREE_FLAGS_INCLUDE_NODISPLAY);
	if (menu_tree == NULL)
		return;

	menu_dir = gmenu_tree_get_root_directory (menu_tree);
	add_directories_from_dir (data, menu_file, NULL, menu_dir, TRUE);
	gmenu_tree_item_unref (menu_dir);

	gmenu_tree_unref (menu_tree);
}


static int
directory_name_column_sort_func (GtkTreeModel *model,
				 GtkTreeIter  *a,
				 GtkTreeIter  *b,
				 gpointer      user_data)
{
	char *app1;
	char *app2;
	int   result;

	gtk_tree_model_get (model, a,
			    DIRECTORY_NAME_COLUMN, &app1,
			    -1);
	gtk_tree_model_get (model, b,
			    DIRECTORY_NAME_COLUMN, &app2,
			    -1);

	result = g_utf8_collate (app1, app2);

	g_free (app1);
	g_free (app2);

	return result;
}


static void
directory_treeview_selection_changed_cb (GtkTreeSelection *selection,
					 gpointer          user_data)
{
	DialogData         *data = user_data;
	GtkTreeIter         iter;
	GMenuTreeDirectory *menu_dir;

	if (! gtk_tree_selection_get_selected (selection, &data->directory_model, &iter))
		return;

	gtk_tree_model_get (data->directory_model, &iter,
			    DIRECTORY_MENU_POINTER_COLUMN, &menu_dir,
			    -1);

	set_application_list (data, menu_dir);
}


static void
directory_list_add_columns (DialogData  *data,
			    GtkTreeView *treeview)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection  *selection;

	column = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
	gtk_tree_view_column_set_sort_column_id (column, DIRECTORY_NAME_COLUMN);

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "pixbuf", DIRECTORY_ICON_COLUMN,
					     NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "text", DIRECTORY_NAME_COLUMN,
					     NULL);

	gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (data->directory_model), directory_name_column_sort_func, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (data->directory_model),
					      GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
					      GTK_SORT_ASCENDING);

	selection = gtk_tree_view_get_selection (treeview);
	g_signal_connect (selection,
			  "changed",
			  G_CALLBACK (directory_treeview_selection_changed_cb),
			  data);
}


static void
use_app_toggled (GtkCellRendererToggle *cell,
		 char                  *path_string,
		 gpointer               callback_data)
{
	DialogData  *data = callback_data;
	GtkTreePath *path;
	GtkTreeIter  iter;
	char        *uri;
	gboolean     active;

	path = gtk_tree_path_new_from_string (path_string);
	gtk_tree_model_get_iter (data->application_model, &iter, path);
	gtk_tree_path_free (path);

	gtk_tree_model_get (GTK_TREE_MODEL (data->application_model), &iter,
			    APP_URI_COLUMN, &uri,
			    USE_APP_COLUMN, &active,
			    -1);

	active = ! active;
	if (active) {
		if (! g_hash_table_lookup (data->uris, uri))
			g_hash_table_insert (data->uris, g_strdup (uri), GINT_TO_POINTER (1));
	}
	else
		g_hash_table_remove (data->uris, uri);

	gtk_list_store_set (GTK_LIST_STORE (data->application_model), &iter,
			    USE_APP_COLUMN, active,
			    -1);

	g_free (uri);
}


static int
application_name_column_sort_func (GtkTreeModel *model,
				   GtkTreeIter  *a,
				   GtkTreeIter  *b,
				   gpointer      user_data)
{
	char *app1;
	char *app2;
	int   result;

	gtk_tree_model_get (model, a,
			    APP_NAME_COLUMN, &app1,
			    -1);
	gtk_tree_model_get (model, b,
			    APP_NAME_COLUMN, &app2,
			    -1);

	result = g_utf8_collate (app1, app2);

	g_free (app1);
	g_free (app2);

	return result;
}


static void
application_list_add_columns (DialogData  *data,
			      GtkTreeView *treeview)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	column = gtk_tree_view_column_new ();

	renderer = gtk_cell_renderer_toggle_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "active", USE_APP_COLUMN,
					     NULL);
	/*g_object_set (G_OBJECT (renderer), "xalign", 0.5, NULL);*/
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

	g_signal_connect (G_OBJECT (renderer),
			  "toggled",
			  G_CALLBACK (use_app_toggled),
			  data);

	/**/

	column = gtk_tree_view_column_new ();
	/*gtk_tree_view_column_set_title (column, _("Application"));*/

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "pixbuf", APP_ICON_COLUMN,
					     NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "text", APP_NAME_COLUMN,
					     NULL);

	/*gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);*/
	gtk_tree_view_column_set_sort_column_id (column, APP_NAME_COLUMN);

	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
	/*gtk_tree_view_set_expander_column (treeview, column);
	gtk_tree_view_column_set_clickable (column, FALSE);*/

	/**/

	/*column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Description"));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "text", APP_COMMENT_COLUMN,
					     NULL);

	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);*/


	gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (data->application_model), application_name_column_sort_func, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (data->application_model),
					      GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
					      GTK_SORT_ASCENDING);
}


void
dlg_add_from_menu (QuickLounge  *quick_lounge,
		   int           pos)
{
	DialogData       *data;
	GtkWidget        *btn_ok;
	GtkWidget        *btn_cancel;
	GtkWidget        *btn_help;
	GtkTreeSelection *selection;
	GtkTreePath      *path;

	data = g_new0 (DialogData, 1);

	data->quick_lounge = quick_lounge;
	data->quick_box = QUICK_BOX (quick_lounge->quick_box);
	data->pos = pos;
	data->uris = g_hash_table_new_full (g_str_hash, g_str_equal, (GDestroyNotify) g_free, NULL);

	data->builder = _gtk_builder_new_from_file ("add-from-menu.ui");

	/* Get the widgets. */

	data->dialog = GET_WIDGET("from_menu_dialog");
	data->directory_treeview = GET_WIDGET("directory_treeview");
	data->application_treeview = GET_WIDGET("application_treeview");

	btn_cancel = GET_WIDGET("cancel_button");
	btn_ok = GET_WIDGET("ok_button");
	btn_help = GET_WIDGET("help_button");

	/* Set widgets data. */

	data->directory_model = GTK_TREE_MODEL (gtk_tree_store_new (DIRECTORY_N_COLUMNS,
								    G_TYPE_POINTER,
								    G_TYPE_STRING,
								    G_TYPE_STRING,
								    GDK_TYPE_PIXBUF,
								    G_TYPE_STRING));
	gtk_tree_view_set_model (GTK_TREE_VIEW (data->directory_treeview), data->directory_model);
	g_object_unref (data->directory_model);
	directory_list_add_columns (data, GTK_TREE_VIEW (data->directory_treeview));

	data->application_model = GTK_TREE_MODEL (gtk_list_store_new (APP_N_COLUMNS,
								      G_TYPE_STRING,
								      GDK_TYPE_PIXBUF,
								      G_TYPE_STRING,
								      G_TYPE_STRING,
								      G_TYPE_BOOLEAN));
	gtk_tree_view_set_model (GTK_TREE_VIEW (data->application_treeview), data->application_model);
	g_object_unref (data->application_model);
	application_list_add_columns (data, GTK_TREE_VIEW (data->application_treeview));

	/* Set the signals handlers. */

	g_signal_connect (G_OBJECT (data->dialog),
			  "destroy",
			  G_CALLBACK (destroy_cb),
			  data);
	g_signal_connect_swapped (G_OBJECT (btn_cancel),
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  data->dialog);
	g_signal_connect (G_OBJECT (btn_help),
			  "clicked",
			  G_CALLBACK (help_cb),
			  data);
	g_signal_connect (G_OBJECT (btn_ok),
			  "clicked",
			  G_CALLBACK (ok_cb),
			  data);

	/* run dialog. */

	g_object_set_data (G_OBJECT (data->dialog), "dialog_data", data);

	gtk_window_set_screen (GTK_WINDOW (data->dialog), gtk_widget_get_screen (GTK_WIDGET (quick_lounge->applet)));
	gtk_widget_show (data->dialog);

	add_directories_from_file (data, "applications.menu");
	add_directories_from_file (data, "settings.menu");

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->directory_treeview));
	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, 0);
	gtk_tree_path_append_index (path, 0);
	gtk_tree_selection_select_path (selection, path);
	gtk_tree_path_free (path);
}
