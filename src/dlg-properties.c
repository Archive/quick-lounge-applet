/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include "dlg-properties.h"
#include "dlg-add-from-menu.h"
#include "file-utils.h"
#include "gtk-utils.h"
#include "quick-lounge.h"
#include "quick-box.h"
#include "quick-button.h"
#include "quick-separator.h"

#define SEPARATOR_NAME         "---" /*_("Space")*/
#define ICON_SIZE              24
#define UPDATE_MAX_SIZE_DELAY  100
#define AUTO_SCROLL_STEP 4
#define AUTO_SCROLL_THRESHOLD 20
#define GET_WIDGET(name) _gtk_builder_get_widget (data->builder, (name))


enum {
	COLUMN_DATA,
	COLUMN_ICON,
	COLUMN_NAME,
	NUM_COLUMNS
};


enum {
	TEXT_URI_LIST,
	GTK_TREE_MODEL_ROW
};

static GtkTargetEntry source_target_table[] = {
	{ "GTK_TREE_MODEL_ROW", GTK_TARGET_SAME_WIDGET, GTK_TREE_MODEL_ROW }
};

static guint source_target_table_n = G_N_ELEMENTS(source_target_table);

static GtkTargetEntry dest_target_table[] = {
	{ "text/uri-list", 0, TEXT_URI_LIST },
	{ "GTK_TREE_MODEL_ROW", GTK_TARGET_SAME_WIDGET, GTK_TREE_MODEL_ROW }
};

static guint dest_target_table_n = G_N_ELEMENTS(dest_target_table);


typedef struct {
	QuickLounge   *quick_lounge;
	QuickBox      *quick_box;
	GtkWidget     *select_button;

	GtkBuilder    *builder;
	 
	GtkWidget     *dialog;
	GtkWidget     *tree_view;
	GtkWidget     *btn_up;
	GtkWidget     *btn_down;
	GtkWidget     *btn_top;
	GtkWidget     *btn_bottom;

	GtkWidget     *btn_edit;
	GtkWidget     *btn_add_launcher;
	GtkWidget     *btn_add_from_menu;
	GtkWidget     *btn_add_space;
	GtkWidget     *btn_delete;

	GtkWidget     *p_minsize_spinbutton;
	GtkWidget     *p_maxsize_spinbutton;
	GtkWidget     *p_rows_spinbutton;
	GtkWidget     *p_apps_scrolledwindow;
	GtkAdjustment *p_apps_vadjustment;

	int            n;
	GtkTreeModel  *model;

	int            timeout;
	int            start_x, start_y;
} DialogData;


static gboolean
change_max_size (gpointer user_data)
{
	DialogData *data = user_data;
	gboolean    needs_update;
	
	g_source_remove (data->timeout);
	data->timeout = 0;

	needs_update = quick_lounge_set_max_size (data->quick_lounge, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (data->p_maxsize_spinbutton)));
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_maxsize_spinbutton), quick_box_get_max_visible_cols (data->quick_box));
	if (needs_update)
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_minsize_spinbutton), quick_box_get_min_visible_cols (data->quick_box));
		
	return FALSE;
}


/* called when the main dialog is closed. */
static void
destroy_cb (GtkWidget  *widget, 
	    DialogData *data)
{
	if (data->timeout != 0) 
		change_max_size (data);
	g_object_unref (data->builder);
	g_free (data);
}


/* called when the "close" button is pressed. */
static void
close_cb (GtkWidget  *widget, 
	  DialogData *data)
{
	gtk_widget_destroy (data->dialog);
}


/* called when the "help" button is pressed. */
static void
help_cb (GtkWidget  *widget, 
	 DialogData *data)
{
	_gtk_show_help (GTK_WINDOW (data->dialog), "quick-lounge", "quick-lounge-prefs");
}


static void
do_select_iter (DialogData  *data,
	     GtkTreeIter *iter)
{
	GtkTreeSelection *selection;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;

	gtk_tree_selection_select_iter (selection, iter);
}


static void
select_and_view_iter (DialogData  *data,
		      GtkTreeIter *iter)
{
	GtkTreeSelection *selection;
	GtkTreePath      *tpath;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;

	gtk_tree_selection_select_iter (selection, iter);
	tpath = gtk_tree_model_get_path (data->model, iter);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (data->tree_view),
				      tpath, NULL, TRUE, 0.5, 0.0);
	gtk_tree_path_free (tpath);
}


static int
get_iter_position (DialogData  *data,
		   GtkTreeIter *iter)
{
	GtkTreePath *tpath;
	int          position;

	tpath = gtk_tree_model_get_path (data->model, iter);
	position = gtk_tree_path_get_indices (tpath)[0];
	gtk_tree_path_free (tpath);

	return position;
}


static GtkWidget *
tree_view_move_iter_to (DialogData  *data, 
			GtkTreeIter *iter,
			int          position,
			gboolean     after)
{
	GtkWidget  *button;
	GdkPixbuf  *pixbuf;
	char       *text;

	gtk_tree_model_get (data->model,
			    iter,
			    COLUMN_DATA, &button,
			    COLUMN_ICON, &pixbuf,
			    COLUMN_NAME, &text,
			    -1);
	gtk_list_store_remove (GTK_LIST_STORE (data->model), iter);

	if (after)
		gtk_list_store_append (GTK_LIST_STORE (data->model),
				       iter);
	else
		gtk_list_store_insert (GTK_LIST_STORE (data->model),
				       iter,
				       position);
	gtk_list_store_set (GTK_LIST_STORE (data->model), iter,
			    COLUMN_DATA, button,
			    COLUMN_ICON, pixbuf,
			    COLUMN_NAME, text,
			    -1);

	if (pixbuf != NULL)
		g_object_unref (pixbuf);
	g_free (text);

	return button;
}


static void
move_up_cb (GtkWidget  *widget, 
	    DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               position;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	position = get_iter_position (data, &iter);
	if (position == 0)
		return;
	position--;
	
	button = tree_view_move_iter_to (data, &iter, position, FALSE);
	select_and_view_iter (data, &iter);
	quick_box_reorder_child (data->quick_box, button, position);

	quick_lounge_save_order (data->quick_lounge);
}


static void
move_down_cb (GtkWidget  *widget, 
	      DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               position;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	position = get_iter_position (data, &iter);
	if (position >= data->n - 1)
		return;
	position++;
	
	button = tree_view_move_iter_to (data, &iter, position, FALSE);
	select_and_view_iter (data, &iter);
	quick_box_reorder_child (data->quick_box, button, position);
	
	quick_lounge_save_order (data->quick_lounge);
}


static void
move_bottom_cb (GtkWidget  *widget, 
		DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               position;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	position = get_iter_position (data, &iter);
	if (position >= data->n - 1)
		return;
	
	button = tree_view_move_iter_to (data, &iter, data->n - 2, TRUE);
	select_and_view_iter (data, &iter);
	quick_box_reorder_child (data->quick_box, button, data->n - 1);

	quick_lounge_save_order (data->quick_lounge);
}


static void
move_top_cb (GtkWidget  *widget, 
	     DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               position;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	position = get_iter_position (data, &iter);
	if (position == 0)
		return;
		
	button = tree_view_move_iter_to (data, &iter, 0, FALSE);
	select_and_view_iter (data, &iter);
	quick_box_reorder_child (data->quick_box, button, 0);

	quick_lounge_save_order (data->quick_lounge);
}


static void
update_sensitivity (DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               position;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
                return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter)
	    || (data->n == 0)) 
	{
		gtk_widget_set_sensitive (data->btn_edit, FALSE);
		gtk_widget_set_sensitive (data->btn_delete, FALSE);
		return;
	}

	gtk_widget_set_sensitive (data->btn_delete, TRUE);

	position = get_iter_position (data, &iter);
	gtk_widget_set_sensitive (data->btn_top, position > 0);
	gtk_widget_set_sensitive (data->btn_up, position > 0);
	gtk_widget_set_sensitive (data->btn_down, position < data->n - 1);	
	gtk_widget_set_sensitive (data->btn_bottom, position < data->n - 1);
	
	gtk_tree_model_get (data->model,
			    &iter,
			    COLUMN_DATA, &button,
			    -1);
	gtk_widget_set_sensitive (data->btn_edit, ! QUICK_IS_SEPARATOR (button));
}


static void
selection_changed_cb (GtkTreeSelection *selection,
		      gpointer          user_data)
{
	DialogData *data = user_data;
	update_sensitivity (data);
}


static void
update_list (DialogData *data)
{
	GList       *children, *scan;
	GtkTreeIter  select_iter;

	gtk_list_store_clear (GTK_LIST_STORE (data->model));

	children = gtk_container_get_children (GTK_CONTAINER (data->quick_box));
	data->n = g_list_length (children);
	for (scan = g_list_last (children); scan; scan = scan->prev) {
		GtkWidget   *child_widget = scan->data;
		QuickButton *button;
		GdkPixbuf   *pixbuf;
		GtkTreeIter  iter;
		double       scale;
		int          w, h;

		if (! QUICK_IS_BUTTON (child_widget))
			continue;

		button = QUICK_BUTTON (child_widget);

		if (QUICK_IS_SEPARATOR (button)) { /* Space */
			gtk_list_store_prepend (GTK_LIST_STORE (data->model), 
						&iter);

			if (child_widget == data->select_button) 
				select_iter = iter;

			gtk_list_store_set (GTK_LIST_STORE (data->model), 
					    &iter,
					    COLUMN_DATA, button,
					    COLUMN_NAME, SEPARATOR_NAME,
					    -1);
			continue;
		}

		/* Application */

		/* icon */
		
		w = gdk_pixbuf_get_width (button->pixbuf);
		h = gdk_pixbuf_get_height (button->pixbuf);
		scale = MIN ((double) ICON_SIZE / w, (double) ICON_SIZE / h);
		w = MAX ((double) w * scale, 1);
		h = MAX ((double) h * scale, 1);

		pixbuf = gdk_pixbuf_scale_simple (button->pixbuf, w, h,
						  GDK_INTERP_BILINEAR);

		/**/

		gtk_list_store_prepend (GTK_LIST_STORE (data->model), &iter);

		if (child_widget == data->select_button) 
			select_iter = iter;

		gtk_list_store_set (GTK_LIST_STORE (data->model), &iter,
				    COLUMN_DATA, button,
				    COLUMN_ICON, pixbuf,
				    COLUMN_NAME, button->name,
				    -1);
		g_object_unref (pixbuf);
	}
	g_list_free (children);

	if (data->select_button != NULL) {
		do_select_iter (data, &select_iter);
		data->select_button = NULL;
	}
}


static void
add_columns (GtkTreeView *treeview)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	/* The Name column. */

	column = gtk_tree_view_column_new ();

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
                                             "pixbuf", COLUMN_ICON,
                                             NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer,
                                             "text", COLUMN_NAME,
                                             NULL);

	/* gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE); FIXME */
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
}


static void edit_cb (GtkWidget *widget, DialogData *data);


static void
add_launcher_cb (GtkWidget  *widget, 
		 DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	int               pos = -1;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
		return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		pos = -2;
	else
		pos = get_iter_position (data, &iter);

	quick_lounge_new_launcher (data->quick_lounge, pos);
}


static void
add_from_menu_cb (GtkWidget  *widget, 
		  DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	int               pos = -1;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
		return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		pos = data->n - 1;
	else
		pos = get_iter_position (data, &iter);

	dlg_add_from_menu (data->quick_lounge, pos);
}


static void
add_space_cb (GtkWidget  *widget, 
	      DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	int               pos = -1;
	gboolean          empty = FALSE;
	GtkWidget        *button;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
		return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		empty = TRUE;
	else
		pos = get_iter_position (data, &iter);

	/* add the separator */
	
	if (empty)
		button = quick_box_add_separator (data->quick_box, -1);
	else
		button = quick_box_add_separator (data->quick_box, pos + 1);
	
	/* update the tree view */
	
	if (empty)
		gtk_list_store_append (GTK_LIST_STORE (data->model), 
				       &iter);
	else
		gtk_list_store_insert_after (GTK_LIST_STORE (data->model), &iter, &iter);
	
	gtk_list_store_set (GTK_LIST_STORE (data->model), 
			    &iter,
			    COLUMN_DATA, button,
			    COLUMN_NAME, SEPARATOR_NAME,
			    -1);
	data->n++;
	
	/* Save changes */
	
	quick_lounge_save_order (data->quick_lounge);
	
	/* select and view */
	
	select_and_view_iter (data, &iter);
}


static void
delete_cb (GtkWidget  *widget, 
	   DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;
	int               pos;
	char             *uri;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
		return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;
	else
		pos = get_iter_position (data, &iter);

	gtk_tree_model_get (data->model,
			    &iter,
			    COLUMN_DATA, &button,
			    -1);

	uri = g_object_get_data (G_OBJECT (button), "uri");
	if (uri != NULL) {
		GFile *file;
		
		file = g_file_new_for_uri (uri);
		g_file_delete (file, NULL, NULL);
		g_object_unref (file);
	}

	gtk_container_remove (GTK_CONTAINER (data->quick_box), button);
	gtk_list_store_remove (GTK_LIST_STORE (data->model), &iter);
	quick_lounge_save_order (data->quick_lounge);

	data->n--;
}


static void
edit_cb (GtkWidget  *widget, 
	 DialogData *data)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkWidget        *button;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	if (selection == NULL)
		return;
	
	if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	gtk_tree_model_get (data->model,
			    &iter,
			    COLUMN_DATA, &button,
			    -1);

	if (g_object_get_data (G_OBJECT (button), "desktop_entry") != NULL)
		quick_lounge_util__item_properties (data->quick_lounge, button);
}


static void
min_size_value_changed_cb (GtkWidget  *widget,
			   DialogData *data)
{
	gboolean needs_update;
	
	needs_update = quick_lounge_set_min_visible_cols (data->quick_lounge, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (data->p_minsize_spinbutton)));	 
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_minsize_spinbutton), quick_box_get_min_visible_cols (data->quick_box));
	if (needs_update)
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_maxsize_spinbutton), quick_box_get_max_visible_cols(data->quick_box));
}


static void
max_size_value_changed_cb (GtkWidget  *widget,
			   DialogData *data)
{
	if (data->timeout != 0)
		g_source_remove (data->timeout);
	data->timeout = g_timeout_add (UPDATE_MAX_SIZE_DELAY, change_max_size, data);
}


static void
rows_value_changed_cb (GtkWidget  *widget,
		       DialogData *data)
{
	quick_lounge_set_rows (data->quick_lounge, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (data->p_rows_spinbutton)));
}


static void  
drag_data_received  (GtkWidget          *widget,
		     GdkDragContext     *context,
		     int                 x,
		     int                 y,
		     GtkSelectionData   *data,
		     guint               info,
		     guint               time,
		     gpointer            extra_data)
{
	DialogData   *ddata = extra_data;
	QuickLounge  *quick_lounge = ddata->quick_lounge;
	QuickBox     *quick_box = ddata->quick_box;
        GList        *list, *scan;
	int           pos;
	GtkTreePath  *pos_path;
	GtkTreeViewDropPosition drop_pos;
	char         *selection_data = (char*)data->data;
	GtkTreeIter   iter;


	if (!((data->length >= 0) && (data->format == 8))) {
		gtk_drag_finish (context, FALSE, FALSE, time);
		return;
	}

	gtk_drag_finish (context, TRUE, FALSE, time);

	if (gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (ddata->tree_view),
					       x, y,
					       &pos_path,
					       &drop_pos)) {
		pos = gtk_tree_path_get_indices (pos_path)[0];
		gtk_tree_path_free (pos_path);
	} else
		pos = 0;

        /* separator 'uri' starts with a : as coded in drag_data_get() */
	if (selection_data[0] == ':') { 
		int          from_pos = atoi (selection_data + 1);
		GtkWidget   *button;

		gtk_tree_model_iter_nth_child (ddata->model, &iter, NULL, from_pos);

		if ((drop_pos == GTK_TREE_VIEW_DROP_BEFORE) ||
		    (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_BEFORE)) {
			if (from_pos < pos)
				pos--;
		}
		
		if ((drop_pos == GTK_TREE_VIEW_DROP_AFTER) ||
		    (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_AFTER)) {
			if (from_pos > pos)
				pos++;
		}
		
		button = tree_view_move_iter_to (ddata, &iter, pos, FALSE);
		quick_box_reorder_child (ddata->quick_box, button, pos);
	} 
	else {
		list = get_file_list_from_url_list (selection_data);
		for (scan = list; scan; scan = scan->next) {
			char      *src_uri = scan->data;
			char      *uri = NULL;
			GtkWidget *button;
			int        button_pos;

			if (! is_desktop_file (src_uri)) {
				GKeyFile *desktop_entry;
				
				desktop_entry = _g_desktop_entry_new_for_uri (src_uri);
				quick_lounge_add_launcher (quick_lounge, desktop_entry, pos);
				
				g_key_file_free (desktop_entry);
				
				continue;
			}

			uri = g_build_filename (quick_lounge->location,
						file_name_from_path (src_uri),
						NULL);

			/* New laucher: add it to the list. */

			if (strcmp (src_uri, uri) != 0) {
				g_free (uri);
				uri = quick_lounge_util__get_unique_uri (quick_lounge);
				if (file_copy (src_uri, uri)) 
					quick_box_add_button (quick_box, 
							      uri, 
							      pos);
				g_free (uri);
				continue;
			}
			
			/* The launcher is already present, this means that
			 * the laucher has been dragged from the properties 
			 * dialog: change launcher position. */

			button = quick_box_get_child_from_uri (quick_box, uri);
			g_free (uri);
				
			if (button == NULL) 
				continue;
				
			button_pos = quick_box_get_child_position (quick_box, button);
			if (button_pos == -1) 
				continue;
			
			if ((drop_pos == GTK_TREE_VIEW_DROP_BEFORE) ||
			    (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_BEFORE)) {
				if (button_pos < pos)
					pos--;
			}
			
			if ((drop_pos == GTK_TREE_VIEW_DROP_AFTER) ||
			    (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_AFTER)) {
				if (button_pos > pos)
					pos++;
			}
			
			quick_box_reorder_child (quick_box, button, pos);
		}
		
		path_list_free (list);
	}
	
	quick_lounge_save_order (quick_lounge);
	dlg_properties_update (quick_lounge->prop_dialog);

	gtk_tree_model_iter_nth_child (ddata->model, &iter, NULL, pos);
	do_select_iter (ddata, &iter);
}


static gboolean
drag_motion (GtkWidget          *widget,
	     GdkDragContext     *context,
	     gint                x,
	     gint                y,
	     guint               time,
	     gpointer            extra_data)
{
	DialogData              *ddata = extra_data;
	GtkTreePath             *pos_path;
	GtkTreeViewDropPosition  drop_pos;
	GtkAdjustment           *adj = ddata->p_apps_vadjustment;

#ifdef DEBUG
	g_print ("(%d, %d) [%d, %d]\n", 
		 x, y, 
		 widget->allocation.width, widget->allocation.height);

	g_print ("(%2.2f) [%2.2f, %2.2f]\n", 
		 ddata->p_apps_vadjustment->value, 
		 ddata->p_apps_vadjustment->lower, 
		 ddata->p_apps_vadjustment->upper);
#endif

	if ((widget->allocation.height - y < AUTO_SCROLL_THRESHOLD)
	    && (adj->value + widget->allocation.height < adj->upper)) {
		gtk_adjustment_set_value (adj, gtk_adjustment_get_value (adj) + AUTO_SCROLL_STEP);
	} 
	else 	if ((y < AUTO_SCROLL_THRESHOLD) && (adj->value > 0.0)) {
		gtk_adjustment_set_value (adj, gtk_adjustment_get_value (adj) - AUTO_SCROLL_STEP);
	}

	if (! gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (ddata->tree_view),
						 x, y,
						 &pos_path,
						 &drop_pos)) 
		pos_path = gtk_tree_path_new_first ();

	if (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_BEFORE)
		drop_pos = GTK_TREE_VIEW_DROP_BEFORE;
	
	else if (drop_pos == GTK_TREE_VIEW_DROP_INTO_OR_AFTER)
		drop_pos = GTK_TREE_VIEW_DROP_AFTER;
	
	gtk_tree_view_set_drag_dest_row  (GTK_TREE_VIEW (ddata->tree_view),
					  pos_path,
					  drop_pos);

	gtk_tree_path_free (pos_path);

	return TRUE;
}


static gboolean
tree_view_button_press_cb (GtkWidget      *widget,
			   GdkEventButton *event,
			   gpointer        data)
{
	DialogData *ddata = data;
	ddata->start_x = event->x;
	ddata->start_y = event->y;
	return FALSE;
}


static void
drag_data_begin (GtkWidget      *widget,
		 GdkDragContext *context,
		 gpointer        data)
{
	DialogData        *ddata = data;
	GtkTreePath       *path = NULL;
	GdkPixmap         *row_pix;
	int                cell_x, cell_y;

	gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (ddata->tree_view),
				       ddata->start_x,
				       ddata->start_y,
				       &path,
				       NULL,
				       &cell_x,
				       &cell_y);
	
	g_return_if_fail (path != NULL);

	row_pix = gtk_tree_view_create_row_drag_icon (GTK_TREE_VIEW (ddata->tree_view), path);
	gtk_drag_set_icon_pixmap (context,
				  gdk_drawable_get_colormap (row_pix),
				  row_pix,
				  NULL,
				  ddata->start_x + 1,
				  cell_y + 1);
	g_object_unref (row_pix);
	gtk_tree_path_free (path);
}


static void  
drag_data_get  (GtkWidget        *widget,
		GdkDragContext   *context,
		GtkSelectionData *selection_data,
		guint             info,
		guint             time,
		gpointer          data)
{
	DialogData       *ddata = data;
        /*char             *target;*/
	const char       *uri;
	GtkTreeIter       iter;
	GtkTreeSelection *selection;
	GtkWidget        *button;
	static char       pos_data[32];

	/*
        target = gdk_atom_name (selection_data->target);
        if (strcmp (target, "text/uri-list") != 0) {
		g_free (target);
		return;
	}
        g_free (target);
	*/

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ddata->tree_view));
	gtk_tree_selection_get_selected (selection, NULL, &iter);
	gtk_tree_model_get (ddata->model,
			    &iter,
			    COLUMN_DATA, &button,
			    -1);
	uri = g_object_get_data (G_OBJECT (button), "uri");
	
	if (uri == NULL) { /* separator */
		int pos = get_iter_position (ddata, &iter);

		/* separators 'uri' are coded as a : and the separator position. */
		sprintf (pos_data, ":%d", pos);
		gtk_selection_data_set (selection_data, 
					selection_data->target,
					8, 
					(guchar*) pos_data,
					strlen (pos_data));
	} 
	else 
		gtk_selection_data_set (selection_data, 
					selection_data->target,
					8, 
					(guchar*) uri,
					strlen (uri));
}


GtkWidget *
dlg_properties (QuickLounge  *quick_lounge,
		GtkWidget    *select_button)
{
	DialogData       *data;
	GtkWidget        *btn_close;
	GtkWidget        *btn_help;
	GtkTreeSelection *selection;
	char             *rows_label;
	char             *columns_label;
	
	data = g_new0 (DialogData, 1);

	data->quick_lounge = quick_lounge;
	data->quick_box = QUICK_BOX (quick_lounge->quick_box);
	data->select_button = select_button;
	data->timeout = 0;

	data->builder = _gtk_builder_new_from_file ("properties.ui");

	/* Get the widgets. */

	data->dialog = GET_WIDGET ("properties_dialog");
	data->tree_view = GET_WIDGET ("p_apps_treeview");
	data->btn_up = GET_WIDGET ("p_up_button");
	data->btn_down = GET_WIDGET ("p_down_button");
	data->btn_top = GET_WIDGET ("p_top_button");
	data->btn_bottom = GET_WIDGET ("p_bottom_button");

	data->btn_edit = GET_WIDGET ("p_edit_button");
	data->btn_add_launcher = GET_WIDGET ("p_add_launcher_button");
	data->btn_add_from_menu = GET_WIDGET ("p_add_from_menu_button");
	data->btn_add_space = GET_WIDGET ("p_add_space_button");
	data->btn_delete = GET_WIDGET ("p_delete_button");

	data->p_minsize_spinbutton = GET_WIDGET ("p_minsize_spinbutton");
	data->p_maxsize_spinbutton = GET_WIDGET ("p_maxsize_spinbutton");
	data->p_rows_spinbutton = GET_WIDGET ("p_rows_spinbutton");

	data->p_apps_scrolledwindow = GET_WIDGET ("p_apps_scrolledwindow");
	data->p_apps_vadjustment = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (data->p_apps_scrolledwindow));

	btn_close = GET_WIDGET ("p_close_button");
	btn_help = GET_WIDGET ("p_help_button");

	/* Set widgets data. */

	data->model = GTK_TREE_MODEL (gtk_list_store_new (NUM_COLUMNS, 
							  G_TYPE_POINTER,
							  GDK_TYPE_PIXBUF,
							  G_TYPE_STRING));
	gtk_tree_view_set_model (GTK_TREE_VIEW (data->tree_view), data->model);
	
	g_object_unref (data->model);
	add_columns (GTK_TREE_VIEW (data->tree_view));

	gtk_widget_set_sensitive (data->btn_up, TRUE);
	gtk_widget_set_sensitive (data->btn_down, TRUE);
	gtk_widget_set_sensitive (data->btn_top, TRUE);
	gtk_widget_set_sensitive (data->btn_bottom, TRUE);

	switch (quick_box_get_orient (data->quick_box)) {
	case PANEL_APPLET_ORIENT_LEFT:
	case PANEL_APPLET_ORIENT_RIGHT:
		rows_label = _("C_olumns:");
		columns_label = _("rows");
		break;
	case PANEL_APPLET_ORIENT_UP:
	case PANEL_APPLET_ORIENT_DOWN:
	default:
		rows_label = _("R_ows:");
		columns_label = _("columns");
		break;
	}
	gtk_label_set_text_with_mnemonic (GTK_LABEL (GET_WIDGET ("rows_label")), rows_label);
	gtk_label_set_text (GTK_LABEL (GET_WIDGET ("columns_label")), columns_label);

	/**/

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_minsize_spinbutton), quick_box_get_min_visible_cols (data->quick_box));
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_maxsize_spinbutton), quick_box_get_max_visible_cols (data->quick_box));
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (data->p_rows_spinbutton), quick_box_get_rows (data->quick_box));

	/* Set the signals handlers. */
	
	g_signal_connect (G_OBJECT (data->dialog), 
			  "destroy",
			  G_CALLBACK (destroy_cb),
			  data);
	g_signal_connect (G_OBJECT (btn_close), 
			  "clicked",
			  G_CALLBACK (close_cb),
			  data);
	g_signal_connect (G_OBJECT (btn_help), 
			  "clicked",
			  G_CALLBACK (help_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_up), 
			  "clicked",
			  G_CALLBACK (move_up_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_down), 
			  "clicked",
			  G_CALLBACK (move_down_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_top), 
			  "clicked",
			  G_CALLBACK (move_top_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_bottom), 
			  "clicked",
			  G_CALLBACK (move_bottom_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_add_launcher), 
			  "clicked",
			  G_CALLBACK (add_launcher_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_add_from_menu), 
			  "clicked",
			  G_CALLBACK (add_from_menu_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_add_space), 
			  "clicked",
			  G_CALLBACK (add_space_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_delete), 
			  "clicked",
			  G_CALLBACK (delete_cb),
			  data);
	g_signal_connect (G_OBJECT (data->btn_edit), 
			  "clicked",
			  G_CALLBACK (edit_cb),
			  data);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (data->tree_view));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (selection,
                          "changed",
                          G_CALLBACK (selection_changed_cb),
			  data);

	g_signal_connect (G_OBJECT (data->p_minsize_spinbutton),
                          "value_changed",
                          G_CALLBACK (min_size_value_changed_cb),
			  data);
	g_signal_connect (G_OBJECT (data->p_maxsize_spinbutton),
                          "value_changed",
                          G_CALLBACK (max_size_value_changed_cb),
			  data);
	g_signal_connect (G_OBJECT (data->p_rows_spinbutton),
			  "value_changed",
			  G_CALLBACK (rows_value_changed_cb),
			  data);
	
	/* Drag & Drop */

	gtk_drag_source_set (data->tree_view,
			     GDK_BUTTON1_MASK,
			     source_target_table,
			     source_target_table_n,
			     GDK_ACTION_MOVE);

	g_signal_connect (G_OBJECT (data->tree_view),
			  "drag_begin",
			  G_CALLBACK (drag_data_begin), 
			  data);
	g_signal_connect (G_OBJECT (data->tree_view),
			  "drag_data_get",
			  G_CALLBACK (drag_data_get), 
			  data);

	g_signal_connect (G_OBJECT (data->tree_view),
			  "button_press_event",
			  G_CALLBACK (tree_view_button_press_cb), 
			  data);

	gtk_drag_dest_set (GTK_WIDGET (data->tree_view),
                           GTK_DEST_DEFAULT_ALL,
                           dest_target_table,
			   dest_target_table_n,
			   GDK_ACTION_COPY | GDK_ACTION_MOVE);

	g_signal_connect (G_OBJECT (data->tree_view), 
			  "drag_data_received",
			  G_CALLBACK (drag_data_received), 
			  data);

	g_signal_connect (G_OBJECT (data->tree_view), 
			  "drag_motion",
			  G_CALLBACK (drag_motion), 
			  data);

	/* run dialog. */

	g_object_set_data (G_OBJECT (data->dialog), "dialog_data", data);

	gtk_widget_show (data->dialog);
	
	update_list (data);
	update_sensitivity (data);

	return data->dialog;
}


void
dlg_properties_update (GtkWidget *dialog)
{
	DialogData *data;

	if (dialog == NULL)
		return;

	data = g_object_get_data (G_OBJECT (dialog), "dialog_data");
	if (data == NULL)
		return;

	update_list (data);
}


void
dlg_properties_select_button (GtkWidget *dialog, 
			      GtkWidget *button)
{
	DialogData *data;

	if (dialog == NULL)
		return;

	data = g_object_get_data (G_OBJECT (dialog), "dialog_data");
	if (data == NULL)
		return;

	data->select_button = button;
	update_list (data);
}
