/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2009 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef QUICK_DESKTOP_ENTRY_DIALOG_H
#define QUICK_DESKTOP_ENTRY_DIALOG_H

#include <gtk/gtk.h>

#define QUICK_TYPE_DESKTOP_ENTRY_DIALOG            (quick_desktop_entry_dialog_get_type ())
#define QUICK_DESKTOP_ENTRY_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), QUICK_TYPE_DESKTOP_ENTRY_DIALOG, QuickDesktopEntryDialog))
#define QUICK_DESKTOP_ENTRY_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), QUICK_TYPE_DESKTOP_ENTRY_DIALOG, QuickDesktopEntryDialogClass))
#define QUICK_IS_DESKTOP_ENTRY_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), QUICK_TYPE_DESKTOP_ENTRY_DIALOG))
#define QUICK_IS_DESKTOP_ENTRY_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), QUICK_TYPE_DESKTOP_ENTRY_DIALOG))
#define QUICK_DESKTOP_ENTRY_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), QUICK_TYPE_DESKTOP_ENTRY_DIALOG, QuickDesktopEntryDialogClass))

typedef struct _QuickDesktopEntryDialog        QuickDesktopEntryDialog;
typedef struct _QuickDesktopEntryDialogClass   QuickDesktopEntryDialogClass;
typedef struct _QuickDesktopEntryDialogPrivate QuickDesktopEntryDialogPrivate;

struct _QuickDesktopEntryDialog {
	GtkDialog parent_instance;
	QuickDesktopEntryDialogPrivate *priv;
};

struct _QuickDesktopEntryDialogClass {
	GtkDialogClass parent_class;
	
	void  (*changed) (QuickDesktopEntryDialog *self,
			  gboolean                 reverted);
};

GType       quick_desktop_entry_dialog_get_type   (void);
GtkWidget * quick_desktop_entry_dialog_new        (const char               *title,
					 	   GtkWindow                *parent,
						   GKeyFile                 *entry);
void        quick_desktop_entry_dialog_set_entry  (QuickDesktopEntryDialog  *self, 
						   GKeyFile                 *entry);
GKeyFile *  quick_desktop_entry_dialog_get_entry  (QuickDesktopEntryDialog  *self,
						   GError                  **error);
void        quick_desktop_entry_dialog_revert     (QuickDesktopEntryDialog  *self);

#endif /* QUICK_DESKTOP_ENTRY_DIALOG_H */
