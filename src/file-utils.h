/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001-2009 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <sys/stat.h>
#include <sys/types.h>
#include <glib.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include "typedefs.h"


typedef enum {
	DESKTOP_ENTRY_TYPE_UNKNOWN,
	DESKTOP_ENTRY_TYPE_APPLICATION,
	DESKTOP_ENTRY_TYPE_LINK
} DesktopEntryType;


typedef void (*PathListDoneFunc) (GList *files, GList *dirs, GError *error, gpointer data);


gboolean            uri_is_file                   (const char        *uri);
gboolean            uri_is_dir                    (const char        *uri);
gboolean            path_is_dir                   (const char        *path);
gboolean            file_copy                     (const char        *source_uri, 
						   const char        *destination_uri);
gboolean            make_directory_tree           (GFile             *dir,
		     				   mode_t             mode,
		     				   GError           **error);
gboolean            make_directory_tree_from_uri  (const char        *uri,
						   mode_t             mode,
						   GError           **error);
gboolean            make_directory_tree_from_path (const char        *path,
		   	                           mode_t             mode,
		   	                           GError           **error);
const char *        file_name_from_path           (const char        *path);
gboolean            path_list_new                 (const char        *path, 
						   GList            **files, 
						   GList            **dirs);
GList *             path_list_dup                 (GList             *list);						   
void                path_list_free                (GList             *list);
void                path_list_async_new           (const gchar       *uri, 
						   GCancellable      *cancellable,
						   PathListDoneFunc   f,
						   gpointer           data);
char *              get_unique_desktop_file       (const char        *base_dir);
gboolean            is_desktop_file               (const char        *uri);
GList *             get_file_list_from_url_list   (char              *url_list);
gboolean            g_write_file                  (GFile             *file,
						   gboolean           make_backup,
					           GFileCreateFlags   flags,		
						   void              *buffer,
						   gsize              count, 
						   GCancellable      *cancellable,
						   GError           **error);
void                _g_key_file_cleanup           (GKeyFile          *file);
GKeyFile *          _g_key_file_dup               (GKeyFile          *file);
GKeyFile *          _g_desktop_entry_new          (DesktopEntryType   type);
GKeyFile *          _g_desktop_entry_new_for_uri  (const char        *uri);
gboolean            _g_desktop_entry_equal        (GKeyFile          *file1,
						   GKeyFile          *file2);
void                _g_desktop_entry_launch       (GKeyFile          *desktop_entry,
			 			   GtkWidget         *source,
			 			   GList             *files);

#endif /* FILE_UTILS_H */
