/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

/* Based upon button-widget.c of the gnome-panel package :
 *
 * (C) 1997,1998,1999,2000 the Free Software Foundation
 * (C) 2000 Eazel, Inc.
 *
 * Authors: Federico Mena
 *          Miguel de Icaza
 *          George Lebl
 */

#include <config.h>
#include <math.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "gtk-utils.h"
#include "quick-button.h"

#define DEFAULT_OFFSET      2
#define PRESSED_TIMEOUT     400
#define DEFAULT_SIZE        48

enum {
	DRAG_STARTED,
	LAST_SIGNAL
};

enum {
        TARGET_URILIST,
};
static GtkTargetEntry target_table[] = {
        { "text/uri-list", 0, TARGET_URILIST },
};
static guint n_targets = sizeof (target_table) / sizeof (target_table[0]);

static void     quick_button_class_init      (QuickButtonClass *klass);
static void     quick_button_instance_init   (QuickButton      *button);
static void     quick_button_size_request    (GtkWidget         *widget,
					      GtkRequisition    *requisition);
static void     quick_button_size_allocate   (GtkWidget         *widget,
					      GtkAllocation     *allocation);
static void     quick_button_realize         (GtkWidget         *widget);
static gboolean quick_button_expose          (GtkWidget         *widget,
					      GdkEventExpose    *event);
static void     quick_button_destroy         (GtkObject         *obj);
static gboolean quick_button_button_press    (GtkWidget         *widget,
					      GdkEventButton	*event);
static gboolean quick_button_enter_notify    (GtkWidget         *widget,
					      GdkEventCrossing  *event);
static gboolean quick_button_leave_notify    (GtkWidget         *widget,
					      GdkEventCrossing  *event);
static void     quick_button_button_pressed  (GtkButton         *button);
static void     quick_button_button_released (GtkButton         *button);
static void     set_pixmap                   (QuickButton       *button,  
					      const char        *icon_path, 
					      int                size);

static GtkButtonClass *parent_class;
static guint quick_button_signals[LAST_SIGNAL] = { 0 };

GType
quick_button_get_type (void)
{
	static GType object_type = 0;

	if (object_type == 0) {
		static const GTypeInfo object_info = {
			sizeof (QuickButtonClass),
                    	(GBaseInitFunc)         NULL,
                    	(GBaseFinalizeFunc)     NULL,
                    	(GClassInitFunc)        quick_button_class_init,
                    	NULL,                   /* class_finalize */
                    	NULL,                   /* class_data */
                   	sizeof (QuickButton),
                    	0,                      /* n_preallocs */
                    	(GInstanceInitFunc)     quick_button_instance_init

		};

		object_type = g_type_register_static (GTK_TYPE_BUTTON, 
						      "QuickButton", 
						      &object_info, 
						      0);
	}

	return object_type;
}


static void
quick_button_class_init (QuickButtonClass *class)
{
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (class);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
	GtkButtonClass *button_class = GTK_BUTTON_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	quick_button_signals[DRAG_STARTED] =
		g_signal_new ("drag_started",
                              G_TYPE_FROM_CLASS (class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (QuickButtonClass, drag_started),
                              NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE, 
                              0);

	gtk_object_class->destroy = quick_button_destroy;

	widget_class->realize            = quick_button_realize;
	widget_class->size_allocate      = quick_button_size_allocate;
	widget_class->size_request       = quick_button_size_request;
	widget_class->button_press_event = quick_button_button_press;
	widget_class->enter_notify_event = quick_button_enter_notify;
	widget_class->leave_notify_event = quick_button_leave_notify;
	widget_class->expose_event       = quick_button_expose;
	button_class->pressed  = quick_button_button_pressed;
	button_class->released = quick_button_button_released;

	class->set_pixmap = set_pixmap;
}


static void
quick_button_realize (GtkWidget *widget)
{
	GdkWindowAttr  attributes;
	int            attributes_mask;
	GtkButton     *button;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (QUICK_IS_BUTTON (widget));

	button = GTK_BUTTON (widget);
	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x           = widget->allocation.x;
	attributes.y           = widget->allocation.y;
	attributes.width       = widget->allocation.width;
	attributes.height      = widget->allocation.height;
	attributes.wclass      = GDK_INPUT_ONLY;
	attributes.event_mask  = (GDK_BUTTON_PRESS_MASK |
				  GDK_BUTTON_RELEASE_MASK |
				  GDK_POINTER_MOTION_MASK |
				  GDK_POINTER_MOTION_HINT_MASK |
				  GDK_KEY_PRESS_MASK |
				  GDK_ENTER_NOTIFY_MASK |
				  GDK_LEAVE_NOTIFY_MASK);
	attributes_mask = GDK_WA_X | GDK_WA_Y;

	widget->window = gtk_widget_get_parent_window (widget);
	g_object_ref (widget->window);
      
	button->event_window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
	gdk_window_set_user_data (button->event_window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
}


static void
quick_button_destroy (GtkObject *obj)
{
	QuickButton *button = QUICK_BUTTON (obj);

	if (button->pressed_timeout != 0) {
		g_source_remove (button->pressed_timeout);
		button->pressed_timeout = 0;
	}

	if (button->pixbuf != NULL) {
		g_object_unref (button->pixbuf);
		button->pixbuf = NULL;
	}
	
	if (button->scaled != NULL) {
		g_object_unref (button->scaled);
		button->scaled = NULL;
	}
	
	if (button->scaled_bright != NULL) {
		g_object_unref (button->scaled_bright);
		button->scaled_bright = NULL;
	}
	
	if (button->icon != NULL) {
		g_free (button->icon);
		button->icon = NULL;
	}
	
	if (button->name != NULL) {
		g_free (button->name);
		button->name = NULL;
	}

	if (button->icon_theme != NULL) {
		g_object_unref (button->icon_theme);
		button->icon_theme = NULL;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}


static void
do_colorshift (GdkPixbuf *dest, 
	       GdkPixbuf *src, 
	       int        shift)
{
	int     i, j;
	int     width, height, has_alpha, srcrowstride, destrowstride;
	guchar *target_pixels;
	guchar *original_pixels;
	guchar *pixsrc;
	guchar *pixdest;
	int     val;
	guchar  r,g,b;

	has_alpha       = gdk_pixbuf_get_has_alpha (src);
	width           = gdk_pixbuf_get_width (src);
	height          = gdk_pixbuf_get_height (src);
	srcrowstride    = gdk_pixbuf_get_rowstride (src);
	destrowstride   = gdk_pixbuf_get_rowstride (dest);
	target_pixels   = gdk_pixbuf_get_pixels (dest);
	original_pixels = gdk_pixbuf_get_pixels (src);

	for (i = 0; i < height; i++) {
		pixdest = target_pixels + i*destrowstride;
		pixsrc  = original_pixels + i*srcrowstride;
		for (j = 0; j < width; j++) {
			r            = *(pixsrc++);
			g            = *(pixsrc++);
			b            = *(pixsrc++);
			val          = r + shift;
			*(pixdest++) = CLAMP (val, 0, 255);
			val          = g + shift;
			*(pixdest++) = CLAMP (val, 0, 255);
			val          = b + shift;
			*(pixdest++) = CLAMP (val, 0, 255);
			
			if (has_alpha)
				*(pixdest++) = *(pixsrc++);
		}
	}
}


static GdkPixbuf *
make_bright_pixbuf (GdkPixbuf *pb)
{
	GdkPixbuf *bright_pixbuf;
	
	if (pb == NULL)
		return NULL;
	
	bright_pixbuf = gdk_pixbuf_new (gdk_pixbuf_get_colorspace (pb),
					gdk_pixbuf_get_has_alpha (pb),
					gdk_pixbuf_get_bits_per_sample (pb),
					gdk_pixbuf_get_width (pb),
					gdk_pixbuf_get_height (pb));
	do_colorshift (bright_pixbuf, pb, 30);

	return bright_pixbuf;
}


static void
button_load_pixbuf (QuickButton *button,
		    const char  *icon_name,
		    int          preffered_size)

{
	GdkPixbuf *new_pixbuf = NULL;
	int        w, h;
	int        pixbuf_w, pixbuf_h;
	double     scale;

	if (preffered_size <= 0)
		preffered_size = DEFAULT_SIZE;

	new_pixbuf = create_pixbuf_or_missing (button->icon_theme, icon_name, preffered_size);	

	/**/

	if (button->pixbuf != NULL)
		g_object_unref (button->pixbuf);
	button->pixbuf = new_pixbuf;

	pixbuf_w = w = gdk_pixbuf_get_width (button->pixbuf);
	pixbuf_h = h = gdk_pixbuf_get_height (button->pixbuf);

	scale = MIN ((double) button->size / w, (double) button->size / h);

	w = MAX ((double) w * scale, 1);
	h = MAX ((double) h * scale, 1);

	if (button->scaled != NULL)
		g_object_unref (button->scaled);
	button->scaled = gdk_pixbuf_scale_simple (button->pixbuf, 
						  w, h,
						  GDK_INTERP_BILINEAR);
						  
	if (button->scaled_bright != NULL)
		g_object_unref (button->scaled_bright);
	button->scaled_bright = make_bright_pixbuf (button->scaled);
}


static int
get_displacement (int size)
{
	return (int) (ceil ((double) size / DEFAULT_SIZE * DEFAULT_OFFSET));
}


static gboolean
quick_button_expose (GtkWidget         *widget,
		     GdkEventExpose    *event)
{
	QuickButton  *quick_button;
	GtkButton    *button;
	GdkRectangle  area, image_bound;
	int           offset, size;
	int           x, y, w, h;
	GdkPixbuf    *pixbuf = NULL;
  
	g_return_val_if_fail (QUICK_IS_BUTTON (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	quick_button = QUICK_BUTTON (widget);
	button = GTK_BUTTON (widget);
	
	if (! GTK_WIDGET_VISIBLE (widget) || ! GTK_WIDGET_MAPPED (widget)) 
		return TRUE;

	size = widget->allocation.height;
	offset = (button->in_button && button->button_down) ? get_displacement (size) : 0;
	
	if (button->in_button || quick_button->dnd_highlight || GTK_WIDGET_HAS_FOCUS (widget))
		pixbuf = quick_button->scaled_bright;
	else
		pixbuf = quick_button->scaled;
	
	w = gdk_pixbuf_get_width (pixbuf);
	h = gdk_pixbuf_get_height (pixbuf);
	x = widget->allocation.x + offset + (widget->allocation.width - w) / 2;
	y = widget->allocation.y + offset + (widget->allocation.height - h) / 2;
	
	image_bound.x = x;
	image_bound.y = y;      
	image_bound.width = w;
	image_bound.height = h;
	
	area = event->area;
	
	if (gdk_rectangle_intersect (&area, &widget->allocation, &area) &&
	    gdk_rectangle_intersect (&image_bound, &area, &image_bound))
		gdk_draw_pixbuf (widget->window, NULL, pixbuf, 
				 image_bound.x - x, image_bound.y - y,
				 image_bound.x, image_bound.y,
				 image_bound.width, image_bound.height,
				 GDK_RGB_DITHER_NORMAL,
				 0, 0);
	
	/*if (quick_button->dnd_highlight) 
		gdk_draw_rectangle (widget->window, 
				    widget->style->black_gc, FALSE,
				    widget->allocation.x, 
				    widget->allocation.y,
				    widget->allocation.width - 1,
				    widget->allocation.height - 1);*/

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		int focus_width, focus_pad;
		int x, y, width, height;

		gtk_widget_style_get (widget,
				      "focus-line-width", &focus_width,
				      "focus-padding", &focus_pad,
				      NULL);

		x = widget->allocation.x + focus_pad;
		y = widget->allocation.y + focus_pad;
		width = widget->allocation.width -  2 * focus_pad;
		height = widget->allocation.height - 2 * focus_pad;

		gtk_paint_focus (widget->style, 
				 widget->window,
				 GTK_WIDGET_STATE (widget),
				 &event->area, 
				 widget, "button",
				 x, y, width, height);
	}
	
	return FALSE;
}


static void
quick_button_size_request (GtkWidget      *widget, 
			   GtkRequisition *requisition)
{
	QuickButton *quick_button = QUICK_BUTTON (widget);
	requisition->width = requisition->height = quick_button->size;
}


static void
quick_button_size_allocate (GtkWidget     *widget, 
			    GtkAllocation *allocation)
{
	g_return_if_fail (QUICK_IS_BUTTON (widget));

	widget->allocation = *allocation;
	if (GTK_WIDGET_REALIZED (widget)) 
		gdk_window_move_resize (GTK_BUTTON (widget)->event_window,
					widget->allocation.x,
					widget->allocation.y,
					widget->allocation.width,
					widget->allocation.height);
}


static void
quick_button_instance_init (QuickButton *button)
{
	button->pixbuf = NULL;
	button->dnd_highlight = FALSE;
	button->pressed_timeout = 0;
}


static gboolean
quick_button_button_press (GtkWidget      *widget, 
			   GdkEventButton *event)
{
 	QuickButton *button;

	g_return_val_if_fail (QUICK_IS_BUTTON (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	button = QUICK_BUTTON (widget);
	if (button->pressed_timeout != 0)
		return FALSE;

	return GTK_WIDGET_CLASS (parent_class)->button_press_event (widget, event);
}


static gboolean
quick_button_enter_notify (GtkWidget        *widget, 
			   GdkEventCrossing *event)
{
	g_return_val_if_fail (QUICK_IS_BUTTON (widget), FALSE);

	GTK_WIDGET_CLASS (parent_class)->enter_notify_event (widget, event);
	if (GTK_BUTTON (widget)->in_button)
		gtk_widget_queue_draw (widget);

	return FALSE;
}


static gboolean
quick_button_leave_notify (GtkWidget        *widget, 
			   GdkEventCrossing *event)
{
	GtkWidget   *event_widget;
	QuickButton *quick_button;
	GtkButton   *button;

	g_return_val_if_fail (QUICK_IS_BUTTON (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	event_widget = gtk_get_event_widget ((GdkEvent*) event);

	quick_button = QUICK_BUTTON (widget);
	button = GTK_BUTTON (widget);

	if ((event_widget == widget) 
	    && (event->detail != GDK_NOTIFY_INFERIOR)) {
		button->in_button = FALSE;
		gtk_widget_queue_draw (widget);
	}

	return FALSE;
}


static gboolean
pressed_timeout_func (gpointer data)
{
	QuickButton *button;

	g_return_val_if_fail (QUICK_IS_BUTTON (data), FALSE);

	button = QUICK_BUTTON (data);
	button->pressed_timeout = 0;

	return FALSE;
}


static void
quick_button_button_pressed (GtkButton *button)
{
	QuickButton *quick_button;

	g_return_if_fail (QUICK_IS_BUTTON (button));

	parent_class->pressed (button);

	quick_button = QUICK_BUTTON (button);
	quick_button->pressed_timeout = g_timeout_add (PRESSED_TIMEOUT, pressed_timeout_func, quick_button);
        gtk_widget_queue_draw (GTK_WIDGET (button));
}


static void
quick_button_button_released (GtkButton *button)
{
	g_return_if_fail (QUICK_IS_BUTTON (button));

	parent_class->released (button);
        gtk_widget_queue_draw (GTK_WIDGET (button));
}


static void  
drag_begin  (GtkWidget        *widget,
	     GdkDragContext   *context)
{
	g_signal_emit (G_OBJECT (widget), 
		       quick_button_signals[DRAG_STARTED],
		       0);
}


static void  
drag_data_get  (GtkWidget        *widget,
		GdkDragContext   *context,
		GtkSelectionData *selection_data,
		guint             info,
		guint             time,
		gpointer          data)
{
        char       *target;
	const char *uri;

        target = gdk_atom_name (selection_data->target);
        if (strcmp (target, "text/uri-list") != 0) {
		g_free (target);
		return;
	}
        g_free (target);

	uri = g_object_get_data (G_OBJECT (widget), "uri");
	if (uri == NULL) 
                return;

        gtk_selection_data_set (selection_data, 
                                selection_data->target,
                                8, 
				(guchar*)uri,
                                strlen (uri));
}


GtkWidget *
quick_button_new (const char     *icon,
		  int             size,
		  const char     *name,
		  GtkIconTheme   *icon_theme)
{
	QuickButton *button;

	g_return_val_if_fail (icon_theme != NULL, NULL);

	button = QUICK_BUTTON (g_object_new (quick_button_get_type (), NULL));

	button->icon = (icon != NULL) ? g_strdup (icon) : NULL;
	button->size = size;
	button->name = (name != NULL) ? g_strdup (name) : NULL;
	button->icon_theme = g_object_ref (icon_theme);

	button_load_pixbuf (button, icon, size);

	gtk_drag_source_set (GTK_WIDGET (button),
                             GDK_BUTTON1_MASK,
                             target_table, n_targets, 
                             GDK_ACTION_COPY | GDK_ACTION_MOVE);

	g_signal_connect (G_OBJECT (button),
                          "drag_begin",
                          G_CALLBACK (drag_begin), 
                          button);
	g_signal_connect (G_OBJECT (button),
                          "drag_data_get",
                          G_CALLBACK (drag_data_get), 
                          button);

	return GTK_WIDGET (button);
}


void
quick_button_set_pixmap (QuickButton *button,
			 const char  *icon,
			 int          size)
{
	QUICK_BUTTON_GET_CLASS (G_OBJECT (button))->set_pixmap (button, icon, size);
}


static void
set_pixmap (QuickButton *button,
	    const char  *icon,
	    int          size)
{
	g_return_if_fail (QUICK_IS_BUTTON (button));
	g_return_if_fail (size > 0);

	if (icon != button->icon) {
		g_free (button->icon);
		button->icon = g_strdup (icon);
	}
	button->size = size;
	
	button_load_pixbuf (button, icon, size);
	gtk_widget_queue_resize (GTK_WIDGET (button));
}


void
quick_button_set_text (QuickButton *button, 
		       const char  *name)
{
	g_return_if_fail (QUICK_IS_BUTTON (button));

	g_free (button->name);
	button->name = name ? g_strdup (name) : NULL;

	gtk_widget_queue_draw (GTK_WIDGET (button));
}


void
quick_button_set_dnd_highlight (QuickButton *button, 
				gboolean     highlight)
{
	g_return_if_fail (button != NULL);
	g_return_if_fail (QUICK_IS_BUTTON (button));

	if(button->dnd_highlight != highlight) {
		button->dnd_highlight = highlight;
		gtk_widget_queue_draw (GTK_WIDGET (button));
	}
}
