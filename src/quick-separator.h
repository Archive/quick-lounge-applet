/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef QUICK_SEPARATOR_H
#define QUICK_SEPARATOR_H

#include <gtk/gtk.h>
#include "quick-button.h"

#define QUICK_TYPE_SEPARATOR            (quick_separator_get_type ())
#define QUICK_SEPARATOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), QUICK_TYPE_SEPARATOR, QuickSeparator))
#define QUICK_SEPARATOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), QUICK_TYPE_SEPARATOR, QuickSeparatorClass))
#define QUICK_IS_SEPARATOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), QUICK_TYPE_SEPARATOR))
#define QUICK_IS_SEPARATOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), QUICK_TYPE_SEPARATOR))
#define QUICK_SEPARATOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), QUICK_TYPE_SEPARATOR, QuickSeparatorClass))

typedef struct _QuickSeparator	     QuickSeparator;
typedef struct _QuickSeparatorClass  QuickSeparatorClass;

struct _QuickSeparator
{
	QuickButton __parent;

	GtkOrientation orient;
	int            panel_size;
};

struct _QuickSeparatorClass
{
	QuickButtonClass parent_class;
};

GType		quick_separator_get_type        (void) G_GNUC_CONST;

GtkWidget*	quick_separator_new	        (int             size,
						 int             panel_size,
						 GtkOrientation  orient);

void            quick_separator_set_size        (QuickSeparator *sep,
						 int             size,
						 int             panel_size);

void            quick_separator_set_orient      (QuickSeparator *sep,
						 GtkOrientation  orient);

#endif /* QUICK_SEPARATOR_H */
