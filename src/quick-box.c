/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001, 2004 Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gtk/gtk.h>
#include "dlg-properties.h"
#include "quick-box.h"
#include "quick-button.h"
#include "quick-separator.h"
#include "gtk-utils.h"

#define ICON_SPACING         1       /* spacing between icons */
#define ICON_SIZE_PANEL      22      /* icons size on panels */
#define ICON_SIZE_MENU       22      /* icons size in menus */
#define DEFAULT_MIN_VISIBLE_COLS 2   /* minimum number of columns to display */
#define DEFAULT_MAX_VISIBLE_COLS 20  /* maximum number of columns to display */
#define INITIAL_OFFSET       2       /* offset between handle and icons */

#define SIZE_HINT_MAX 0
#define SIZE_HINT_MIN 1

static GtkTargetEntry target_table[] = {
	{ "text/uri-list", 0, 0 }
};
static guint n_targets = sizeof (target_table) / sizeof (target_table[0]);

static GtkContainerClass *parent_class = NULL;

struct _QuickBoxPrivate {
	GtkWidget         *arrow_up;
	GtkWidget         *arrow_down;
	GtkWidget         *arrow_left;
	GtkWidget         *arrow_right;

	PanelAppletOrient  orient;
	int                size;

	int                rows;
	int                columns;
	int               *col_size;
	int               *buttons_on_col;
	int                min_visible_cols;
	int                max_visible_cols;
	int                size_hint[2];
	
	int                drag_pos_begin;
	int                drag_pos;
	QuickDropPos       drop_position;
	GtkWidget         *popup_menu;

	PopulateMenuFunc   populate_menu_func;
	gpointer           populate_menu_data;

	GtkIconTheme      *icon_theme;
};


static GtkOrientation
get_gtk_orientation (QuickBox *quick_box)
{
	GtkOrientation orientation;

	switch (quick_box->priv->orient) {
	case PANEL_APPLET_ORIENT_LEFT:
	case PANEL_APPLET_ORIENT_RIGHT:
		orientation = GTK_ORIENTATION_VERTICAL;
		break;
	case PANEL_APPLET_ORIENT_UP:
	case PANEL_APPLET_ORIENT_DOWN:
	default:
		orientation = GTK_ORIENTATION_HORIZONTAL;
		break;
	}

	return orientation;
}


static GtkWidget *
get_arrow_widget (QuickBox *quick_box)
{
	switch (quick_box->priv->orient) {
	case PANEL_APPLET_ORIENT_LEFT:
		return quick_box->priv->arrow_left;
	case PANEL_APPLET_ORIENT_RIGHT:
		return quick_box->priv->arrow_right;
	case PANEL_APPLET_ORIENT_UP:
		return quick_box->priv->arrow_up;
	case PANEL_APPLET_ORIENT_DOWN:
		return quick_box->priv->arrow_down;
	default:
		return NULL;
	}
}


static int
count_columns (QuickBox *quick_box)
{
	GList *children, *scan;
	int    col = 0;
	int    row = 0;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;

		if (QUICK_IS_SEPARATOR (child_widget)) {
			if (row > 0)
				col++;
			col++;
			row = 0;
		} 
		else {
			row++;
			if (row == quick_box->priv->rows) {
				col++;
				row = 0;
			}
		}			
	}
	g_list_free (children);

	if (row > 0)
		col++;

	return col;
}


static void
quick_box_size_request (GtkWidget        *widget,
			GtkRequisition   *requisition)
{
	QuickBox        *quick_box;
	QuickBoxPrivate *p;
	GtkBox          *box;
	GtkWidget       *arrow;
	int              arrow_width, arrow_height;
	GtkOrientation   orientation;
	GtkRequisition   child_requisition;
	GList           *children, *scan;
	int              spacing;
	int              min_columns, max_columns;
	int              row, col;
	int              min_size, max_size, cur_size;
	int              max_column_width, max_column_height;

	quick_box = QUICK_BOX (widget);
	p         = quick_box->priv;
	box       = GTK_BOX (widget);

	orientation = get_gtk_orientation (quick_box);
	spacing = box->spacing;

	p->columns = count_columns (quick_box);
	g_free (p->col_size);
	g_free (p->buttons_on_col);
	p->col_size = g_new0 (int, p->columns);
	p->buttons_on_col = g_new0 (int, p->columns);

	min_columns = MIN (p->columns, p->min_visible_cols);
	min_columns = MAX (min_columns, 0);

	max_columns = p->max_visible_cols;
	max_columns = MAX (max_columns, 0);
	max_columns = MAX (max_columns, min_columns);

	/**/

	min_size = 0;
	max_size = 0;
	cur_size = 0;
	max_column_width = 0;
	max_column_height = 0;
	col = 1;
	row = 1;

	children = gtk_container_get_children (GTK_CONTAINER (box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget      *child_widget = scan->data;
		GtkRequisition  child_requisition;

		gtk_widget_size_request (GTK_WIDGET (child_widget), 
					 &child_requisition);

		if ((orientation == GTK_ORIENTATION_HORIZONTAL)
		    && QUICK_IS_SEPARATOR (child_widget)) {

			if (max_column_width != 0) {
				p->col_size[col - 1] = max_column_width + spacing;
				p->buttons_on_col[col - 1] = row - 1;
				cur_size += p->col_size[col - 1];
				if (col == min_columns)
					min_size = cur_size;
				if (col == max_columns)
					max_size = cur_size;
				max_column_width = 0;
				col++;
				row = 1;
			}
			
			p->col_size[col - 1] = child_requisition.width + spacing;
			p->buttons_on_col[col - 1] = 1;
			cur_size += p->col_size[col - 1];
			if (col == min_columns)
				min_size = cur_size;
			if (col == max_columns)
				max_size = cur_size;
			max_column_width = 0;
			col++;
			row = 1;
		} 
		else if ((orientation == GTK_ORIENTATION_HORIZONTAL)
			   && QUICK_IS_BUTTON (child_widget)) {

			max_column_width = MAX (max_column_width, 
						child_requisition.width);

			if (row++ == p->rows) {
				p->col_size[col - 1] = max_column_width + spacing;
				p->buttons_on_col[col - 1] = row - 1;
				cur_size += p->col_size[col - 1];
				if (col == min_columns)
					min_size = cur_size;
				if (col == max_columns)
					max_size = cur_size;
				max_column_width = 0;
				col++;
				row = 1;
			} 
		} 
		else if ((orientation == GTK_ORIENTATION_VERTICAL)
			   && QUICK_IS_SEPARATOR (child_widget)) {
			
			if (max_column_height != 0) {
				p->col_size[row - 1] = max_column_height + spacing;
				p->buttons_on_col[row - 1] = col - 1;
				cur_size += p->col_size[row - 1];
				if (row == min_columns)
					min_size = cur_size;
				if (row == max_columns)
					max_size = cur_size;
				max_column_height = 0;
				row++;
				col = 1;
			}
			
			p->col_size[row - 1] = child_requisition.height + spacing;
			p->buttons_on_col[row - 1] = 1;
			cur_size += p->col_size[row - 1];
			if (row == min_columns)
				min_size = cur_size;
			if (row == max_columns)
				max_size = cur_size;
			max_column_height = 0;
			row++;
			col = 1;
		} 
		else if ((orientation == GTK_ORIENTATION_VERTICAL)
			   && QUICK_IS_BUTTON (child_widget)) {
			
			max_column_height = MAX (max_column_height, 
						 child_requisition.height);

			if (col++ == p->rows) {
				p->col_size[row - 1] = max_column_height + spacing;
				p->buttons_on_col[row - 1] = col - 1;
				cur_size += p->col_size[row - 1];
				if (row == min_columns)
					min_size = cur_size;
				if (row == max_columns)
					max_size = cur_size;
				max_column_height = 0;
				row++;
				col = 1;
			}
		}
	}

	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		if (max_column_width != 0) {
			p->col_size[col - 1] = max_column_width + spacing;
			p->buttons_on_col[col - 1] = row - 1;
			cur_size += p->col_size[col - 1];
		}
	} 
	else {
		if (max_column_height != 0) {
			p->col_size[row - 1] = max_column_height + spacing;
			p->buttons_on_col[row - 1] = col - 1;
			cur_size += p->col_size[row - 1];
		}
	}

	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		if (col <= min_columns)
			min_size = cur_size;
		if (col <= max_columns)
			max_size = cur_size;
	} 
	else {
		if (row <= min_columns)
			min_size = cur_size;
		if (row <= max_columns)
			max_size = cur_size;
	}

	min_size += spacing + INITIAL_OFFSET * 2;
	max_size += spacing + INITIAL_OFFSET * 2;

	/* add the arrow if visible */

	arrow = get_arrow_widget (quick_box);
	gtk_widget_size_request (arrow, &child_requisition);
	arrow_width = child_requisition.width;
	arrow_height = child_requisition.height;

	if (orientation == GTK_ORIENTATION_HORIZONTAL) { 
		int last_col = col;
		if (row == 1)
			last_col--;
		if (last_col != min_columns)
			min_size += spacing + arrow_width;

		max_size += spacing + arrow_width;
		if (children == NULL) /* when the box is empty */
			min_size = p->size / p->rows; 
		requisition->width = min_size;
		requisition->height = MAX (p->size, arrow_height);
		p->size_hint[SIZE_HINT_MAX] = max_size - requisition->width;
	} 
	else {
		int last_row = row;
		if (col == 1)
			last_row--;
		if (last_row != min_columns)
			min_size += spacing + arrow_height;

		max_size += spacing + arrow_height;
		if (children == NULL) /* when the box is empty */
			min_size = p->size / p->rows; 

		/* FIXME: 
		When the orientation is vertical, requisition->width expects the
		height, and requisition->height expects the width. I don't know 
		if this is by design or if it's a bug in gnome-panel, but 
		swapping the width and the height works for now. */
		requisition->height = MAX (p->size, arrow_width);
		requisition->width = min_size;
		p->size_hint[SIZE_HINT_MAX] = max_size - requisition->width;
	}

	p->size_hint[SIZE_HINT_MIN] = 0;

	/* FIXME */
	/*if (! GTK_WIDGET_VISIBLE (arrow)) {*/
		if (orientation == GTK_ORIENTATION_HORIZONTAL)
			p->size_hint[SIZE_HINT_MAX] -= spacing + arrow_width;
		else
			p->size_hint[SIZE_HINT_MAX] -= spacing + arrow_height;
	/*}*/
	
	/**/

	g_list_free (children);
	
	/* add border width */

	requisition->width += GTK_CONTAINER (box)->border_width * 2;
	requisition->height += GTK_CONTAINER (box)->border_width * 2;

#ifdef DEBUG
        g_print ("REQ [%d, %d]\n", requisition->width, requisition->height);
#endif
}


static void
quick_box_forall_arrows (QuickBox     *quick_box,
			 GtkCallback   callback,
			 gpointer      callback_data)
{
	(* callback) (quick_box->priv->arrow_up, callback_data);
	(* callback) (quick_box->priv->arrow_down, callback_data);
	(* callback) (quick_box->priv->arrow_left, callback_data);
	(* callback) (quick_box->priv->arrow_right, callback_data);
}


static void
quick_box_forall (GtkContainer *container,
		  gboolean      include_internals,
		  GtkCallback   callback,
		  gpointer      callback_data)
{
	g_return_if_fail (callback != NULL);
	
	GTK_CONTAINER_CLASS (parent_class)->forall (container,
						    include_internals,
						    callback,
						    callback_data);

	if (include_internals) {
		QuickBox *quick_box = QUICK_BOX (container);
		quick_box_forall_arrows (quick_box, callback, callback_data);
	}
}


#if 0
static void
quick_box_add (GtkContainer *container,
	       GtkWidget    *widget)
{
	g_print ("ADD\n");
	GTK_CONTAINER_CLASS (parent_class)->add (container, widget);
}


static void 
quick_box_remove (GtkContainer *container,
		  GtkWidget    *widget)
{
	g_print ("REMOVE\n");
	GTK_CONTAINER_CLASS (parent_class)->remove (container, widget);
}
#endif


static void
hide_non_current_arrow_cb (GtkWidget *arrow, 
			   QuickBox  *quick_box)
{
	if (arrow != get_arrow_widget (quick_box)) {
		gtk_widget_hide (arrow);
		gtk_widget_set_sensitive (arrow, FALSE);
	}
}


static void
hide_all_arrows_cb (GtkWidget *arrow, 
		    QuickBox  *quick_box)
{
	gtk_widget_hide (arrow);
	gtk_widget_set_sensitive (arrow, FALSE);
}


static int
children_under_the_arrow (QuickBox *quick_box,
			  int       arrow_size)
{
	int n_buttons = 0;
	int i, c;

	for (c = quick_box->priv->columns - 1; c >= 0; c--) {
		arrow_size -= quick_box->priv->col_size[c];
		if (arrow_size <= 0)
			break;
	}

	for (i = c; (i >= 0) && (i < quick_box->priv->columns); i++)
		n_buttons += quick_box->priv->buttons_on_col[i];

	return MAX (0, n_buttons);
}


static gboolean
last_n_child (QuickBox  *quick_box, 
	      GtkWidget *child_widget,
	      int        last_buttons)
{
	GList *children, *scan;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = g_list_last (children); (last_buttons > 0) && scan; scan = scan->prev) {
		if (scan->data == child_widget)
			return TRUE;
		last_buttons--;
	}
	g_list_free (children);

	return FALSE;
}


static void
show_first_n_children (QuickBox *quick_box,
		       int       n)
{
	GList *children, *scan;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; (n > 0) && scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		gtk_widget_show (child_widget);
		n--;
	}
	g_list_free (children);
}


static void
hide_last_n_children (QuickBox *quick_box,
		      int       n)
{
	GList *children, *scan;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = g_list_last (children); (n > 0) && scan; scan = scan->prev) {
		GtkWidget *child_widget = scan->data;
		gtk_widget_hide (child_widget);
		n--;
	}
	g_list_free (children);
}


static int
get_child_size (QuickBox *quick_box)
{
	int child_size = 0;

	g_return_val_if_fail (quick_box->priv->rows > 0, 0);

	if (quick_box->priv->rows == 1)
		return quick_box->priv->size;

	child_size = (quick_box->priv->size - (quick_box->priv->rows - 1) * ICON_SPACING) / quick_box->priv->rows;

	return MAX (0, child_size);
}


static void
quick_box_size_allocate (GtkWidget        *widget, 
			 GtkAllocation    *allocation)
{
	QuickBox       *quick_box;
	GtkBox         *box;
	GtkOrientation  orientation;
	GtkWidget      *arrow;
	int             border_width, spacing;
	int             x, y, width, height;
	int             arrow_width, arrow_height;
	int             max_width, max_height;
	int             current_width, current_height;
	int             max_line_width, max_line_height;
	int             initial_x, initial_y;
	GtkRequisition  child_requisition;
	gboolean        ignoring_arrow;
	gboolean        do_not_fit = FALSE;
	GList          *children;
	GList          *scan;
	GList          *hide_from_here = NULL;
	int             under_arrow_children;
	int             children_to_hide;
	int             n_children;
	gboolean        second_try = FALSE;
	
	quick_box = QUICK_BOX (widget);
	box       = GTK_BOX (widget);

	widget->allocation = *allocation;

	orientation  = get_gtk_orientation (quick_box);
	border_width = GTK_CONTAINER (box)->border_width;
	spacing      = box->spacing;

	arrow = get_arrow_widget (quick_box);
	gtk_widget_get_child_requisition (arrow, &child_requisition);
	arrow_width  = child_requisition.width;
	arrow_height = child_requisition.height;

	width  = MAX (1, allocation->width - border_width * 2);
	height = MAX (1, allocation->height - border_width * 2);

	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		initial_x = allocation->x + border_width + spacing + INITIAL_OFFSET;

		/* center vertically */
		initial_y = (allocation->height - (get_child_size (quick_box) + ICON_SPACING) * quick_box->priv->rows) / 2;
	} 
	else {
		/* center horizontally */
		initial_x = (allocation->width - (get_child_size (quick_box) + ICON_SPACING) * quick_box->priv->rows) / 2;

		initial_y = allocation->y + border_width + spacing + INITIAL_OFFSET;
	}

	max_width  = allocation->width;
	max_height = allocation->height;

	if (orientation == GTK_ORIENTATION_HORIZONTAL)
		max_width -= arrow_width + spacing;
	else 
		max_height -= arrow_height + spacing; 

	max_line_width = 0;
	max_line_height = 0;

	x = initial_x;
	y = initial_y;
	current_width  = initial_x;
	current_height = initial_y;

	if (orientation == GTK_ORIENTATION_HORIZONTAL)
		under_arrow_children = children_under_the_arrow (quick_box, arrow_width);
	else
		under_arrow_children = children_under_the_arrow (quick_box, arrow_height);
	ignoring_arrow = TRUE;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; ) {
		GtkWidget     *child_widget = scan->data;
		GtkAllocation  child_allocation;

		if (ignoring_arrow && last_n_child (quick_box, child_widget, under_arrow_children)) {
			ignoring_arrow = FALSE;
			if (orientation == GTK_ORIENTATION_HORIZONTAL) 
				max_width += arrow_width + spacing;
			else 
			        max_height += arrow_height + spacing;
		}

		child_allocation.x = x;
		child_allocation.y = y;
		gtk_widget_get_child_requisition (child_widget, &child_requisition);
		child_allocation.width = child_requisition.width;
		child_allocation.height = child_requisition.height;

		if (orientation == GTK_ORIENTATION_HORIZONTAL)
			current_height += child_allocation.height;
		else
			current_width += child_allocation.width;


		if (orientation == GTK_ORIENTATION_HORIZONTAL) {

#ifdef DEBUG
			g_print ("\n");
			g_print ("[X] (%d + %d) %d <--> %d\n", x, child_allocation.width, x + child_allocation.width, max_width);
			g_print ("[Y] %d <--> %d\n", current_height, max_height);
#endif
			
			if (x + child_allocation.width > max_width) {
				if (! do_not_fit) {
					do_not_fit = TRUE;
					hide_from_here = scan;
				}
			} 
			else if (current_height > max_height) { /* new row */
				if (second_try) {
					do_not_fit = TRUE;
					hide_from_here = scan;
				} 
				else { /* new column */
					x += max_line_width + spacing;
					y = initial_y;
					current_height = border_width;
					max_line_width = 0;
					second_try = TRUE;
					continue;
				}
			}
		} 
		else {
			
#ifdef DEBUG
			g_print ("\n");
			g_print ("[X] %d <--> %d\n", current_width, max_width);
			g_print ("[Y] %d <--> %d\n", y + child_allocation.height, max_height);
			
#endif
			
			if (y + child_allocation.height > max_height) {
				if (! do_not_fit) {
					do_not_fit = TRUE;
					hide_from_here = scan;
				}
			} 
			else if (current_width > max_width) { /* new row */
				if (second_try) {
					do_not_fit = TRUE;
					hide_from_here = scan;
				} 
				else {
					x = initial_x;
					y += max_line_height + spacing;
					current_width = border_width;
					max_line_height = 0;
					second_try = TRUE;
					continue;
				}
			} 
		}

		gtk_widget_size_allocate (child_widget, &child_allocation);

		if (orientation == GTK_ORIENTATION_HORIZONTAL) {
			max_line_width = MAX (max_line_width, child_allocation.width);
			current_height += spacing;
			y += child_allocation.width + spacing;
		} 
		else {
			max_line_height = MAX (max_line_height, child_allocation.height);
			current_width += spacing;
			x += child_allocation.height + spacing;
		}

		second_try = FALSE;
		scan = scan->next;
	}

	if (do_not_fit) {
		GtkAllocation  child_allocation;

		if (orientation == GTK_ORIENTATION_HORIZONTAL) {
			child_allocation.x = allocation->width - arrow_width;
			child_allocation.y = border_width + 1;
			child_allocation.width  = arrow_width;
			child_allocation.height = height - 1;
		} 
		else {
			child_allocation.x = border_width + 1;
			child_allocation.y = allocation->height - arrow_height;
			child_allocation.width  = width - 1;
			child_allocation.height = arrow_height;
		}

		gtk_widget_size_allocate (arrow, &child_allocation);

		gtk_widget_show (arrow);
		gtk_widget_set_sensitive (arrow, TRUE);

		quick_box_forall_arrows (quick_box,
					 (GtkCallback) hide_non_current_arrow_cb,
					 quick_box);
	} 
	else {
		quick_box_forall_arrows (quick_box,
					 (GtkCallback) hide_all_arrows_cb,
					 NULL);
	}
	
	if (do_not_fit) {
		children_to_hide = MAX (g_list_length (hide_from_here), under_arrow_children);
		hide_last_n_children (quick_box, children_to_hide);
	} 
	else 
		children_to_hide = 0;

	n_children = g_list_length (children);
	show_first_n_children (quick_box, n_children - children_to_hide);

	/**/

	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		if (do_not_fit)
			widget->allocation.width = allocation->width;
		else
			widget->allocation.width = x + max_line_width + spacing;
	} 
	else {
		if (do_not_fit)
			widget->allocation.height = allocation->height;
		else
			widget->allocation.height = y + max_line_height + spacing;
	}

	gtk_widget_get_child_requisition (widget, &child_requisition);

	if (orientation == GTK_ORIENTATION_HORIZONTAL)
		widget->allocation.width = MAX (widget->allocation.width, 
						child_requisition.width);
	else
		widget->allocation.height = MAX (widget->allocation.height, 
						 child_requisition.width);

	g_list_free (children);
}


static void
get_drag_destination_rect (QuickBox     *quick_box,
			   int           pos,
			   GdkRectangle *rect)
{
	GtkWidget *widget = GTK_WIDGET (quick_box);
	GList     *children, *scan;
	int        i, cx, cy, cwidth, cheight;

	cx = widget->allocation.x;
	cy = widget->allocation.y;
	cwidth = widget->allocation.width;
	cheight = widget->allocation.height;
	
	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	i = 0;
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;

		if (! QUICK_IS_BUTTON (child_widget))
			continue;

		if (i == pos) {
			cx = child_widget->allocation.x;
			cy = child_widget->allocation.y;
			cwidth = child_widget->allocation.width;
			cheight = child_widget->allocation.height;
			break;
		}
		
		i++;
	}
	g_list_free (children);

	rect->x = cx;
	rect->y = cy;
	rect->width = cwidth;
	rect->height = cheight;
}


static gboolean
quick_box_expose (GtkWidget        *widget,
		  GdkEventExpose   *event)
{
	QuickBox *quick_box = QUICK_BOX (widget);
	int       drag_pos;
	int       drag_pos_begin;
		
	GTK_WIDGET_CLASS (parent_class)->expose_event (widget, event);

	drag_pos = quick_box->priv->drag_pos;
	drag_pos_begin = quick_box->priv->drag_pos_begin;

	if ((drag_pos != -1) && (drag_pos != drag_pos_begin)) {
		GdkRectangle r;

#if 0		
		switch (quick_box->priv->drop_position) {
		case QUICK_DROP_POSITION_INTO_OR_BEFORE:
			g_print ("DropPosition: (%d) Into-Or-Before\n", drag_pos);
			break;
		case QUICK_DROP_POSITION_INTO_OR_AFTER:
			g_print ("DropPosition: (%d) Into-Or-After\n", drag_pos);
			break;
		case QUICK_DROP_POSITION_BEFORE:
			g_print ("DropPosition: (%d) Before\n", drag_pos);
			break;
		case QUICK_DROP_POSITION_AFTER:
			g_print ("DropPosition: (%d) After\n", drag_pos);
			break;
		}
#endif

		get_drag_destination_rect (quick_box, 
					   quick_box->priv->drag_pos, 
					   &r);

		if (quick_box->priv->drop_position == QUICK_DROP_POSITION_BEFORE) {
			switch (get_gtk_orientation (quick_box)) {
			case GTK_ORIENTATION_HORIZONTAL:
				gdk_draw_line (widget->window,
					       widget->style->black_gc,
					       r.x,
					       r.y,
					       r.x,
					       r.y + r.height - 1);
				break;
			case GTK_ORIENTATION_VERTICAL:
				gdk_draw_line (widget->window,
					       widget->style->black_gc,
					       r.x,
					       r.y,
					       r.x + r.width - 1,
					       r.y);
				break;
			}
		}
		else if (quick_box->priv->drop_position == QUICK_DROP_POSITION_AFTER) {
			switch (get_gtk_orientation (quick_box)) {
			case GTK_ORIENTATION_HORIZONTAL:
				gdk_draw_line (widget->window,
					       widget->style->black_gc,
					       r.x + r.width - 1,
					       r.y,
					       r.x + r.width - 1,
					       r.y + r.height - 1);
				break;
			case GTK_ORIENTATION_VERTICAL:
				gdk_draw_line (widget->window,
					       widget->style->black_gc,
					       r.x,
					       r.y + r.height - 1,
					       r.x + r.width - 1,
					       r.y + r.height - 1);
				break;
			}
		}	
	}

	if (GTK_WIDGET_HAS_FOCUS (widget) 
	    && (_gtk_container_get_n_children (GTK_CONTAINER (widget)) == 0)) {
		int focus_width, focus_pad;
		int x, y, width, height;
		
		gtk_widget_style_get (widget,
				      "focus-line-width", &focus_width,
				      "focus-padding", &focus_pad,
				      NULL);

		x = widget->allocation.x + focus_pad;
		y = widget->allocation.y + focus_pad;
		width = widget->allocation.width -  2 * focus_pad;
		height = widget->allocation.height - 2 * focus_pad;
		
		gtk_paint_focus (widget->style, 
				 widget->window,
				 GTK_WIDGET_STATE (widget),
				 &event->area, 
				 widget, "box",
				 x, y, width, height);
	}

	return FALSE;
}


static gboolean
quick_box_button_press (GtkWidget         *widget,
			GdkEventButton    *event)
{
	return FALSE;
}


static void
quick_box_finalize (GObject *object)
{
        QuickBox* quick_box;

        g_return_if_fail (object != NULL);
        g_return_if_fail (QUICK_IS_BOX (object));
  
	quick_box = QUICK_BOX (object);

	g_free (quick_box->priv->col_size);
	g_free (quick_box->priv->buttons_on_col);
	g_object_unref (quick_box->priv->icon_theme);
	g_free (quick_box->priv);

	/* Chain up */
	G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
quick_box_class_init (QuickBoxClass    *class)
{
	GtkWidgetClass    *widget_class = (GtkWidgetClass*) class;
	GObjectClass      *gobject_class = (GObjectClass*) class;
	GtkContainerClass *container_class = GTK_CONTAINER_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	gobject_class->finalize = quick_box_finalize;
	widget_class->size_request = quick_box_size_request;
	widget_class->size_allocate = quick_box_size_allocate;
	widget_class->button_press_event = quick_box_button_press;
	widget_class->expose_event = quick_box_expose;

#if 0
	container_class->add = quick_box_add;
	container_class->remove = quick_box_remove;
#endif
	container_class->forall = quick_box_forall;
}


static gboolean
item_activated (QuickButton *button)
{
	GKeyFile *desktop_entry;

	desktop_entry = g_object_get_data (G_OBJECT (button), "desktop_entry");
	if (desktop_entry != NULL)
		_g_desktop_entry_launch (desktop_entry, GTK_WIDGET (button), NULL);
		
	return TRUE;
}


static void
popup_menu_position (GtkMenu   *menu,
		     int       *x,
		     int       *y,
		     gboolean  *push_in,
		     gpointer	user_data)
{
	QuickBox       *quick_box = user_data;
	GtkRequisition  requisition;
	GtkAllocation   allocation;
	int             ox, oy;
	
	gtk_widget_get_child_requisition (GTK_WIDGET (menu), &requisition);
	allocation = get_arrow_widget (quick_box)->allocation;

	switch (quick_box->priv->orient) {
	case PANEL_APPLET_ORIENT_RIGHT:
		*x = allocation.x + allocation.width;
		*y = allocation.y;
		break;
	case PANEL_APPLET_ORIENT_LEFT:
		*x = allocation.x - requisition.width;
		*y = allocation.y;
		break;
	case PANEL_APPLET_ORIENT_DOWN:
		*x = allocation.x;
		*y = allocation.y + allocation.height;
		break;
	case PANEL_APPLET_ORIENT_UP:
		*x = allocation.x;
		*y = allocation.y - requisition.height;
		break;
	default:
		return;
	}

	gdk_window_get_origin (GTK_WIDGET (quick_box)->window, &ox, &oy);
	*x += ox;
	*y += oy;

	*x = MIN (gdk_screen_width () - requisition.width, *x);
	*y = MIN (gdk_screen_height () - requisition.height, *y);

	*x = MAX (0, *x);
	*y = MAX (0, *y);

	*push_in = TRUE;
}


static gboolean
button_button_press_cb (GtkWidget      *button,
			GdkEventButton *event,
			gpointer        data)
{
	QuickBox  *quick_box = QUICK_BOX (button->parent);
	GtkWidget *popup_menu;

	if ((event->button != 3) 
	    || (quick_box->priv->populate_menu_func == NULL))
		return FALSE;

	popup_menu = gtk_menu_new ();
	gtk_menu_set_screen (GTK_MENU (popup_menu),
			     gtk_widget_get_screen (GTK_WIDGET (quick_box)));

	if (quick_box->priv->popup_menu != NULL)
		g_signal_connect_swapped (G_OBJECT (popup_menu),
					  "deactivate",
					  G_CALLBACK (gtk_menu_shell_deactivate),
					  G_OBJECT (quick_box->priv->popup_menu));

	(*quick_box->priv->populate_menu_func) (popup_menu,
						button,
						quick_box->priv->populate_menu_data);
	
	gtk_menu_popup (GTK_MENU (popup_menu),
			NULL, NULL, 
			NULL, /* popup_menu_position, FIXME */
			quick_box, 
			3,
			event->time);
	
	return TRUE;
}


static void  
menu_item__drag_begin  (GtkWidget        *widget,
			GdkDragContext   *context,
			gpointer          data)
{
	QuickBox *quick_box = data;
	
	gtk_widget_set_has_tooltip (GTK_WIDGET (quick_box), FALSE);
}


static void  
menu_item__drag_end  (GtkWidget        *widget,
		      GdkDragContext   *context,
		      gpointer          data)
{
	QuickBox *quick_box = data;
	
	gtk_widget_set_has_tooltip (GTK_WIDGET (quick_box), TRUE);
}


static void  
menu_item__drag_data_get  (GtkWidget        *widget,
			   GdkDragContext   *context,
			   GtkSelectionData *selection_data,
			   guint             info,
			   guint             time,
			   gpointer          data)
{
	QuickButton *button = data;
        char        *target;
	const char  *uri;

        target = gdk_atom_name (selection_data->target);
        if (strcmp (target, "text/uri-list") != 0) {
		g_free (target);
		return;
	}
        g_free (target);

	uri = g_object_get_data (G_OBJECT (button), "uri");
	if (uri == NULL) 
                return;

        gtk_selection_data_set (selection_data, 
                                selection_data->target,
                                8, 
				(guchar*) uri,
                                strlen (uri));
}


static void deactivate_arrow (QuickBox *quick_box);


static void
display_popup_menu (QuickBox *quick_box)
{
	GtkWidget  *popup_menu = NULL;
	GtkWidget  *item;
	GList      *children = NULL, *scan;
	gboolean    menu_empty = TRUE;
	
	if (quick_box->priv->popup_menu != NULL) {
		gtk_menu_shell_deactivate (GTK_MENU_SHELL (quick_box->priv->popup_menu));
		quick_box->priv->popup_menu = NULL;
		return;
	}

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));

	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		GKeyFile  *desktop_entry;
		char      *name;
		char      *icon;
		GtkWidget *image = NULL;
		char      *comment;
		char      *tip;

		if ((child_widget != NULL) && GTK_WIDGET_VISIBLE (child_widget))
			continue;

		desktop_entry = g_object_get_data (G_OBJECT (child_widget), "desktop_entry");

		if (desktop_entry == NULL) { /* add separator */
			
			/* do not display the separator if it is the first 
			 * item in the menu. */

			if (menu_empty)
				continue;

			item = gtk_separator_menu_item_new ();
			if (quick_box->priv->orient == PANEL_APPLET_ORIENT_UP)
				gtk_menu_shell_prepend (GTK_MENU_SHELL (popup_menu), item);
			else
				gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);
			gtk_widget_show (item);

			continue;
		}

		/* add launcher */

		if (menu_empty) {
			quick_box->priv->popup_menu = popup_menu = gtk_menu_new ();
			menu_empty = FALSE;
		}

		/**/

		name = g_key_file_get_locale_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL); 
		item = gtk_image_menu_item_new_with_label (name);
		gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (item), TRUE);

		icon = g_key_file_get_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
		if (icon != NULL)
			image = create_image (quick_box->priv->icon_theme, icon, ICON_SIZE_MENU);
		if (image != NULL)
			gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);

		/**/

		comment = g_key_file_get_locale_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
		
		if ((comment != NULL) && (strcmp (comment, "") != 0))
			tip = g_strdup (comment);
		else
			tip = g_strdup (name);
		gtk_widget_set_tooltip_text  (GTK_WIDGET (quick_box), tip);
		
		/**/

		if (quick_box->priv->orient == PANEL_APPLET_ORIENT_UP)
			gtk_menu_shell_prepend (GTK_MENU_SHELL (popup_menu), item);
		else
			gtk_menu_shell_append (GTK_MENU_SHELL (popup_menu), item);

		g_signal_connect_swapped (G_OBJECT (item), 
					  "activate",
					  G_CALLBACK (item_activated), 
					  child_widget);
		g_signal_connect_swapped (G_OBJECT (item), 
					  "button_press_event",
					  G_CALLBACK (button_button_press_cb), 
					  child_widget);		

		/**/

		gtk_drag_source_set (item,
				     GDK_BUTTON1_MASK,
				     target_table, n_targets, 
				     GDK_ACTION_COPY);
		
		g_signal_connect (G_OBJECT (item),
				  "drag_data_get",
				  G_CALLBACK (menu_item__drag_data_get), 
				  child_widget);

		g_signal_connect (G_OBJECT (item),
				  "drag_begin",
				  G_CALLBACK (menu_item__drag_begin), 
				  quick_box);

		g_signal_connect (G_OBJECT (item),
				  "drag_end",
				  G_CALLBACK (menu_item__drag_end), 
				  quick_box);

		gtk_widget_show (item);
		
		g_free (tip);
		g_free (comment);
		g_free (icon);
		g_free (name);
	}

	g_list_free (children);

	if (menu_empty) {
		deactivate_arrow (quick_box);
		return;
	}

	gtk_widget_set_tooltip_text (GTK_WIDGET (get_arrow_widget (quick_box)), _("Hide launchers menu"));

	g_signal_connect_swapped (G_OBJECT (popup_menu),
				  "deactivate",
				  G_CALLBACK (deactivate_arrow), 
				  quick_box);

        gtk_menu_set_screen (GTK_MENU (popup_menu),
                             gtk_widget_get_screen (GTK_WIDGET (quick_box)));

	gtk_menu_popup (GTK_MENU (popup_menu),
			NULL, NULL, 
			popup_menu_position, 
			quick_box, 
			0,
			GDK_CURRENT_TIME);
}


static int
arrow_button_toggled (GtkToggleButton *tbutton,
		      QuickBox        *quick_box)
{
	g_return_val_if_fail (QUICK_IS_BOX (quick_box), TRUE);
	display_popup_menu (quick_box);
	return TRUE;
}


static void
deactivate_arrow (QuickBox *quick_box)
{
	GtkWidget *arrow;

	arrow = get_arrow_widget (quick_box);
	g_signal_handlers_block_by_func (G_OBJECT (arrow),
					 G_CALLBACK (arrow_button_toggled),
					 quick_box);
	
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (arrow), FALSE);
	gtk_widget_set_tooltip_text (GTK_WIDGET (arrow), _("Show hidden launchers"));
	
	g_signal_handlers_unblock_by_func (G_OBJECT (arrow),
					   G_CALLBACK (arrow_button_toggled),
					   quick_box);

	quick_box->priv->popup_menu = NULL;
}


static gboolean
arrow_button_do_not_eat_button_press (GtkWidget	*widget,
				      GdkEventButton *event)
{
	if (event->button != 1) {
		g_signal_stop_emission_by_name (widget, "button_press_event");
	}

	return FALSE;
}


static GtkWidget *
create_arrow_button (QuickBox     *quick_box,
		     GtkArrowType  arrow_type)
{
	GtkWidget *button;
	GtkWidget *arrow;

	button = gtk_toggle_button_new ();
	arrow = gtk_arrow_new (arrow_type, GTK_SHADOW_IN);
	gtk_widget_show (arrow);
	gtk_container_add (GTK_CONTAINER (button), arrow);

	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_widget_set_parent (button, GTK_WIDGET (quick_box));

	gtk_widget_set_tooltip_text (GTK_WIDGET (button), _("Show hidden launchers"));
	g_signal_connect (G_OBJECT (button), 
			  "button_press_event",
			  G_CALLBACK (arrow_button_do_not_eat_button_press), 
			  NULL);
	g_signal_connect (G_OBJECT (button), 
			  "toggled",
			  G_CALLBACK (arrow_button_toggled), 
			  quick_box);
	gtk_widget_hide (button);

	return button;
}


static void
quick_box_init (QuickBox *quick_box)
{
	GtkBox          *box = GTK_BOX (quick_box);
	QuickBoxPrivate *priv = quick_box->priv;

	box->spacing = ICON_SPACING;
	box->homogeneous = FALSE;
	gtk_container_set_resize_mode (GTK_CONTAINER (quick_box),
				       GTK_RESIZE_QUEUE);

	priv = quick_box->priv = g_new0 (QuickBoxPrivate, 1);
	priv->size = ICON_SIZE_PANEL;
	priv->orient = PANEL_APPLET_ORIENT_DOWN;
	priv->drag_pos = -1;
	priv->drag_pos_begin = -1;
	priv->rows = 1;
	priv->min_visible_cols = DEFAULT_MIN_VISIBLE_COLS;
	priv->max_visible_cols = DEFAULT_MAX_VISIBLE_COLS;
	priv->col_size = NULL;
	priv->buttons_on_col = NULL;

	priv->populate_menu_func = NULL;
	priv->populate_menu_data = NULL;

	priv->arrow_up = create_arrow_button (quick_box, GTK_ARROW_UP);
	priv->arrow_down = create_arrow_button (quick_box, GTK_ARROW_DOWN);
	priv->arrow_left = create_arrow_button (quick_box, GTK_ARROW_LEFT);
	priv->arrow_right = create_arrow_button (quick_box, GTK_ARROW_RIGHT);
}


GType
quick_box_get_type ()
{
        static GType type = 0;

        if (! type) {
                GTypeInfo type_info = {
			sizeof (QuickBoxClass),
			NULL,
			NULL,
			(GClassInitFunc) quick_box_class_init,
			NULL,
			NULL,
			sizeof (QuickBox),
			0,
			(GInstanceInitFunc) quick_box_init
		};

		type = g_type_register_static (GTK_TYPE_BOX,
					       "QuickBox",
					       &type_info,
					       0);
	}

        return type;
}


int
quick_box_get_pointer_position (QuickBox *quick_box, 
				int       x, 
				int       y)
{
	GtkBox *box = GTK_BOX (quick_box);
	GList  *children, *scan;
	int     pos = 0;
	int     same_col_pos = -1;
	int     same_row_pos = -1;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	if (children == NULL)
		return -1;

	for (scan = children; scan; scan = scan->next) {
		GtkWidget    *child_widget = scan->data;
		GdkRectangle  rect = child_widget->allocation;
		
		rect.x      -= box->spacing;
		rect.y      -= box->spacing;
		rect.width  += box->spacing * 2;
		rect.height += box->spacing * 2;
		
		if ((x >= rect.x) && (x <= rect.x + rect.width)
		    && (y >= rect.y) && (y <= rect.y + rect.height))
		{
			return pos;
		}
		
		if ((x >= rect.x) && (x <= rect.x + rect.width))
			same_col_pos = pos;

		if ((y >= rect.y) && (y <= rect.y + rect.height))
			same_row_pos = pos;

		pos++;
	}
	g_list_free (children);

	if (get_gtk_orientation (quick_box) == GTK_ORIENTATION_HORIZONTAL)
		return same_col_pos + 1;
	else
		return same_row_pos + 1;
}


static QuickDropPos
get_drop_position_for_rect (QuickBox     *quick_box, 
			    GdkRectangle *rect, 
			    int           x, 
			    int           y)
{
	gboolean before;
	gboolean inside;
	float    dx;
	float    dy;
	float    border_x;
	float    border_y;
	
	dx = x - rect->x;
	dy = y - rect->y;
	border_x = ((float) rect->width) / 4.0;
	border_y = ((float) rect->height) / 4.0;
	
	switch (get_gtk_orientation (quick_box)) {
	case GTK_ORIENTATION_HORIZONTAL:
		before = dx < (((float) rect->width) / 2.0);
		inside = (dx > border_x) && (dx < rect->width - border_x);
		break;
	case GTK_ORIENTATION_VERTICAL:
		before = dy < (((float) rect->height) / 2.0);
		inside = (dy > border_y) && (dy < rect->height - border_y);
		break;
	}
	
	if (before) {
		if (inside) 
			return QUICK_DROP_POSITION_INTO_OR_BEFORE;
		else
			return QUICK_DROP_POSITION_BEFORE;
	}
	else {
		if (inside) 
			return QUICK_DROP_POSITION_INTO_OR_AFTER;
		else
			return QUICK_DROP_POSITION_AFTER;
	}
	
	return QUICK_DROP_POSITION_BEFORE;
}


int
quick_box_get_drop_position (QuickBox     *quick_box, 
			     int           x, 
			     int           y,
			     QuickDropPos *drop_position)
{
	GtkBox *box = GTK_BOX (quick_box);
	GList  *children, *scan;
	int     pos;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	if (children == NULL) {
		if (drop_position != NULL)
			*drop_position = QUICK_DROP_POSITION_BEFORE;
		return -1;
	}

	pos = 0;
	for (scan = children; scan; scan = scan->next) {
		GtkWidget    *child_widget = scan->data;
		GdkRectangle  rect = child_widget->allocation;
		
		if (! QUICK_IS_BUTTON (child_widget))
			continue;
		
		rect.x      -= box->spacing;
		rect.y      -= box->spacing;
		rect.width  += box->spacing * 2;
		rect.height += box->spacing * 2;
		
		if ((x >= rect.x) && (x <= rect.x + rect.width)
		    && (y >= rect.y) && (y <= rect.y + rect.height))
		{
			if (drop_position != NULL) 
				*drop_position = get_drop_position_for_rect (quick_box, &rect, x, y);
			g_list_free (children);
			return pos;
		}

		pos++;
	}
	g_list_free (children);
	
	if (drop_position != NULL) 
		*drop_position = QUICK_DROP_POSITION_AFTER;
	
	return pos - 1;
}


static void
draw_drag_destination (QuickBox     *quick_box,
		       int           pos,
		       QuickDropPos  drop_pos)
{
	GtkWidget    *widget = GTK_WIDGET (quick_box);
	GdkRectangle  r;

	if ((pos == quick_box->priv->drag_pos) && (drop_pos == quick_box->priv->drop_position)) 
		return;

	if (pos != quick_box->priv->drag_pos) {
		quick_button_set_dnd_highlight (QUICK_BUTTON (quick_box_get_child_at_pos (quick_box, quick_box->priv->drag_pos)), FALSE);
		get_drag_destination_rect (quick_box, 
					   quick_box->priv->drag_pos,
					   &r);
		gtk_widget_queue_draw_area (widget, 
					    r.x,
					    r.y,
					    r.width,
					    r.height);
	}

	quick_box->priv->drag_pos = pos;
	quick_box->priv->drop_position = drop_pos;

	if ((quick_box->priv->drop_position == QUICK_DROP_POSITION_INTO_OR_BEFORE)
	    || (quick_box->priv->drop_position == QUICK_DROP_POSITION_INTO_OR_AFTER))
	{
		quick_button_set_dnd_highlight (QUICK_BUTTON (quick_box_get_child_at_pos (quick_box, quick_box->priv->drag_pos)), TRUE);
	}
	else
		quick_button_set_dnd_highlight (QUICK_BUTTON (quick_box_get_child_at_pos (quick_box, quick_box->priv->drag_pos)), FALSE);
		
	get_drag_destination_rect (quick_box, pos, &r);
	gtk_widget_queue_draw_area (widget, 
				    r.x,
				    r.y,
				    r.width,
				    r.height);
}


static gboolean
drag_motion (GtkWidget          *widget,
	     GdkDragContext     *context,
	     gint                x,
	     gint                y,
	     guint               time)
{
	QuickBox     *quick_box = QUICK_BOX (widget);
	int           pos;
	QuickDropPos  drop_pos;
	
	pos = quick_box_get_drop_position (quick_box, x, y, &drop_pos);
	if ((pos == -1) && (_gtk_container_get_n_children (GTK_CONTAINER (widget)) == 0))
		pos = 0;
	draw_drag_destination (quick_box, pos, drop_pos);

	return TRUE;
}


static void 
drag_leave (GtkWidget          *widget,
	    GdkDragContext     *context,
	    guint               time)
{
	QuickBox      *quick_box = QUICK_BOX (widget);
	GdkRectangle   r;

	if (quick_box->priv->drag_pos == -1)
		return;

	quick_box->priv->drag_pos_begin = -1;

	quick_button_set_dnd_highlight (QUICK_BUTTON (quick_box_get_child_at_pos (quick_box, quick_box->priv->drag_pos)), FALSE);
	get_drag_destination_rect (quick_box, quick_box->priv->drag_pos, &r);
	quick_box->priv->drag_pos = -1;
	gtk_widget_queue_draw_area (widget, 
				    r.x,
				    r.y,
				    r.width,
				    r.height);
}


static void
button_drag_started_cb (GtkWidget *button,
			QuickBox  *quick_box)
{
	quick_box->priv->drag_pos_begin = quick_box_get_drop_position (quick_box, button->allocation.x + 1, button->allocation.y + 1, NULL);
}


void
quick_box_icon_theme_changed (QuickBox *quick_box)
{
	int    child_size;
	GList *children, *scan;
	
	child_size = get_child_size (quick_box);

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		GKeyFile  *desktop_entry;
		char      *icon;

		if (! QUICK_IS_BUTTON (child_widget))
			continue;

		desktop_entry = g_object_get_data (G_OBJECT (child_widget), "desktop_entry");
		if (desktop_entry == NULL)
			continue;

		icon = g_key_file_get_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
		quick_button_set_pixmap (QUICK_BUTTON (child_widget), icon, child_size);
		g_free (icon);
	}
	g_list_free (children);
}


GtkWidget *
quick_box_add_button (QuickBox   *quick_box,
		      const char *uri,
		      int         pos)
{
	char       *filename;
	GKeyFile   *desktop_entry;
	char       *name;
	GtkWidget  *button;
	char       *comment;
	char       *tip;
	char       *icon_path;
	int         child_size;

	filename = g_filename_from_uri (uri, NULL, NULL);
	
	desktop_entry = g_key_file_new ();
	if (! g_key_file_load_from_file (desktop_entry, filename, G_KEY_FILE_KEEP_TRANSLATIONS, NULL)) {
		g_free (filename);
		return NULL;
	}
	_g_key_file_cleanup (desktop_entry);

	g_free (filename);
	
	icon_path = g_key_file_get_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	child_size = get_child_size (quick_box);
	name = g_key_file_get_locale_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL);
	button = quick_button_new (icon_path, child_size, name, quick_box->priv->icon_theme);
	
	if (button == NULL) 
		return NULL;
	
	g_signal_connect_swapped (G_OBJECT (button), 
				  "clicked",
				  G_CALLBACK (item_activated), 
				  button);
	g_signal_connect (G_OBJECT (button), 
			  "button_press_event",
			  G_CALLBACK (button_button_press_cb), 
			  NULL);
	g_signal_connect (G_OBJECT (button), 
			  "drag_started",
			  G_CALLBACK (button_drag_started_cb), 
			  quick_box);

	g_object_set_data_full (G_OBJECT (button), 
				"desktop_entry", 
				desktop_entry,
				(GDestroyNotify) g_key_file_free);
	g_object_set_data_full (G_OBJECT (button),
				"uri", 
				g_strdup (uri),
				g_free);

	/* tip */

	comment = g_key_file_get_locale_string (desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
	tip = g_strconcat (name,
			   ((comment != NULL) && (*comment != 0)) ? "\n" : NULL,
			   ((comment != NULL) && (*comment != 0)) ? comment: NULL,
			   NULL);
	gtk_widget_set_tooltip_text (button, tip);
	
	g_free (tip);
	g_free (comment);
	g_free (icon_path);
	g_free (name);

	/**/

	gtk_box_pack_start (GTK_BOX (quick_box), button, 0, 0, 0);
	if (pos != -1)
		quick_box_reorder_child (quick_box, button, pos);

	gtk_widget_show (button);

	return button;
}


GtkWidget *
quick_box_add_separator (QuickBox *quick_box,
			 int       pos)
{
	GtkWidget *quick_sep;
	int        max_size;

	max_size = quick_box_get_size (quick_box) - GTK_CONTAINER (quick_box)->border_width * 2;
	quick_sep = quick_separator_new (get_child_size (quick_box), 
					 max_size,
					 get_gtk_orientation (quick_box));

	if (quick_sep == NULL) 
		return NULL;

	g_signal_connect (G_OBJECT (quick_sep), 
			  "button_press_event",
			  G_CALLBACK (button_button_press_cb), 
			  NULL);

	g_object_set_data (G_OBJECT (quick_sep), "desktop_entry", NULL);	

	gtk_box_pack_start (GTK_BOX (quick_box), quick_sep, 0, 0, 0);
	if (pos != -1)
		quick_box_reorder_child (quick_box, quick_sep, pos);

	gtk_widget_show (quick_sep);
	
	return quick_sep;
}


GtkWidget*     
quick_box_new (PanelAppletOrient  orient,
	       int                size,
	       GtkIconTheme      *icon_theme)
{
	QuickBox *quick_box;

	g_return_val_if_fail (icon_theme != NULL, NULL);

	quick_box = QUICK_BOX (g_object_new (QUICK_TYPE_BOX, NULL));
	quick_box->priv->orient = orient;
	quick_box->priv->size   = size;
	quick_box->priv->rows   = MAX (1, size / ICON_SIZE_PANEL);
	quick_box->priv->icon_theme = g_object_ref (icon_theme);

	g_signal_connect (G_OBJECT (quick_box), 
			  "drag_motion",
			  G_CALLBACK (drag_motion), 
			  quick_box);
	g_signal_connect (G_OBJECT (quick_box), 
			  "drag_leave",
			  G_CALLBACK (drag_leave), 
			  quick_box);

	return GTK_WIDGET (quick_box);
}


static void
set_separator_orient_cb (GtkWidget *widget, 
			 gpointer   data)
{
	GtkOrientation orient = GPOINTER_TO_INT (data);
	
	if (! QUICK_IS_SEPARATOR (widget))
		return;
	
	quick_separator_set_orient (QUICK_SEPARATOR (widget), orient);
}


void
quick_box_set_orient (QuickBox          *quick_box, 
		      PanelAppletOrient  orient)
{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->orient = orient;

	gtk_container_foreach (GTK_CONTAINER (quick_box),
			       (GtkCallback) set_separator_orient_cb,
			       GINT_TO_POINTER (get_gtk_orientation (quick_box)));
	gtk_widget_queue_resize (GTK_WIDGET (quick_box));
}


PanelAppletOrient
quick_box_get_orient (QuickBox *qbox)
{
	return qbox->priv->orient;
}


static void
set_button_size_cb (GtkWidget *widget, 
		    gpointer   data)
{
	QuickBox *quick_box = QUICK_BOX (widget->parent);
	int       size = GPOINTER_TO_INT (data);

	if (! QUICK_IS_BUTTON (widget))
		return;
	
	if (QUICK_IS_SEPARATOR (widget))
		quick_separator_set_size (QUICK_SEPARATOR (widget), 
					  size,
					  quick_box->priv->size);
	else if (QUICK_IS_BUTTON (widget))
		quick_button_set_pixmap (QUICK_BUTTON (widget), 
					 QUICK_BUTTON (widget)->icon, 
					 size);
}


void
quick_box_set_size (QuickBox *quick_box, 
		    int       size)
{
	int child_size;

	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->size = size;
	
	child_size = get_child_size (quick_box);
	gtk_container_foreach (GTK_CONTAINER (quick_box),
			       (GtkCallback) set_button_size_cb,
			       GINT_TO_POINTER (child_size));
	gtk_widget_queue_resize (GTK_WIDGET (quick_box));
}


int
quick_box_get_size (QuickBox *quick_box)
{
	g_return_val_if_fail (QUICK_IS_BOX (quick_box), 0);
	return quick_box->priv->size;
}


void
quick_box_set_min_visible_cols (QuickBox *quick_box,
				int       cols)
{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->min_visible_cols = cols;
	gtk_widget_queue_resize (GTK_WIDGET (quick_box));
}


int
quick_box_get_min_visible_cols (QuickBox *quick_box)
{
	g_return_val_if_fail (QUICK_IS_BOX (quick_box), 0);
	return quick_box->priv->min_visible_cols;
}


void
quick_box_set_max_visible_cols (QuickBox *quick_box,
				int       value)
{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->max_visible_cols = value;
	gtk_widget_queue_resize (GTK_WIDGET (quick_box));
}


int
quick_box_get_max_visible_cols (QuickBox *quick_box)
{
	g_return_val_if_fail (QUICK_IS_BOX (quick_box), 0);
	return quick_box->priv->max_visible_cols;
}


void
quick_box_set_rows (QuickBox *quick_box,
		    int       value)

{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->rows = value;
	quick_box_set_size (quick_box, quick_box->priv->size);
}


int
quick_box_get_rows (QuickBox *quick_box)
{
	g_return_val_if_fail (QUICK_IS_BOX (quick_box), 0);

	return quick_box->priv->rows;
}


const int *
quick_box_get_size_hint_list (QuickBox *quick_box,
			      int      *n_elements)
{
	*n_elements = 2;
	return quick_box->priv->size_hint;
}


void
quick_box_reorder_child (QuickBox  *quick_box,
			 GtkWidget *child,
			 int        pos)
{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	gtk_box_reorder_child (GTK_BOX (quick_box), child, pos);
	if (! GTK_WIDGET_VISIBLE (child) && GTK_WIDGET_VISIBLE (quick_box))
		gtk_widget_queue_resize (child);
}


void
quick_box_populate_menu_func (QuickBox          *quick_box,
			      PopulateMenuFunc   pm_func,
			      gpointer           data)
{
	g_return_if_fail (QUICK_IS_BOX (quick_box));

	quick_box->priv->populate_menu_func = pm_func;
	quick_box->priv->populate_menu_data = data;
}


void
quick_box_update_child (QuickBox    *quick_box,
			QuickButton *quick_button,
			GKeyFile    *desktop_entry)
{
	GKeyFile *new_desktop_entry;
	char     *icon;
	char     *name;
	char     *comment;
	char     *tip;

	g_return_if_fail (QUICK_IS_BOX (quick_box));
	g_return_if_fail (QUICK_IS_BUTTON (quick_button));

	new_desktop_entry = _g_key_file_dup (desktop_entry);

	g_object_set_data_full (G_OBJECT (quick_button), 
				"desktop_entry", 
				new_desktop_entry,
				(GDestroyNotify) g_key_file_free);

	icon = g_key_file_get_string (new_desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, NULL);
	quick_button_set_pixmap (quick_button, icon, get_child_size (quick_box));
	
	name = g_key_file_get_locale_string (new_desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL, NULL);
	quick_button_set_text (quick_button, name);

	comment = g_key_file_get_locale_string (new_desktop_entry, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, NULL, NULL);
	tip = g_strconcat (name, 
			   ((comment != NULL) && (*comment != 0)) ? "\n" : NULL, 
			   comment, 
			   NULL);
	gtk_widget_set_tooltip_text (GTK_WIDGET (quick_button), tip);

	g_free (tip);
	g_free (comment);
	g_free (name);
	g_free (icon);
}


int
quick_box_get_child_position (QuickBox  *quick_box,
			      GtkWidget *child_to_find)
{
	GList    *children, *scan;
	gboolean  found = FALSE;
	int       pos;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (pos = 0, scan = children; scan; scan = scan->next) {
		GtkWidget *child_widget = scan->data;
		
		if (! QUICK_IS_BUTTON (child_widget))
			continue;
		
		if (child_widget == child_to_find) {
			found = TRUE;
			break;
		}
		
		pos++;
	}
	g_list_free (children);

	if (! found)
		return -1;
	else
		return pos;
}


GtkWidget *
quick_box_get_child_at_pos (QuickBox *quick_box,
			    int       pos)
{
	GtkWidget *child_widget = NULL;
	GList     *children, *scan;
	int        i;
	    
	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (i = 0, scan = children; scan; scan = scan->next) {
		child_widget = scan->data;		
		if (! QUICK_IS_BUTTON (child_widget))
			continue;
			
		if (pos == i)
			break;
			
		i++;
	}
	g_list_free (children);
	
	return child_widget;
}


GtkWidget *
quick_box_get_child_from_uri (QuickBox *quick_box,
			      char     *uri)
{
	GList *children, *scan;

	if (uri == NULL)
		return NULL;

	children = gtk_container_get_children (GTK_CONTAINER (quick_box));
	for (scan = children; scan; scan = scan->next) {
		GtkWidget  *child_widget = scan->data;
		const char *uri2;

		uri2 = g_object_get_data (G_OBJECT (child_widget), "uri");
		if (uri2 == NULL)
			continue;
		if (strcmp (uri, uri2) == 0)
			return child_widget;
	}
	g_list_free (children);

	return NULL;
}
