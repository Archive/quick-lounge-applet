/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef QUICK_LOUNGE_H
#define QUICK_LOUNGE_H

#include <gtk/gtk.h>
#include <panel-applet.h>
#include "file-utils.h"
#include "quick-box.h"


typedef struct {
	GtkWidget         *applet;
	GtkWidget         *prop_dialog;
	QuickBox          *quick_box;
	gboolean           loading;
	char              *location;
	int                size;
	PanelAppletOrient  orientation;
	GtkIconTheme      *icon_theme;
} QuickLounge;


void      quick_lounge_save_order                  (QuickLounge  *quick_lounge);
char *    quick_lounge_get_path                    (QuickLounge  *quick_lounge);
void      quick_lounge_load_uri                    (QuickLounge  *quick_lounge, 
						    const char   *uri);
gboolean  quick_lounge_set_min_visible_cols        (QuickLounge  *quick_lounge, 
						    int           cols);
gboolean  quick_lounge_set_max_size                (QuickLounge  *quick_lounge, 
						    int           value);
void	  quick_lounge_set_rows			   (QuickLounge  *quick_lounge,
						    int		  value);
void      quick_lounge_new_launcher                (QuickLounge  *quick_lounge,
						    int           pos);
gboolean  quick_lounge_add_launcher                (QuickLounge  *quick_lounge,
						    GKeyFile     *desktop_entry,
						    int           pos);
						    
/* utils */

void    quick_lounge_util__item_properties         (QuickLounge  *quick_lounge,
						    GtkWidget    *button);
char *  quick_lounge_util__get_unique_uri          (QuickLounge  *quick_lounge);

#endif /* QUICK_LOUNGE_H */
