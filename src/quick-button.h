/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  Quick Lounge Applet
 *
 *  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifndef QUICK_BUTTON_H
#define QUICK_BUTTON_H

#include <gtk/gtk.h>

#define QUICK_TYPE_BUTTON            (quick_button_get_type ())
#define QUICK_BUTTON(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), QUICK_TYPE_BUTTON, QuickButton))
#define QUICK_BUTTON_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), QUICK_TYPE_BUTTON, QuickButtonClass))
#define QUICK_IS_BUTTON(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), QUICK_TYPE_BUTTON))
#define QUICK_IS_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), QUICK_TYPE_BUTTON))
#define QUICK_BUTTON_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), QUICK_TYPE_BUTTON, QuickButtonClass))

typedef struct _QuickButton		QuickButton;
typedef struct _QuickButtonClass	QuickButtonClass;

struct _QuickButton
{
	GtkButton __parent;

	char            *name;
	char            *icon;
	int              size;
	
	guint            dnd_highlight : 1;
	guint            pressed_timeout;
	GdkPixbuf       *pixbuf;
	GdkPixbuf       *scaled;
	GdkPixbuf       *scaled_bright;
	GtkIconTheme    *icon_theme;
};

struct _QuickButtonClass
{
	GtkButtonClass parent_class;

	/*< virtual functions >*/

	void (*set_pixmap) (QuickButton *button,
			    const char  *icon,
			    int          size);

	/*< signals >*/

	void (*drag_started) (QuickButton *button);
};

GType		quick_button_get_type		(void) G_GNUC_CONST;
GtkWidget*	quick_button_new		(const char     *icon_path,
						 int             size,
						 const char     *text,
						 GtkIconTheme   *icon_theme);
void		quick_button_set_pixmap	        (QuickButton    *button,
						 const char     *icon,
						 int             size);
void		quick_button_set_text		(QuickButton    *button,
						 const char     *text);
void		quick_button_set_dnd_highlight	(QuickButton    *button,
						 gboolean        highlight);

#endif /* QUICK_BUTTON_H */
