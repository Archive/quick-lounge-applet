# Slovenian translation of quick-lounge-applet
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the alacarte quick-lounge-applet.
#
# Matej Urbančič <mateju@svn.gnome.org>, 2006 - 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: quick-lounge-applet master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=quick-lounge-applet&component=general\n"
"POT-Creation-Date: 2009-04-17 13:26+0000\n"
"PO-Revision-Date: 2010-06-02 14:55+0100\n"
"Last-Translator: Matej Urbančič <mateju@svn.gnome.org>\n"
"Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);\n"
"X-Poedit-Language: Slovenian\n"
"X-Poedit-Country: SLOVENIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../data/GNOME_QuickLoungeApplet.xml.h:1
msgid "_About"
msgstr "_O Programu"

#: ../data/GNOME_QuickLoungeApplet.xml.h:2
msgid "_Help"
msgstr "Pomo_č"

#: ../data/GNOME_QuickLoungeApplet.xml.h:3
msgid "_Preferences"
msgstr "_Možnosti"

#: ../data/GNOME_QuickLoungeApplet_Factory.server.in.in.h:1
msgid "Factory for the Quick Lounge applet"
msgstr "Tovarna za aplet hitrih zagonov"

#: ../data/GNOME_QuickLoungeApplet_Factory.server.in.in.h:2
#: ../src/quick-lounge.c:137
msgid "Launchers List"
msgstr "Seznam zaganjalnikov"

#: ../data/GNOME_QuickLoungeApplet_Factory.server.in.in.h:3
msgid "Organize your favourite applications on the Panel"
msgstr "Urejanje priljubljenih programov pulta"

#: ../data/GNOME_QuickLoungeApplet_Factory.server.in.in.h:4
msgid "Quick Lounge Factory"
msgstr "Tovarna hitrega zaganjanja"

#: ../data/quick-lounge.schemas.in.h:1
msgid "Launchers order"
msgstr "Vrstni red zaganjalnikov"

#: ../data/quick-lounge.schemas.in.h:2
msgid "Location to load"
msgstr "Mesto za nalaganje"

#: ../data/quick-lounge.schemas.in.h:3
msgid "Maximum number of columns to display"
msgstr "Največje število prikazanih stolpcev"

#: ../data/quick-lounge.schemas.in.h:4
msgid "Minimum number of columns to display"
msgstr "Najmanjše število prikazanih stolpcev"

#: ../data/quick-lounge.schemas.in.h:5
#| msgid "Maximum number of columns to display"
msgid "Number of rows to display"
msgstr "Število prikazanih stolpcev"

#: ../data/ui/add-from-menu.ui.h:1
msgid "Add From Menu"
msgstr "Dodaj iz menija"

#: ../data/ui/add-from-menu.ui.h:2
#| msgid "Available applications:"
msgid "_Available applications:"
msgstr "_Programi na voljo:"

#: ../data/ui/add-from-menu.ui.h:3
msgid "_Menu:"
msgstr "_Meni:"

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/desktop-entry-editor.ui.h:2
#: ../data/ui/link-entry-editor.ui.h:2
msgid "<b>C_omment:</b>"
msgstr "<b>_Opomba:</b>"

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/desktop-entry-editor.ui.h:4
msgid "<b>_Command:</b>"
msgstr "<b>_Ukaz:</b>"

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/desktop-entry-editor.ui.h:6
#: ../data/ui/link-entry-editor.ui.h:6
#| msgid "<b>Size</b>"
msgid "<b>_Name:</b>"
msgstr "<b>_Ime:</b>"

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/desktop-entry-editor.ui.h:8
#| msgid "<b>Size</b>"
msgid "<b>_Type:</b>"
msgstr "<b>_Vrsta:</b>"

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/desktop-entry-editor.ui.h:10
#: ../data/ui/link-entry-editor.ui.h:8
msgid "_Browse..."
msgstr "_Prebrskaj ..."

#. Use the same translation used by the GNOME Panel in the Launcher Properties dialog.
#: ../data/ui/link-entry-editor.ui.h:4
#| msgid "<b>Launchers</b>"
msgid "<b>_Location:</b>"
msgstr "<b>_Mesto:</b>"

#. The maximum number of launcher columns to display
#: ../data/ui/properties.ui.h:2
#| msgid "Ma_ximum size:"
msgid ", ma_ximum:"
msgstr ", naj_več:"

#: ../data/ui/properties.ui.h:3
msgid "<b>Launchers</b>"
msgstr "<b>Zaganjalniki</b>"

#: ../data/ui/properties.ui.h:4
msgid "<b>Size</b>"
msgstr "<b>Velikost</b>"

#.
#: ../data/ui/properties.ui.h:5
#: ../src/quick-lounge.c:721
msgid "Add From M_enu..."
msgstr "Dodaj iz _menija ..."

#.
#: ../data/ui/properties.ui.h:6
#: ../src/quick-lounge.c:745
#| msgid "Add _Space"
msgid "Add _Separator"
msgstr "Dodaj _ločilno črto"

#: ../data/ui/properties.ui.h:7
msgid "Launchers List Preferences"
msgstr "Seznam možnosti zaganjalnikov"

#. The minimum number of launcher columns to display
#: ../data/ui/properties.ui.h:9
#| msgid "_Minimum size:"
msgid "_Minimum:"
msgstr "_Najmanj:"

#.
#: ../data/ui/properties.ui.h:10
#: ../src/quick-lounge.c:733
#| msgid "New Launcher"
msgid "_New Launcher"
msgstr "_Nov zaganjalnik"

#: ../src/dlg-properties.c:1041
#| msgid "columns"
msgid "C_olumns:"
msgstr "Stolp_ci:"

#: ../src/dlg-properties.c:1042
msgid "rows"
msgstr "vrstic"

#: ../src/dlg-properties.c:1047
msgid "R_ows:"
msgstr "_Vrstice:"

#: ../src/dlg-properties.c:1048
msgid "columns"
msgstr "stolpci"

#. Translators: %s is a URI
#: ../src/file-utils.c:703
#, c-format
msgid "Open '%s'"
msgstr "Odpri '%s'"

#: ../src/file-utils.c:804
msgid "Could not launch the application"
msgstr "Programa ni mogoče zagnati"

#: ../src/file-utils.c:815
#| msgid "Could not display help"
msgid "Could not show the location"
msgstr "Ni mogoče prikazati mesta"

#: ../src/gtk-utils.c:231
msgid "Quick Lounge"
msgstr "Hitri zagon"

#: ../src/gtk-utils.c:795
msgid "Could not display help"
msgstr "Ni mogoč prikazati pomoči"

#: ../src/quick-box.c:1292
msgid "Hide launchers menu"
msgstr "Skrij meni zaganjalnikov"

#: ../src/quick-box.c:1332
#: ../src/quick-box.c:1369
msgid "Show hidden launchers"
msgstr "Pokaži skrite zaganjalnike"

#: ../src/quick-desktop-entry-dialog.c:262
msgid "Select an Image"
msgstr "Izbor slike"

#: ../src/quick-desktop-entry-dialog.c:358
msgid "Select a Command"
msgstr "Izbor ukaza"

#: ../src/quick-desktop-entry-dialog.c:358
msgid "Select a File"
msgstr "Izbor datoteke"

#: ../src/quick-desktop-entry-dialog.c:514
msgid "Application"
msgstr "Program"

#: ../src/quick-desktop-entry-dialog.c:514
#| msgid "Application"
msgid "Application in Terminal"
msgstr "Program v Terminalu"

#: ../src/quick-lounge.c:125
msgid "translator_credits"
msgstr "Matej Urbančič <mateju@svn.gnome.org>"

#: ../src/quick-lounge.c:140
msgid "Organize your favorite applications on the Panel"
msgstr "Urejanje priljubljenih programov na pultu"

#: ../src/quick-lounge.c:345
#: ../src/quick-lounge.c:355
#: ../src/quick-lounge.c:362
#| msgid "Could not create the new launcher"
msgid "Could not save the launcher"
msgstr "Ni mogoče shraniti zaganjalnika"

#: ../src/quick-lounge.c:389
msgid "Launcher Properties"
msgstr "Lastnosti zaganjalnika"

#: ../src/quick-lounge.c:501
#: ../src/quick-lounge.c:512
#: ../src/quick-lounge.c:558
msgid "Could not create the new launcher"
msgstr "Ni mogoče ustvariti novega zaganjalnika"

#: ../src/quick-lounge.c:585
msgid "New Launcher"
msgstr "Nov zaganjalnik"

#: ../src/quick-lounge.c:695
#| msgid "New Launcher"
msgid "_Launch"
msgstr "_Zaženi"

#.
#: ../src/quick-lounge.c:773
msgid "_Move"
msgstr "_Premakni"

#~ msgid "Could not display help: %s"
#~ msgstr "Ni mogoče prikazati pomoči: %s"
#~ msgid "Description"
#~ msgstr "Opis"
#~ msgid "Space"
#~ msgstr "Presledek"
#~ msgid "Cannot find a terminal, using xterm, even if it may not work"
#~ msgstr "Ni mogoče najti terminala, zato bo uporabljen xterm, čeprav morda ne bo deloval"
#~ msgid "    "
#~ msgstr "    "
#~ msgid "Add Existing Launcher"
#~ msgstr "Dodaj obstoječi zaganjalnik"
#~ msgid "Add _Existing Launcher"
#~ msgstr "Dodaj _obstoječi zaganjalnik"
#~ msgid "Add _New Launcher"
#~ msgstr "Dodaj _nov zaganjalnik"
#~ msgid "Always use one row"
#~ msgstr "Vedno uporabi eno vrstico"
#~ msgid "Collap_se All"
#~ msgstr "_Skrči vse"
#~ msgid "E_xpand All"
#~ msgstr "_Razširi vse"
#~ msgid ""
#~ "Cannot save changes to launcher\n"
#~ "\n"
#~ "Details: %s"
#~ msgstr ""
#~ "Ni mogoče shraniti sprememb v zaganjalnik\n"
#~ "\n"
#~ "Podrobnosti: %s"
#~ msgid "There was an error displaying help: %s"
#~ msgstr "Prišlo je do napake med prikazovanjem pomoči: %s"
#~ msgid "Whether icons size follows the panel size"
#~ msgstr "Ali velikost ikon sledi velikosti pulta"
