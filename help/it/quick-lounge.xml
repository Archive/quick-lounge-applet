<?xml version="1.0"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" 
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "1.1.4" >
  <!ENTITY manrevision "2.0.1" >
  <!ENTITY date "Gennaio 2004" >
  <!ENTITY app "<application>Quick Lounge</application>" >
  <!ENTITY appname "Quick Lounge" >
  <!ENTITY ug "Manuale utente del desktop GNOME" >
]><!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
--><!-- =============Document Header ============================= -->
 <article id="index" lang="it"><!-- please do not change the id; for translations, change lang to --><!-- appropriate code -->


  <articleinfo> 
    <title>Manuale di &appname; V&manrevision; </title>       
    <copyright><year>2004</year> <holder>Sun Microsystems</holder>  </copyright><!-- translators: uncomment this:

  <copyright>
   <year>2004</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

    <publisher><publishername>GNOME Documentation Project</publishername>  </publisher><!-- This file  contains link to license for the documentation (GNU FDL), and 
        other legal stuff such as "NO WARRANTY" statement. Please do not change 
	any of this. -->&legal;  <authorgroup> 
      <author><firstname>Sun</firstname> <surname>GNOME Documentation Team</surname> <affiliation><orgname>Sun Microsystems</orgname> </affiliation>
      </author><!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->

    </authorgroup>


    <revhistory>
      <revision><revnumber>Manuale dell'applet &appname;  V&manrevision;</revnumber> <date>&date;</date> <revdescription> 
          <para role="author">Sun GNOME Documentation Team</para>
          <para role="publisher">GNOME Documentation Project </para>
	</revdescription> 
      </revision> 
    </revhistory><releaseinfo>Questo manuale descrive la versione &appversion; dell'applet &appname;.</releaseinfo>  <legalnotice> 
      <title>Commenti</title> 
      <para>Per segnalare un problema o inviare suggerimenti sull'applet &app; o su questo manuale, seguire le istruzioni presenti alla <ulink url="ghelp:gnome-feedback" type="help">Pagina di commenti su GNOME</ulink>.  </para><!-- Translators may also add here feedback address for translations -->

    </legalnotice> 
  </articleinfo><!-- ============= Introduction  ================================ --><indexterm zone="index"> 
    <primary>Applet Quick Lounge</primary> 
  </indexterm> <sect1 id="quick-lounge-intro">
    <title>Introduzione</title>

    <figure id="quick-lounge-fig"> 
      <title>Applet &appname;</title> 
      <screenshot> 
        <mediaobject> 
          <imageobject><imagedata fileref="figures/quick-lounge_window.png" format="PNG"/>  </imageobject>
          <textobject><phrase>L'applet &appname;.</phrase>  </textobject> 
        </mediaobject> 
      </screenshot> 
    </figure>

    <para>L'applet &app; permette di organizzare i pulsanti di avvio in un pannello.  </para>
    <para>Il pulsante di avvio &egrave; un pulsante che avvia una determinata applicazione, esegue un comando o apre un file. Per maggiori informazioni sui pulsanti di avvio, vedere l'ultima versione del &ug; per la propria piattaforma. </para>

    <sect2 id="quick-lounge-intro-add">
      <title>Aggiungere l'applet &appname; a un pannello </title>
      <para>Per aggiungere l'applet &app; a un pannello, fare clic con il pulsante destro del mouse sul pannello e scegliere  <menuchoice><guimenu>Aggiungi al pannello</guimenu> <guisubmenu>Utilit&agrave;</guisubmenu> <guimenuitem>&appname;</guimenuitem> </menuchoice>. </para>
    </sect2>

  </sect1><!-- ============= Usage  ================================ -->

  
  <sect1 id="quick-lounge-usage">
    <title>Uso</title>

    <sect2 id="quick-lounge-usage-popup">
      <title>Usare i menu popup </title>
    <para>&app; utilizza due tipi di menu popup:  </para>

    <itemizedlist>
      <listitem>
        <para>Il menu popup dell'applet contiene le voci standard incluse nella maggior parte dei menu popup dello stesso tipo.  </para>
        <para>Per visualizzare il menu popup dell'applet, fare clic con il pulsante destro del mouse sul punto di aggancio posto sul lato sinistro dell'applet o in qualsiasi area vuota dell'applet.  </para>
      </listitem>
      <listitem>
        <para>Il menu popup dei pulsanti di avvio contiene vari collegamenti alle azioni di &app;.  </para>
        <para>Per visualizzare questo menu, fare clic con il pulsante destro del mouse su un pulsante di avvio nell'applet. Il menu popup dei pulsanti di avvio contiene le seguenti voci: </para>
        <informaltable frame="all">
          <tgroup cols="2" colsep="1" rowsep="1">
            <colspec colname="COLSPEC0" colwidth="50*"/>
            <colspec colname="COLSPEC1" colwidth="50*"/>
            <thead>
              <row valign="top">
                <entry colname="COLSPEC0">
                  <para>Opzione</para></entry>
                <entry colname="COLSPEC1">
                  <para>Descrizione </para></entry>
              </row>
            </thead>
            <tbody>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Propriet&agrave;</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Mostra la finestra <guilabel>Propriet&agrave; del pulsante di avvio</guilabel> relativa al pulsante di avvio selezionato.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Guida</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Mostra la Guida in linea dell'applet &app;.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Guida sull'applicazione <replaceable>nome_applicazione</replaceable></guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Mostra la Guida in linea dell'applicazione associata al pulsante di avvio selezionato.  </para>
                  <para>Se l'applicazione non dispone di una Guida in linea, questa voce non compare nel menu popup.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Aggiungi pulsante di avvio</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Aggiunge un pulsante di avvio all'applet.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Aggiungi dal menu</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Aggiunge all'applet un pulsante di avvio da un menu.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Aggiungi spazio</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Aggiunge uno spazio all'applet.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Rimuovi</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Rimuove un pulsante di avvio o uno spazio dall'applet.  </para>
                </entry>
              </row>
              <row valign="top">
                <entry>
                  <para><guimenuitem>Sposta</guimenuitem>  </para>
                </entry>
                <entry>
                  <para>Sposta un pulsante di avvio o uno spazio in una posizione differente dell'applet.  </para>
                </entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>
        <note>
          <para>Se l'applet non contiene pulsanti di avvio, non &egrave; possibile visualizzare il menu popup dei pulsanti di avvio. In questi casi, per eseguire l'azione richiesta &egrave; possibile usare la finestra di dialogo <guilabel>Propriet&agrave; di &appname;</guilabel>. Per maggiori informazioni sulla finestra di dialogo <guilabel>Propriet&agrave; di &appname;</guilabel>, vedere <xref linkend="quick-lounge-prefs"/>. </para>
        </note>
      </listitem>
    </itemizedlist>

    </sect2>

    <sect2 id="quick-lounge-usage-add">
      <title>Aggiungere un pulsante di avvio </title>
      <para>Per aggiungere un pulsante di avvio &egrave; possibile procedere in diversi modi. </para>

      <sect3 id="quick-lounge-usage-add-new">
        <title>Aggiungere un nuovo pulsante di avvio </title>
        <para>Per aggiungere un nuovo pulsante di avvio, procedere come segue:  </para>
        <orderedlist>
          <listitem>
            <para>Fare clic con il pulsante destro del mouse su un pulsante di avvio nell'applet e scegliere <guimenuitem>Aggiungi pulsante di avvio</guimenuitem> dal menu popup.  </para>
          </listitem>
          <listitem>
            <para>Inserire le informazioni appropriate nella finestra di dialogo <guilabel>Nuovo pulsante di avvio</guilabel>.  </para>
          </listitem>
          <listitem>
            <para>Fare clic su <guibutton>OK</guibutton> per aggiungere il pulsante di avvio all'applet e chiudere la finestra di dialogo. </para>
          </listitem>
        </orderedlist>
      </sect3>
  
      <sect3 id="quick-lounge-usage-add-existing">
        <title>Aggiungere un pulsante di avvio esistente da un menu </title>
        <para>Per aggiungere un pulsante di avvio esistente da un menu, procedere come segue:  </para>
        <orderedlist>
          <listitem>
            <para>Fare clic con il pulsante destro del mouse su un pulsante di avvio nell'applet e scegliere <guimenuitem>Aggiungi dal menu</guimenuitem> dal menu popup.  </para>
            <para>Viene aperta la finestra di dialogo <guilabel>Scegli applicazioni</guilabel>.  </para>
          </listitem>
          <listitem>
            <para>Fare clic sul pulsante <guibutton>Espandi tutto</guibutton> per visualizzare tutte le applicazioni.  </para>
            <para>In alternativa, fare clic sul pulsante <guibutton>Comprimi tutto</guibutton> per visualizzare solo le categorie. Fare clic sulla freccia posta accanto al nome di una categoria per visualizzare le applicazioni di quella categoria.  </para>
          </listitem>
          <listitem>
            <para>Per selezionare un'applicazione, fare clic sulla casella di selezione posta accanto al nome corrispondente.  </para>
          </listitem>
          <listitem>
            <para>Fare clic su <guibutton>OK</guibutton> per aggiungere il pulsante di avvio all'applet e chiudere la finestra di dialogo. </para>
          </listitem>
        </orderedlist>
      </sect3>
  
      <sect3 id="quick-lounge-usage-add-menu">
        <title>Aggiungere un pulsante di avvio da un menu </title>
        <para>Per aggiungere un pulsante di avvio da un menu, procedere come segue:  </para>
        <orderedlist>
          <listitem>
            <para>Aprire un menu che contiene il pulsante di avvio.  </para>
          </listitem>
          <listitem>
            <para>Trascinare il pulsante di avvio sull'applet. </para>
          </listitem>
        </orderedlist>
      </sect3>
  
      <sect3 id="quick-lounge-usage-add-filemgr">
        <title>Aggiungere un pulsante di avvio dal File manager </title>
        <para>Per aggiungere un pulsante di avvio dal File manager, procedere come segue:  </para>
        <orderedlist>
          <listitem>
            <para>Individuare il file <filename>.desktop</filename> per il pulsante di avvio nel file system.  </para>
          </listitem>
          <listitem>
            <para>Trascinare il file <filename>.desktop</filename> sull'applet. </para>
          </listitem>
        </orderedlist>
      </sect3>

    </sect2>

    <sect2 id="quick-lounge-usage-add-space">
      <title>Aggiungere uno spazio </title>
      <para>Per aggiungere uno spazio all'applet, fare clic con il pulsante destro del mouse su un pulsante di avvio nell'applet e scegliere <guimenuitem>Aggiungi spazio</guimenuitem> dal menu popup. </para>
    </sect2>

    <sect2 id="quick-lounge-usage-move">
      <title>Spostare un pulsante di avvio o uno spazio </title>
      <para>Per spostare un pulsante di avvio in una posizione differente nell'applet, procedere come segue:  </para>
      <orderedlist>
        <listitem>
          <para>Fare clic con il pulsante destro del mouse sul pulsante di avvio o sullo spazio nell'applet, quindi scegliere <guimenuitem>Sposta</guimenuitem> dal menu popup.  </para>
        </listitem>
        <listitem>
          <para>Per cambiare la posizione del pulsante di avvio o dello spazio selezionato, fare clic sui pulsanti seguenti nella finestra di dialogo <guilabel>Propriet&agrave; di &appname;</guilabel>: </para>
          <informaltable frame="all">
            <tgroup cols="2" colsep="1" rowsep="1">
              <colspec colname="COLSPEC0" colwidth="50*"/>
              <colspec colname="COLSPEC1" colwidth="50*"/>
              <thead>
                <row valign="top">
                  <entry colname="COLSPEC0">
                    <para>Pulsante</para></entry>
                  <entry colname="COLSPEC1">
                    <para>Azione </para></entry>
                </row>
              </thead>
              <tbody>
                <row valign="top">
                  <entry>
                    <para>
                      <inlinemediaobject>
                        <imageobject><imagedata fileref="figures/quick-lounge_start.png" format="PNG"/>  </imageobject>
                        <textobject><phrase>Pulsante per lo spostamento del componente selezionato all'inizio dell'elenco.</phrase>  </textobject>
                      </inlinemediaobject>
                    </para>
                  </entry>
                  <entry>
                    <para>Sposta il componente selezionato all'inizio dell'elenco.  </para>
                  </entry>
                </row>
                <row valign="top">
                  <entry>
                    <para>
                      <inlinemediaobject>
                        <imageobject><imagedata fileref="figures/quick-lounge_up.png" format="PNG"/>  </imageobject>
                        <textobject><phrase>Pulsante per lo spostamento del componente selezionato di una posizione pi&ugrave; in alto nell'elenco.</phrase>  </textobject>
                      </inlinemediaobject>
                    </para>
                  </entry>
                  <entry>
                    <para>Sposta il componente selezionato di una posizione pi&ugrave; in alto nell'elenco.  </para>
                  </entry>
                </row>
                <row valign="top">
                  <entry>
                    <para>
                      <inlinemediaobject>
                        <imageobject><imagedata fileref="figures/quick-lounge_down.png" format="PNG"/>  </imageobject>
                        <textobject><phrase>Pulsante per lo spostamento del componente selezionato di una posizione pi&ugrave; in basso nell'elenco.</phrase>  </textobject>
                      </inlinemediaobject>
                    </para>
                  </entry>
                  <entry>
                    <para>Sposta il componente selezionato di una posizione pi&ugrave; in basso nell'elenco.  </para>
                  </entry>
                </row>
                <row valign="top">
                  <entry>
                    <para>
                      <inlinemediaobject>
                        <imageobject><imagedata fileref="figures/quick-lounge_end.png" format="PNG"/>  </imageobject>
                        <textobject><phrase>Pulsante per lo spostamento del componente selezionato alla fine dell'elenco.</phrase>  </textobject>
                      </inlinemediaobject>
                    </para>
                  </entry>
                  <entry>
                    <para>Sposta il componente selezionato alla fine dell'elenco.  </para>
                  </entry>
                </row>
              </tbody>
            </tgroup>
          </informaltable>
        </listitem>
        <listitem>
          <para>Fare clic su <guibutton>Chiudi</guibutton>. </para>
        </listitem>
      </orderedlist>
    </sect2>

    <sect2 id="quick-lounge-usage-remove">
      <title>Rimuovere un pulsante di avvio o uno spazio </title>
      <para>Per rimuovere un pulsante di avvio o uno spazio dall'applet, fare clic con il pulsante destro del mouse sul pulsante di avvio o sullo spazio e scegliere <guimenuitem>Rimuovi</guimenuitem> dal menu popup. </para>
    </sect2>

    <sect2 id="quick-lounge-usage-additional">
      <title>Accedere ad altri pulsanti di avvio </title>
      <para>Per accedere ad altri pulsanti di avvio nell'applet &app;, fare clic sul pulsante a freccia dell'applet. </para>
    </sect2>

    <sect2 id="quick-lounge-usage-props">
      <title>Visualizzare le propriet&agrave; di un pulsante di avvio </title>
      <para>Per visualizzare le propriet&agrave; di un pulsante di avvio, fare clic con il pulsante destro del mouse sul pulsante di avvio nell'applet e scegliere <guimenuitem>Propriet&agrave;</guimenuitem> dal menu popup.  </para>
      <para>Viene visualizzata la finestra di dialogo <guilabel>Propriet&agrave; del pulsante di avvio</guilabel>. </para>
    </sect2>

  </sect1>

  <sect1 id="quick-lounge-prefs">
    <title>Preferenze </title>
    <para>Per configurare &app;, fare clic con il pulsante destro del mouse sull'aggancio posto sul lato sinistro dell'applet e scegliere <guimenuitem>Propriet&agrave;</guimenuitem>. Viene aperta la finestra di dialogo <guilabel>Propriet&agrave; di &appname;</guilabel>.  </para>
    <variablelist>
      <varlistentry><term><guilabel>Pulsanti di avvio</guilabel></term>  <listitem>
          <itemizedlist>
            <listitem>
              <para>Elenco dei componenti  </para>
              <para>Usare questo elenco per selezionare un pulsante di avvio o uno spazio.  </para>
            </listitem>
            <listitem>
              <para><guibutton>Propriet&agrave;</guibutton>  </para>
              <para>Fare clic su questo pulsante per visualizzare le propriet&agrave; del pulsante di avvio selezionato.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-props"/>.  </para>
            </listitem>
            <listitem>
              <para><guibutton>Aggiungi pulsante di avvio</guibutton>  </para>
              <para>Fare clic su questo pulsante per aggiungere un pulsante di avvio all'applet.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-add-new"/>.  </para>
            </listitem>
            <listitem>
              <para><guibutton>Aggiungi dal menu</guibutton>  </para>
              <para>Fare clic su questo pulsante per aggiungere all'applet un pulsante di avvio da un menu.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-add-existing"/>.  </para>
            </listitem>
            <listitem>
              <para><guibutton>Aggiungi spazio</guibutton>  </para>
              <para>Fare clic su questo pulsante per aggiungere uno spazio all'applet.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-add-space"/>.  </para>
            </listitem>
            <listitem>
              <para><guibutton>Rimuovi</guibutton>  </para>
              <para>Fare clic su questo pulsante per rimuovere un pulsante di avvio o uno spazio dall'applet.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-remove"/>.  </para>
            </listitem>
            <listitem>
              <para>Pulsanti a freccia  </para>
              <para>Fare clic su questi pulsanti per spostare un pulsante di avvio o uno spazio nell'applet.  </para>
              <para>Per maggiori informazioni, vedere <xref linkend="quick-lounge-usage-move"/>.  </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
        <varlistentry><term><guilabel>Dimensioni</guilabel></term>  <listitem>
          <itemizedlist>
            <listitem>
              <para><guilabel>Dimensione minima</guilabel>  </para>
              <para>Usare questa casella di selezione per specificare il numero minimo di pulsanti di avvio da visualizzare nell'applet.  </para>
            </listitem>
            <listitem>
              <para><guilabel>Dimensione massima</guilabel>  </para>
              <para>Usare questa casella di selezione per specificare il numero massimo di pulsanti di avvio da visualizzare nell'applet.  </para>
            </listitem>
            <listitem>
              <para><guilabel>Le dimensioni dell'icona seguono quelle del pannello</guilabel>  </para>
              <para>Selezionare questa opzione per ridimensionare le icone dei pulsanti di avvio in proporzione alle dimensioni del pannello. </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>

 </sect1>
</article>